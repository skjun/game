<?php
/**
 * Created by PhpStorm.
 * User: yuyue
 * Date: 13-12-12
 * Time: 上午11:29
 * descript: 此类主要定义一些系统常量，使用时，可以直接  类名::常量名
 */
class Constant
{
    // 定义常量
    const title = "Deal for mom |  The best online deals,sales, discounted coupons, promotions, gift cards for mom&baby - Deal for mom";
    const description = "Amazon mom daily deals,Best Amazon baby deals,Baby disapers on sale,Amazon mom coupons, Amazon deal of the day";
    const keywords = "The best deals, sales, discount coupons, gift card for amazon mom&baby products.Amazon mom daily deals,best amazon baby deals,baby disapers on sale, cheapest baby toys, cheapest and best baby strollers,etc... ";
    //const只能用于定义简单类型的常量，复杂类型的可以使用static关键字
    public static $seoarray = array("title" => self::title, "description" => self::description, "keywords" => self::keywords);


    /**
     * 网站统计代码
     */
    public static function getTongji()
    {
        require_once SERVICE . DS . 'admin/CommonService.class.php';
        $commonService = new CommonService(self::getDB());
        $sqlParamSql = "select id,name,value from tb_system_param  where name='tongji'";
        $params = $commonService->query($sqlParamSql);
        return $params[0]->value;
    }

    public static function getHotTag()
    {
        $file_name = PROJECT . "/model/cache/hottag";
        $handle = fopen($file_name, "r");
        $contents = fread($handle, filesize($file_name));
        fclose($handle);
        $array = unserialize($contents);
        return $array;
    }

    public static function getHotCate()
    {
        $file_name = PROJECT . "/model/cache/hotcate";
        $handle = fopen($file_name, "r");
        $contents = fread($handle, filesize($file_name));
        fclose($handle);
        $array = unserialize($contents);
        return $array;
    }

    public static function getDB()
    {
        global $CONFIG;
        $dbutil = new DbUtil($CONFIG['DB']['db_host'], $CONFIG['DB']['db_user'], $CONFIG['DB']['db_password'], $CONFIG['DB']['db_database']);
        return $dbutil;
    }

    public static function indexHeader()
    {
        //要考虑在这里会出现的表达式{website_name}，只有这一个。
        require_once SERVICE . DS . 'admin/CommonService.class.php';
        $commonService = new CommonService(self::getDB());
        $sqlParamSql = "select id,name,value from tb_system_param  where name in ('website_name','home_title','home_keywords','home_desc')";
        $params = $commonService->query($sqlParamSql);
        $title = str_replace("{website_name}", $params[0]->value, $params[1]->value);
        $seoarray = array("title" => $title, "description" => $params[3]->value, "keywords" => $params[2]->value, "tongji" => self::getTongji());
        $pagesets = array("seo" => $seoarray);
        return $pagesets;

    }

    public static function initGameSeo($game)
    {
        require_once SERVICE . DS . 'admin/CommonService.class.php';
        $commonService = new CommonService(self::getDB());
        $sqlParamSql = "select name,value from tb_system_param ";
        $params = $commonService->query($sqlParamSql);
        $paramkv = array();
        for ($i = 0; $i < count($params); $i++) {
            $paramkv[$params[$i]->name] = $params[$i]->value;
        }
        //hand title
        $title = str_replace("{home_title}", $paramkv['home_title'], $paramkv['game_title']);
        $title = str_replace("{game_title}", $game->game_name, $title);
        $title = str_replace("{website_name}", $paramkv['website_name'], $title);
        //hand keywords
        $keywords = str_replace("{home_keywords}", $paramkv['home_keywords'], $paramkv['game_keywords']);
        $tags = array();
        for ($i = 0; $i < count($game->gamtags); $i++) {
            $tags[] = $game->gamtags[$i]->tag_name;
        }
        $keywords = str_replace("{game_tags}", implode(',', $tags), $keywords);
        //hand desc
        $desc = str_replace("{home_desc}", $paramkv['home_desc'], $paramkv['game_desc']);
        $desc = str_replace("{game_desc}", $game->shortdesc, $desc);
        $seoarray = array("title" => $title, "description" => $desc, "keywords" => $keywords, "tongji" => self::getTongji());
        $pagesets = array("seo" => $seoarray);
        return $pagesets;
    }

    public static function initTagSeo($tag)
    {
        require_once SERVICE . DS . 'admin/CommonService.class.php';
        $commonService = new CommonService(self::getDB());
        $sqlParamSql = "select name,value from tb_system_param ";
        $params = $commonService->query($sqlParamSql);
        $paramkv = array();
        for ($i = 0; $i < count($params); $i++) {
            $paramkv[$params[$i]->name] = $params[$i]->value;
        }
        //hand title
        $title = str_replace("{home_title}", $paramkv['home_title'], $paramkv['tag_title']);
        $title = str_replace("{tag}", $tag->tag_name, $title);
        $title = str_replace("{website_name}", $paramkv['website_name'], $title);
        //hand keywords
        $keywords = str_replace("{home_keywords}", $paramkv['home_keywords'], $paramkv['tag_keywords']);
        $keywords = str_replace("{tag}", $tag->tag_name, $keywords);
        //hand desc
        $desc = str_replace("{home_desc}", $paramkv['home_desc'], $paramkv['tag_desc']);
        $desc = str_replace("{tag}", $tag->tag_name, $desc);
        $seoarray = array("title" => $title, "description" => $desc, "keywords" => $keywords, "tongji" => self::getTongji());
        $pagesets = array("seo" => $seoarray);
        return $pagesets;
    }

    public static function postHeader($postId)
    {
        $js = array(
            WEBSITE_URL . "public/assets/js/jquery.cookie.js",
            WEBSITE_URL . "public/js/posts/post.js"
        );

        require_once SERVICE . DS . 'admin/CommonService.class.php';
        $commonService = new CommonService(self::getDB());
        $sqlParamSql = "select id,name,value from tb_system_param  where name in ('website_name','home_title','home_keywords','home_desc','post_title','post_keywords','post_desc')";
        $params = $commonService->query($sqlParamSql);
        $paramkv = array();
        for ($i = 0; $i < count($params); $i++) {
            $paramkv[$params[$i]->name] = $params[$i]->value;
        }
        $postsql = "select * from posts where ID=$postId";
        $posts = $commonService->query($postsql);
        $post = $posts[0];
        //hand title
        $title = str_replace("{home_title}", $paramkv['home_title'], $paramkv['post_title']);
        $title = str_replace("{post_title}", $post->post_title, $title);
        $title = str_replace("{website_name}", $paramkv['website_name'], $title);
        //hand keywords
        $keywords = str_replace("{home_keywords}", $paramkv['home_keywords'], $paramkv['post_keywords']);
        $keywords = str_replace("{post_tags}", $post->tags, $keywords);

        //hand desc
        $desc = str_replace("{home_desc}", $paramkv['home_desc'], $paramkv['post_desc']);
        $desc = str_replace("{post_excerpt}", $post->post_excerpt, $desc);
        $seoarray = array("title" => $title, "description" => $desc, "keywords" => $keywords, "tongji" => self::getTongji());
        $pagesets = array(
            "seo" => $seoarray,
            "js" => $js,
            "nav" => self::getNav()
        );
        return $pagesets;
    }

    public static function tagHeader($tagname)
    {
        $js = array(WEBSITE_URL . "public/js/jquery.imagesloaded.js",
            WEBSITE_URL . "public/js/jquery.wookmark.min.js",
            WEBSITE_URL . "public/js/listBox.js",
            WEBSITE_URL . "public/js/tag.js");
        require_once SERVICE . DS . 'admin/CommonService.class.php';

        $commonService = new CommonService(self::getDB());
        $sqlParamSql = "select id,name,value from tb_system_param  where name in ('website_name','home_title','home_keywords','home_desc','tag_title','tag_keywords','tag_desc')";
        $params = $commonService->query($sqlParamSql);
        $paramkv = array();
        for ($i = 0; $i < count($params); $i++) {
            $paramkv[$params[$i]->name] = $params[$i]->value;
        }
        //hand title
        $title = str_replace("{home_title}", $paramkv['home_title'], $paramkv['tag_title']);
        $title = str_replace("{tag}", $tagname, $title);
        $title = str_replace("{website_name}", $paramkv['website_name'], $title);
        //hand keywords
        $keywords = str_replace("{home_keywords}", $paramkv['home_keywords'], $paramkv['tag_keywords']);
        $keywords = str_replace("{tag}", $tagname, $keywords);
        //hand desc
        $desc = str_replace("{home_desc}", $paramkv['home_desc'], $paramkv['tag_desc']);
        $desc = str_replace("{tag}", $tagname, $desc);

        $seoarray = array("title" => $title, "description" => $desc, "keywords" => $keywords, "tongji" => self::getTongji());
        $pagesets = array("seo" => $seoarray, "js" => $js, "nav" => Constant::getNav());

        return $pagesets;
    }

    //得到页面的二级导航 ，也就当前在哪里，传入一个分类id
    public static function getSecCategoryNav($categoryid)
    {
        $file_name = PROJECT . "/model/cache/nav";
        $handle = fopen($file_name, "r");
        $contents = fread($handle, filesize($file_name));
        fclose($handle);
        $array = unserialize($contents);
        $sec_cateogory_nav = '&gt;';
        foreach ($array as $pcategory) {
            if ($categoryid == $pcategory['parent']->category_id) {
                $sec_cateogory_nav .= '<a href=' . $categoryid . '/index.html>  ' . $pcategory['parent']->category_name . '</a>';
            } else {
                //还要判断是不是子一级的
                if (!empty($pcategory['childres'])) {
                    foreach ($pcategory['childres'] as $category) {
                        if ($categoryid == $category->category_id) {
                            $sec_cateogory_nav .= '<a href=' . $pcategory['parent']->category_id . '/index.html>  ' . $pcategory['parent']->category_name . '</a>&gt;';
                            $sec_cateogory_nav .= '<a href=' . $pcategory['parent']->category_id . '/' . $categoryid . '/index.html>  ' . $category->category_name . '</a>';

                        }
                    }
                }
            }
        }
        return $sec_cateogory_nav;
    }

    //得到页面的二级导航 ，也就当前在哪里，传入一个分类id
    public static function getCategoryName($categoryid)
    {
        $file_name = PROJECT . "/model/cache/nav";
        $handle = fopen($file_name, "r");
        $contents = fread($handle, filesize($file_name));
        fclose($handle);
        $array = unserialize($contents);
        $categoryname = '';

        foreach ($array as $pcategory) {
            if ($categoryid == $pcategory['parent']->category_id) {
                $categoryname = $pcategory['parent']->category_name;
            } else {
                //还要判断是不是子一级的
                if (!empty($pcategory['childres'])) {
                    foreach ($pcategory['childres'] as $category) {
                        if ($categoryid == $category->category_id) {
                            $categoryname = $category->category_name;
                            break;
                        }
                    }
                }
            }
        }
        return $categoryname;
    }


    //装载管理员界面的语言包
    public static function loadAdminLanguange($ln)
    {
        if ($ln == null) {
            $ln = "cn";
        }
        include_once 'lang/admin/' . $ln . '.lang.php';
        $_SESSION['adminlanguagename'] = $str['0'];
        return $str;
    }

    //启用game的url链接
    public static function createSingleGameURL($game)
    {
        return $game->game_id . '_' . str_replace(" ", "-", str_replace(":", "", $game->game_name)) . '.html';
    }

    //启用game的url链接
    public static function extendSingleGameURLID($url)
    {
        $pms = explode('_', $url);
        return $pms[0];
    }

    //
    //
    public static function extendUrls($str, $spit = '_')
    {
        $pms = str_replace('.html', '', $str);
        $pmarray = explode('_', $pms);
        foreach ($pmarray as $item) {
            $result[substr($item, 0, 1)] = substr($item, 1);
        }
        return $result;
    }

    //获取配置文件的seo信息
    public static function getSeoMsg($key)
    {
        require_once SERVICE . DS . 'admin/CommonService.class.php';
        $commonService = new CommonService(self::getDB());
        $sqlParamSql = "SELECT t.value FROM tb_system_param t WHERE t.name = '" . $key . "_seo'";
        $obj = $commonService->getResult($sqlParamSql);
        if ($obj->value != null) {
            return $obj->value;
        }
        $key = "home";

        $sqlParamSql = "SELECT t.value FROM tb_system_param t WHERE t.name = '" . $key . "_seo'";
        $obj = $commonService->getResult($sqlParamSql);

        return $obj->value;

    }
}