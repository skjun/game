<?php

class AdController extends  Controller{
    /**
     * 前台，使用ajax请用controller，这个controller去调用接口服务，
     * 其实，用ajax也可以直接调接口，只是会有浏览器跨域的问题。所以这样方便点
     * 与接口交互，使用面向对象，然后转json串。
     * 出错时：{ "code" : 0, "message" : "the user name or password is error" }
     * 正常返回:{ "code" : 1, "message" : "{\"key\":\"a\",\"value\":\"a\"}" }
     *
    1. 广告
    	通过key得到value，如果用户没有设置，取超级管理员设置的广告，key为 广告title,值为广告href
     */
	public function index(){
//        require_once MODEL . DS . "MessageDTO.php";
//        require_once MODEL . DS . "StatisticReqDTO.php";
//        global  $CONFIG;
//
//        //请求参数
//        $statisticdDTO = new StatisticReqDTO();
//
//        $statisticdDTO->username=$CONFIG['DATA_SERVER']['SERVER_USERNAME'];
//        $statisticdDTO->password=$CONFIG['DATA_SERVER']['SERVER_PWD'];
//        $statisticdDTO->userId = $CONFIG['DATA_SERVER']['SERVER_USER_ID'];
//        $statisticdDTO->websiteName = $CONFIG['DATA_SERVER']['SERVER_WEBSITE_NAME'];
//        $statisticdDTO->websiteId =$CONFIG['DATA_SERVER']['SERVER_WEBSITE_ID'];
//        $statisticdDTO->gameId = 3;
//        $statisticdDTO->statisType = 1;
//
//        $reqJson = json_encode($statisticdDTO);
//
//        $message = new MessageDTO();
//        $message->code=1;
//        $message->message=$reqJson;
//
//        $data_string = json_encode($message);
//        $server_url=$CONFIG['DATA_SERVER']['SERVER_BASE_URL']."api/statistical/add";
//        $ch = curl_init($server_url);
////        $ch = curl_init(SERVER_BASE_URL'http://localhost:8080/gamesitebuilder/api/ad/getAdByPlace');
//        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
//        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
//                'Content-Type: application/json',
//                'Content-Length: ' . strlen($data_string))
//        );
//
//        $result = curl_exec($ch);
//        //这样就得到了数据，直接把json解开
//        $message = json_decode($result);
        $this->affcode("bigfish",null);


	}
    public function statistic($gameId,$type){
        require_once MODEL . DS . "MessageDTO.php";
        require_once MODEL . DS . "StatisticReqDTO.php";
        global  $CONFIG;

        //请求参数
        $statisticdDTO = new StatisticReqDTO();

        $statisticdDTO->username=$CONFIG['DATA_SERVER']['SERVER_USERNAME'];
        $statisticdDTO->password=$CONFIG['DATA_SERVER']['SERVER_PWD'];
        $statisticdDTO->userId = $CONFIG['DATA_SERVER']['SERVER_USER_ID'];
        $statisticdDTO->websiteName = $CONFIG['DATA_SERVER']['SERVER_WEBSITE_NAME'];
        $statisticdDTO->websiteId =$CONFIG['DATA_SERVER']['SERVER_WEBSITE_ID'];
        $statisticdDTO->gameId = $gameId; //游戏id   进入页面就统计
        $statisticdDTO->statisType = $type; // 0为浏览，1为下载或购买，点那个按钮时统计

        $reqJson = json_encode($statisticdDTO);

        $message = new MessageDTO();
        $message->code=1;
        $message->message=$reqJson;

        $data_string = json_encode($message);
        $server_url=$CONFIG['DATA_SERVER']['SERVER_BASE_URL']."api/statistical/add";
        $ch = curl_init($server_url);
//        $ch = curl_init(SERVER_BASE_URL'http://localhost:8080/gamesitebuilder/api/ad/getAdByPlace');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $result = curl_exec($ch);
        //这样就得到了数据，直接把json解开
        $message = json_decode($result);
    }
    /**
    这个返回值中的message就是结果
     获取用户的aff code
     */
    public function affcode($affName,$language){
        require_once MODEL . DS . "MessageDTO.php";
        require_once MODEL . DS . "AffCodeDTO.php";
        global  $CONFIG;

        //请求参数
        $afDTO = new AffCodeDTO();
        $afDTO->userid = $CONFIG['DATA_SERVER']['SERVER_USER_ID'];
        $afDTO->gamelanguage = $language;
        $afDTO->affName =$affName;

        $reqJson = json_encode($afDTO);

        $message = new MessageDTO();
        $message->code=1;
        $message->message=$reqJson;

        $data_string = json_encode($message);
        $server_url=$CONFIG['DATA_SERVER']['SERVER_BASE_URL']."api/affcode/getAffcode";
        $ch = curl_init($server_url);
//        $ch = curl_init('http://localhost:8080/gamesitebuilder/api/adcode/getAdcode');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $result = curl_exec($ch);
        //这样就得到了数据，直接把json解开
        $message = json_decode($result);
//        var_dump($server_url);
//        var_dump($data_string);
//
     //   var_dump($message);
         return $message->message;

    }

    /**
     * 返回的广告数据是有{ ["key"]=> string(1) "a" ["value"]=> string(1) "a" }
     * 超级管理员的广告内容的key要跟用户website_param 是同样的，这样，用户没有设设置的，就取超级管理员的
     * 有一个唯一固定的，这个key在wbsite_param本身不存在，所以，就取了超管在广告内容设置的
     */
	public function getKeyAd($key){
        require_once MODEL . DS . "MessageDTO.php";
        require_once MODEL . DS . "AdReqDTO.php";
        global  $CONFIG;

        //请求参数
        $adDTO = new AdReqDTO();

        $adDTO->username=$CONFIG['DATA_SERVER']['SERVER_USERNAME'];
        $adDTO->password=$CONFIG['DATA_SERVER']['SERVER_PWD'];
        $adDTO->userId = $CONFIG['DATA_SERVER']['SERVER_USER_ID'];
        $adDTO->websiteName = $CONFIG['DATA_SERVER']['SERVER_WEBSITE_NAME'];
        $adDTO->websiteId =$CONFIG['DATA_SERVER']['SERVER_WEBSITE_ID'];

//        $adDTO->key =$_POST['key'];
        $adDTO->key =$key;
        $reqJson = json_encode($adDTO);

        $message = new MessageDTO();
        $message->code=1;
        $message->message=$reqJson;

        $data_string = json_encode($message);
        $server_url=$CONFIG['DATA_SERVER']['SERVER_BASE_URL']."api/ad/getAdByPlace";
        $ch = curl_init($server_url);
//        $ch = curl_init(SERVER_BASE_URL'http://localhost:8080/gamesitebuilder/api/ad/getAdByPlace');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $result = curl_exec($ch);
        //这样就得到了数据，直接把json解开
        $message = json_decode($result);
        $code = json_decode($message->message);
        //var_dump($code);
        return $code->value;
        //return json_encode( $code);

	}
    public function  show($param){
       echo $this->getKeyAd($param[0]);
       // echo "aaa";
    }

}