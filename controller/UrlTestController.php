<?php
class UrlTestController extends  Controller{
	
	public function index(){
	   echo "index ,localhost/urlTest";
	}
    public function two(){
        echo "method two ,localhost/urlTest/two";
    }

    /**
     * 前两个应该都知道，主要是这个第三个，记住第三级放参数就好了
     * 参考下面
     * http://localhost/urlTest/three/a=1&b=2&c=3
     * 那么，params
     * @param $params
     */
    public function three($params){
      //  var_dump($params);
        echo $params["a"];
        echo $params["b"];
        echo $params["c"];
        echo "http://localhost/urlTest/three/a=1&b=2&c=3";
    }
}