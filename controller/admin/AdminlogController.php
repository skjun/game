<?php

class AdminLogController extends Controller {
	public function index() {
		$this->getSmarty ();
		
	 	include_once 'common/Constant.class.php';
		$this->smarty->assign("str",Constant::loadAdminLanguange($_SESSION["adminlanguage"]));
		$this->smarty->display ( "admin/users/admin_log.tpl" );
	}

	public function logList(){//页码
		$page = $_POST["page"];
		if($page == null ){
			$page = $_GET["page"];
		}
		//每页显示记录数
		$rows = $_POST["rows"];
		if($rows == null ){
			$rows = $_GET["rows"];
		}
		$start = ($page-1)*$rows ;
		$end = $page*$rows;
		//排序索引id
		$sidx = $_POST["sidx"];
		if($sidx == null ){
			$sidx = $_GET["sidx"];
		}
		//排序顺序
		$sord = $_POST["sord"];
		if($sord == null ){
			$sord = $_GET["sord"];
		}
		
		//日志类型
		$logType = $_POST["logType"];
		if($logType == null ){
			$logType = $_GET["logType"];
		}
		$logDesc = $_POST["logDesc"];
		if($logDesc == null ){
			$logDesc = $_GET["logDesc"];
		}
		$startTime = $_POST["startTime"];
		if($startTime == null ){
			$startTime = $_GET["startTime"];
		}
		$endTime = $_POST["endTime"];
		if($endTime == null ){
			$endTime = $_GET["endTime"];
		}
		
		$dbutil = $this->getDB();
		
		$countSql = " select count(log.id) total from admin_log log LEFT JOIN 
				admin_users admin on  admin.id = log.admin_id where 1=1 ";
		$sql = " select log.id,log.log_type,log.log_desc,log.log_time,admin.user_name from admin_log log LEFT JOIN 
				admin_users admin on  admin.id = log.admin_id where 1=1 ";
		
		if($logType != null && $logType != ""){
			$countSql = $countSql." and log.log_type like '%$logType%' ";
			$sql = $sql." and log.log_type like '%$logType%' ";
		}
		if($logDesc != null && $logDesc != ""){
			$countSql = $countSql." and log.log_desc like '%$logDesc%' ";
			$sql = $sql." and log.log_desc like '%$logDesc%' ";
		}
		if($startTime != null && $startTime != ""){
			$countSql = $countSql." and log.log_time > str_to_date('$startTime','%Y-%m-%d')  ";
			$sql = $sql." and log.log_time > str_to_date('$startTime','%Y-%m-%d')  ";
		}
		if($endTime != null && $endTime != ""){
			$countSql = $countSql." and log.log_time < str_to_date('$endTime','%Y-%m-%d')  ";
			$sql = $sql." and log.log_time < str_to_date('$endTime','%Y-%m-%d')  ";
		}
		
		$res = $dbutil->get_results($countSql);
		$records = $res;
		foreach ($res as $re){
			$records = $re->total;
			break;
		}
		
		if($sidx != null && $sidx != "" && $sord != null && $sord != ""){
			$sql = $sql." order by log.$sidx $sord ";
		}else{
			$sql = $sql." order by log.log_time ";
		}
		$sql = $sql." limit $start ,$rows";
		
		$res = $dbutil->get_results($sql);
		$data = array();
		foreach ($res as $re){
			$data[] = array(
					"id"=>$re->id,
					"cell"=>array(
							$re->id,
							$re->user_name,
							$re->log_type,
							$re->log_desc,
							$re->log_time
							)
			);
		}
		
		$totalPage = ceil($records/$rows);
		
		$result = array(
				"rows"=>$data,
				"page"=>$page,
				"total"=>$totalPage,
				"yy"=>$sql,
				"records"=>$records);
		
		echo json_encode($result);
	}
}

?>