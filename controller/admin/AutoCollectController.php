<?php
class AutoCollectController extends  Controller{
	public function index(){
        require_once MODEL . DS . "MessageDTO.php";
        require_once MODEL . DS . "UserDTO.php";
        set_time_limit(0); //prevent timeout

        $user = new UserDTO();
        $user->password=md5("superyuyue");
        $user->username="superyuyue";
        $user->website=1;
        $userJson = json_encode($user);

        $message = new MessageDTO();
        $message->code=1;
        $message->message=$userJson;

        $data_string = json_encode($message);
        set_time_limit(0);
        $url = "http://localhost:8080/gamesitebuilder/api/game/gamesbysite";

        $fp = fopen (dirname(__FILE__) . '/datas.zip', 'w+');//This is the file where we save the    information
        $ch = curl_init(str_replace(" ","%20",$url));//Here is the file we are downloading, replace spaces with %20
        curl_setopt($ch, CURLOPT_TIMEOUT, 5040);
        curl_setopt($ch, CURLOPT_FILE, $fp); // write curl response to file
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

       curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
   //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
       curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        curl_exec($ch); // get curl response
        curl_close($ch);
        fclose($fp);
        echo dirname(__FILE__);
    }
    /**
     * 使用php的 curl api下载文件
     */
    public function downloadGames(){
        require_once MODEL . DS . "MessageDTO.php";
        require_once MODEL . DS . "UserDTO.php";
        set_time_limit(0); //prevent timeout

        $user = new UserDTO();
        $user->password=md5("superyuyue");
        $user->username="superyuyue";
        $user->website=1;
        $userJson = json_encode($user);

        $message = new MessageDTO();
        $message->code=1;
        $message->message=$userJson;

        $data_string = json_encode($message);
        set_time_limit(0);
        $url = "http://198.23.115.180:8080/gamesitebuilder/api/game/gamesbysite";

        $fp = fopen (dirname(__FILE__) . '/datas.zip', 'w+');//This is the file where we save the    information
        $ch = curl_init(str_replace(" ","%20",$url));//Here is the file we are downloading, replace spaces with %20
        curl_setopt($ch, CURLOPT_TIMEOUT, 5040);
        curl_setopt($ch, CURLOPT_FILE, $fp); // write curl response to file
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        curl_exec($ch); // get curl response
        curl_close($ch);
        fclose($fp);
        echo dirname(__FILE__);
    }
    /**
     * 使用php的 curl api获取发送及获取json数据
     */
    public function checkUserWithCURL(){
        require_once MODEL . DS . "MessageDTO.php";
        require_once MODEL . DS . "UserDTO.php";

        $user = new UserDTO();
        $user->password=md5("superyuyue");
        $user->username="superyuyue";
        $user->$websiteName=1;
        $userJson = json_encode($user);

        $message = new MessageDTO();
        $message->code=1;
        $message->message=$userJson;

        $data_string = json_encode($message);


        $ch = curl_init('http://198.23.115.180:8080/gamesitebuilder/api/user/validateuser');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $result = curl_exec($ch);
        echo $result;
    }

    /**
     * 使用file_get_contents的php api获取整个url返回的内容
     */
    public function testGetOnGameUseFileGetContents(){
        set_time_limit(0);
        $url = 'http://198.23.115.180:8080/gamesitebuilder/api/game/gamessiteone';
        $html = @file_get_contents($url);
        echo "now collect game";
        var_dump(json_decode($html));
    }
}