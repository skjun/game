<?php
class CollectController extends Controller{
	
	public function index(){
		$this->getSmarty ();
		include_once 'common/Constant.class.php';
		$this->smarty->assign("str",Constant::loadAdminLanguange($_SESSION["adminlanguage"]));
		$this->smarty->display ( "admin/collect/collect.tpl" );
	}
	
    public function collect(){
		set_time_limit(0);
		require_once SERVICE . DS . "admin" . DS . "CollectService.class.php";

		$dbutil = $this->getMedooDb();
		//在安装的时候，获取数据，并替换到config.php中了
		global  $CONFIG;
		$username=$CONFIG['DATA_SERVER']['SERVER_USERNAME'];
		$password=$CONFIG['DATA_SERVER']['SERVER_PWD'];
		$userId = $CONFIG['DATA_SERVER']['SERVER_USER_ID'];
		$websiteName = $CONFIG['DATA_SERVER']['SERVER_WEBSITE_NAME'];
		$websiteId =$CONFIG['DATA_SERVER']['SERVER_WEBSITE_ID'];

		$collectService = new CollectService($dbutil,$userId,$username,$password,$websiteName,$websiteId);
		$collectService->download();
		$collectService->storage();
		$result = array("success"=>true);
		 
		echo json_encode($result);
    }
}