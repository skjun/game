<?php
class FileuploadController extends Controller {
	public function index() {
		$this->getSmarty ();
		// $this->smarty->display("map.tpl");
	}
	public function fileup() {
		global $fileload;
		if ($_FILES ['msUploadFile'] ['size'] < 1024 * 1024 * 5) {
			
			$name = date ( "Y-m-d_H-i-s" );
			$direct = date ( "Y-m-d" );
			$uploaddir = PROJECT . DS . "upload" . DS; // 上传路径
			$pinfo = pathinfo ( $_FILES ['msUploadFile'] ['name'] );
			$ftype = $pinfo [extension];
			if (! file_exists ( $uploaddir . $direct )) {
				mkdir ( $uploaddir . $direct );
			}
			$filepath = $direct . DS . $name . "." . $ftype;
			
			$destination = $uploaddir . $filepath;
			
			if (move_uploaded_file ( $_FILES ['msUploadFile'] ['tmp_name'], $destination )) {
				$fileload->type = "1";
				$fileload->message = "上传成功！ ";
				$fileload->size = $_FILES ['msUploadFile'] ['size'];
				$fileload->tmpname = $_FILES ['msUploadFile'] ['tmp_name'];
				$fileload->filename =WEBSITE_URL."upload".DS.$filepath;
			}
		} else {
			$fileload->type = "0";
			$fileload->message = "请上传小于5MB的附件);";
		}
		echo json_encode ( $fileload );
	}
	public function datafileupLoad() {
		global $fileload;
		
		$name = date ( "Y-m-d_H-i-s" );
		$direct = date ( "Y-m-d" );
		$uploaddir = PROJECT . DS . "upload" . DS; // 上传路径
		$pinfo = pathinfo ( $_FILES ['upload'] ['name'] );
		$ftype = $pinfo [extension];
		if (! file_exists ( $uploaddir . $direct )) {
			mkdir ( $uploaddir . $direct );
		}
		$filepath = $direct . DS . $name . "." . $ftype;
		
		$destination = $uploaddir . $filepath;
		
		if (move_uploaded_file ( $_FILES ['upload'] ['tmp_name'], $destination )) {
			$fileload->type = "1";
			$fileload->message = "上传成功！ ";
			$fileload->size = $_FILES ['upload'] ['size'];
			$fileload->tmpname = $_FILES ['upload'] ['tmp_name'];
			$fileload->filename = $filepath;
		}
		
		echo json_encode ( $fileload );
	}
}
