<?php
class FriendlinksController extends  Controller{

	public function index(){
		$this->getSmarty();

		include_once 'common/Constant.class.php';
		$this->smarty->assign("str",Constant::loadAdminLanguange($_SESSION["adminlanguage"]));
		$this->smarty->display("admin/links/friendlinks.tpl");
	}
	public function datalist(){
		require_once SERVICE . DS . 'admin/CommonService.class.php';
		//hand where condition

		$common = new CommonService($this->getDB());
		$page = isset ( $_POST ['page'] ) ? intval ( $_POST ['page'] ) : 1;
		$rows = isset ( $_POST ['rows'] ) ? intval ( $_POST ['rows'] ) : 10;
		$offset = ($page - 1) * $rows;
		$result = array ();
		$row_count = $common->countNumWhere("friendlinks",$where);
		$result ["total"] = ceil($row_count/$rows);
		$result ["records"] = $row_count;
		$result ["page"] = $page;
		$user_infos = $common->dataPageWhere('friendlinks', $offset, $rows,$where);
		$items = array ();
		$result ["rows"] = $user_infos;
		echo json_encode ( $result );
	}


	 
	function delete(){
		require_once SERVICE . DS .'admin'.DS. 'CommonService.class.php';
		$common = new CommonService($this->getDB());
		$id = $_POST ['id'];
		$input_condition = "id = '". $id."'";
		$common->delete("friendlinks", $input_condition);
		$data = array("success"=>$id);
		echo json_encode ( $data );
	}
	function addOrModify(){
		$smaryt = $this->getSmarty ();
		require_once SERVICE . DS.'admin'.DS . 'CommonDataService.class.php';
		$service = new CommonDataService ( $this->getDB () );
		$sitename = $_POST['sitename'];
		$id = $_POST['id'];
		$siteurl = $_POST['siteurl'];
		$sitelogo = $_POST['sitelogo'];
		if(empty($id)){
			$input_data = array (
				'id' => uniqid(),
				'sitename' => $sitename,
				'siteurl' =>$siteurl,
				'sitelogo' =>$sitelogo,
				'create_time' => date ( "Y-m-d H-i-s" )
			);
			$id = $service->add("friendlinks", $input_data);
		}else{
			//修改
			$input_data = array (
				'sitename' => $sitename,
				'siteurl' =>$siteurl,
				'sitelogo' =>$sitelogo
			);
			$input_condition = array (
				'id' => $id
			);
			$service->edit("friendlinks", $input_data, $input_condition);
		}
		echo json_encode($input_data);
	}
}