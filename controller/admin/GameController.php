<?php
class GameController extends Controller {
	public function index() {
		$smaryt = $this->getSmarty ();

		 include_once 'common/Constant.class.php';
		$this->smarty->assign("str",Constant::loadAdminLanguange($_SESSION["adminlanguage"]));
		$this->smarty->display ( "admin/game/game.tpl" );
	}
    //类似这种查询应该放在内存中，比如，redis,
    public function getLanguage(){
        $dbutil = $this->getDB();
        $sql = "select language,count(language) countnum from game group by language";
        $res = $dbutil->get_results($sql);
        $datas =array();
    	include_once 'lang/admin/country.lang.php';
        foreach ($res as $re){
            $data  = array("language"=>$re->language,"showlanguage"=>$lnstr[$re->language]."(".$re->countnum.")");
            $datas[]=$data;
        }
        echo json_encode ( $datas );
    }

    
	public function getGenreName(){
        $dbutil = $this->getDB();
        $language = $_POST["language"];
        $sql = "select genre_name,count(genre_name) countnum from game where genre_name is not null and genre_name != '' and language = '".$language."' group by genre_name  ";
        $res = $dbutil->get_results($sql);
        $datas =array();
        foreach ($res as $re){
            $data  = array("genre_name"=>$re->genre_name,"genre_name_num"=>$re->genre_name."(".$re->countnum.")");
            $datas[] =$data;
        }
        echo json_encode ( $datas );
    }
    

    public function gameList(){//页码
        $page = $_POST["page"];
        if($page == null ){
            $page = $_GET["page"];
        }
        //每页显示记录数
        $rows = $_POST["rows"];
        if($rows == null ){
            $rows = $_GET["rows"];
        }
        $start = ($page-1)*$rows ;
        $end = $page*$rows;
        //排序索引id
        $sidx = $_POST["sidx"];
        if($sidx == null ){
            $sidx = $_GET["sidx"];
        }
        //排序顺序
        $sord = $_POST["sord"];
        if($sord == null ){
            $sord = $_GET["sord"];
        }

        //游戏类型
        $gameName = $_POST["gameName"];
        $gameType =   $_POST["gameType"];
        $genreName = $_POST["genreName"];
        $languge = $_POST["languge"];

        $logDesc = $_POST["logDesc"];
        if($logDesc == null ){
            $logDesc = $_GET["logDesc"];
        }
        $startTime = $_POST["startTime"];
        if($startTime == null ){
            $startTime = $_GET["startTime"];
        }
        $endTime = $_POST["endTime"];
        if($endTime == null ){
            $endTime = $_GET["endTime"];
        }

        $dbutil = $this->getDB();

        $countSql = " select count(g.id) total from game g where 1=1  ";
        $sql = " select g.id,g.game_id,g.game_name,g.site,g.genre_name,g.language,g.price,g.gametype ,g.longdesc , g.shortdesc , g.meddesc from game g where 1=1  ";

        if($gameName != null && $gameName != ""){
            $countSql = $countSql." and g.game_name like '%$gameName%' ";
            $sql = $sql." and g.game_name like '%$gameName%' ";
        }
        if($gameType != null && $gameType != ""){
            $countSql = $countSql." and g.gameType like '%$gameType%' ";
            $sql = $sql." and g.gameType like '%$gameType%' ";
        }
        if($genreName != null && $genreName != ""){
        	$countSql = $countSql." and g.genre_name = '$genreName' ";
            $sql = $sql." and g.genre_name = '$genreName' ";
        }
        
        if($languge != null && $languge != ""){
        	$countSql = $countSql." and g.language = '$languge' ";
            $sql = $sql." and g.language = '$languge' ";
        }
        
        if($startTime != null && $startTime != ""){
            $countSql = $countSql." and log.log_time > str_to_date('$startTime','%Y-%m-%d')  ";
            $sql = $sql." and log.log_time > str_to_date('$startTime','%Y-%m-%d')  ";
        }
        if($endTime != null && $endTime != ""){
            $countSql = $countSql." and log.log_time < str_to_date('$endTime','%Y-%m-%d')  ";
            $sql = $sql." and log.log_time < str_to_date('$endTime','%Y-%m-%d')  ";
        }

        $res = $dbutil->get_results($countSql);
        $records = $res;
        foreach ($res as $re){
            $records = $re->total;
            break;
        }

        if($sidx != null && $sidx != "" && $sord != null && $sord != ""){
            $sql = $sql." order by log.$sidx $sord ";
        }else{
            $sql = $sql." order by g.id ";
        }
        $sql = $sql." limit $start ,$rows";

        $res = $dbutil->get_results($sql);
        $data = array();
        include_once 'lang/admin/country.lang.php';
        foreach ($res as $re){
            $data[] = array(
                "id"=>$re->id,
                "cell"=>array(
                    $re->game_id,
                    $re->game_name,
                    $re->genre_name,
                    $re->site,
                    $re->price,
                    $lnstr[$re->language],
                    $re->gametype,
                    $re->shortdesc,
                    $re->meddesc,
                    $re->longdesc,
                    $re->id
                )
            );
        }

        $totalPage = ceil($records/$rows);

        $result = array(
            "rows"=>$data,
            "page"=>$page,
            "total"=>$totalPage,
            "yy"=>$sql,
            "records"=>$records);

        echo json_encode($result);
    }
	public function getMsg() {
		$smaryt = $this->getSmarty ();
		$user = $_SESSION ['aduser'];
		if (empty ( $user->user_image )) {
			$user->user_image = "user-iocn.gif";
		}
		echo json_encode ( $user );
	}
	
	public function edit(){
		$gameid = $_POST["gameid"];
        $shortdesc = $_POST["shortdesc"];
        $meddesc = $_POST["meddesc"];
        $longdesc = $_POST["longdesc"];
        
        $dbutil = $this->getDB();
        
        $data = array(
        	"longdesc"=> $longdesc, 
        	"shortdesc"=> $shortdesc, 
        	"meddesc"=> $longdesc
        );
        
        $where = array(
        	"id"=>$gameid
        );
        
        $dbutil->update("game", $data, $where);
        
		$result = array(
			'success'=>true,
        	"short"=>$shortdesc ,
        	"med"=>$meddesc ,
        	"long"=>$longdesc
		);
		echo json_encode ( $result );
	}
	
	public function modify() {
		$smaryt = $this->getSmarty ();
		require_once SERVICE . DS . 'admin' . DS . 'AdminuserService.class.php';
		$userService = new AdminuserService ( $this->getDB () );
		$password = $_POST ["passwd"];
		$user = $_SESSION ['aduser'];
		if ($password != $user->passwd) {
			$password = MD5 ( $password );
		}
		$input_data = array (
				'user_name' => $_POST ["user_name"],
				'mobile' => $_POST ["mobile"],
				'email' => $_POST ["email"],
				'user_image' => $_POST ["user_image"],
				'passwd' => $password 
		);
		$input_condition = array (
				'id' => $_POST ["id"] 
		);
		
		$user_id = $userService->edit ( $input_data, $input_condition );
		if ($user_id > 0) {
			$newuser = $userService->getAdminByID ( $_POST ["id"] );
			$_SESSION ['aduser'] = $newuser;
		}
		echo $user_id;
	}
}