<?php

class GametagsController extends Controller {

	public function index(){
		$this->getSmarty();
		 include_once 'common/Constant.class.php';
		$this->smarty->assign("str",Constant::loadAdminLanguange($_SESSION["adminlanguage"]));
		$this->smarty->display("admin/tags/game_tags.tpl");
	}
	
	
	public function datalist(){
        require_once SERVICE . DS . 'admin/CommonService.class.php';

        $common = new CommonService($this->getDB());
        $page = isset ( $_POST ['page'] ) ? intval ( $_POST ['page'] ) : 1;
        $rows = isset ( $_POST ['rows'] ) ? intval ( $_POST ['rows'] ) : 10;
        $offset = ($page - 1) * $rows;
        $tagname = $_POST ['tagname'];
        $language = $_POST ['language'];
        $where= 'where 1=1 ';
        if($tagname){
            $where .= " and tag_name like '%$tagname%' ";
        }
		if($language){
            $where .= " and language = '$language' ";
        }
        $result = array ();
        $row_count = $common->countNumWhere("tags",$where);
        
        $result ["total"] = ceil($row_count/$rows);
        $result ["records"] = $row_count;
        $result ["page"] = $page;
        
        $sql = "SELECT t.tag_index,t.language,t.tag_name, t.tag_id, t.tag_img, COUNT(r.tag_id) num FROM tags t , relations r ".$where. " and t.tag_id = r.tag_id";
//         if($where != ''){
//             $sql .= " and t.tag_name like '%$tagname%' ";
//         }
         $sql .=" GROUP BY r.tag_id ORDER BY num desc";
        $data = $common->dataPageSql($sql, $offset, $rows);
        
	 	include_once 'lang/admin/country.lang.php';
        foreach ($data as  $item){
        	$item->language = $lnstr[$item->language];
        }
        $result ["rows"] = $data;
        echo json_encode ( $result );
    }
 	/**
     * 添加 tag
     */
    function addTag(){
        require_once SERVICE . DS . 'admin/TagsService.class.php';
        // START 数据库查询及分页数据
        $tagsService = new TagsService( $this->getDB () );
        $tagname = $_POST ['tagname'];
        $tagimg =   $_POST ['tagimg'];
        $tagid = $tagsService->addGameTag($tagname, $tagimg);
        $data = array("id"=>$tagid,"name"=>$tagname);
        echo json_encode ( $data );
    }
    
    function delTag(){
        require_once SERVICE . DS . 'admin/TagsService.class.php';
        // START 数据库查询及分页数据
        $tagsService = new TagsService( $this->getDB () );
        $tagid = $_POST ['tagid'];
        $tagid = $tagsService->delGameTag($tagid);
        $data = array("success"=>$tagid);
        echo json_encode ( $data );
    }
    function updateTag(){
        $tagname = $_POST ['tagname'];
        $tagimg =   $_POST ['tagimg'];
        $tagid = $_POST ['tagid'];
        $index = $_POST ['index'];
        require_once SERVICE . DS . 'admin/CommonService.class.php';
        $common = new CommonService($this->getDB());
        $data = array("tag_name"=>$tagname,"tag_img"=>$tagimg,"tag_index"=>$index);
        $where = array("tag_id"=>$tagid);
        $common->update("tags",$data,$where);
        $data = array("success"=>$tagid);
        echo json_encode ( $data );
    }
    
    function getTagLans(){
    	require_once SERVICE . DS . 'admin/CommonService.class.php';
        $common = new CommonService($this->getDB());
        $data = $common->query('SELECT DISTINCT(language) FROM  tags ');
        include_once 'lang/admin/country.lang.php';
        foreach ($data as  $item){
        	$item->showlanguage = $lnstr[$item->language];
        }
        echo json_encode ( $data );
    }
}