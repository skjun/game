<?php
class IndexController extends  Controller{

	
	public function index(){
		$this->getSmarty();
		$sql ="select count(*) gamecount,(select count(*) from tags)  tagcount from game";
        $dbutil = $this->getDB();
        $datacount = $dbutil->get_row($sql);
        //任务统计
        $this->smarty->assign("datacount",$datacount);
        //近期变化统计
        $neartsql="SELECT	DATE_FORMAT(po.post_date, '%y-%m-%d') postdate,COUNT(po.ID) postcount,(select count(*) from post_comments where DATE_FORMAT(comment_date, '%y-%m-%d')=postdate) commentcount FROM posts po WHERE	post_date >= DATE_SUB(CURDATE(), INTERVAL 7 DAY) GROUP BY	postdate";
        $neardata = $dbutil->get_results($neartsql,ARRAY_N);
        $this->smarty->assign("neardata",$neardata);
        
    	//统计
		$languagecountsql = "select `language`,count(*) lcount from game g GROUP BY g.`language`";
		$languagecount = $dbutil->get_results($languagecountsql);
		$this->smarty->assign("languagecount",$languagecount);

		
		 include_once 'common/Constant.class.php';
		$this->smarty->assign("str",Constant::loadAdminLanguange($_SESSION["adminlanguage"]));
		$this->smarty->display("admin/index.tpl");
    }
}