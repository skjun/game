﻿<?php
class LoginController extends  Controller{
	
	public function index(){ 
		 $smaryt = $this->getSmarty();
		 
		 
        if(empty( $_SESSION["template"])){
            $_SESSION["template"]="default";
        }
	 	if(empty( $_SESSION['adminlanguage'])){
            $_SESSION["adminlanguage"]="cn";
        }
        include_once 'common/Constant.class.php';
		$this->smarty->assign("str",Constant::loadAdminLanguange($_SESSION["adminlanguage"]));
		
		 //如果不是post方式的提交，直接转向
		 if(!CommonBase::isPost()){
		 	$this->smarty->display("admin/login.tpl"); 
		 	return;
		 }
		  
		 $user_name = $password = $remember = $verify_code = '';
	     extract ( $_POST, EXTR_IF_EXISTS ); 
		 
		 if(strtolower($verify_code) != strtolower($_SESSION['osa_verify_code'])){
		    $alert_html = HtmlWrap::alert("error",ErrorMessage::VERIFY_CODE_WRONG);
			$this->smarty->assign("admin_action_alert",$alert_html);
		 }else{
		 	
		 	require_once SERVICE.DS.'admin/AdminuserService.class.php';
		 	$userSerivce = new AdminuserService($this->getDB());
		 	$user_info = $userSerivce->checkPassword ( $user_name, $password );
		
				if ($user_info) { 
					//得到用户的菜单
                    $_SESSION['aduser']=$user_info;
                    $menus = $this->getLoginUserMenus($user_info);

                     if(empty($menus)){
                        $alert_html = HtmlWrap::alert("error",ErrorMessage::USER_ROLE_WRONG);
                        $this->smarty->assign("admin_action_alert",$alert_html);

                    }
                    $_SESSION['usermenus']= $menus;
                    $result = $this->getSub("0");
                    $_SESSION['siderMenu']= $result;


                    //可以处理用户登录日志
                    require_once SERVICE.DS.'admin/AdminLogService.class.php';
                    $logSerivce = new AdminLogService($this->getDB());
                    $logSerivce->logInfo($user_info->id, "login in", "user login!");

					$url = WEBSITE_URL."admin/index";
	     	        	 	$redirect = "<script language='javascript' type='text/javascript'>";
							$redirect .= "window.location.href='$url'";
							$redirect .= "</script>";
					echo $redirect;
				}else{
					$alert_html = HtmlWrap::alert("error",ErrorMessage::USER_OR_PWD_WRONG);

					$this->smarty->assign("admin_action_alert",$alert_html);
				}
			 
 		 }
		  
		 $this->smarty->display("admin/login.tpl"); 
	}

    /**
     * 写的逻辑还是很清晰的
     */
    private function getSub($pid){

        $data = array();

        $sql = "select * from tb_menus where menu_p_id='$pid' order by menu_create_date asc " ;
        $rss = $this->dbutil->get_results($sql);
        $menus = $_SESSION['usermenus'];
        if($rss != null && count($rss) > 0){
            foreach ($rss as $rs){
                $id = $rs->menu_id;
                if(!in_array($id,$menus)){
                    continue;
                }
                $data[] = array(
                    "menu_id"=>$id,
                    "menu_name"=>$rs->menu_name,
                    "menu_href"=>$rs->menu_href,
                    "child"=> $this->getSub($id)
                );
            }
        }

        return $data;
    }
    public function getLoginUserMenus($user){
        require_once SERVICE . DS . 'admin/CommonService.class.php';
        $commonService = new CommonService( $this->getDB () );
//        $findrole = "select role_id from tb_user_role where user_id=".$userid;
//        $roles =$commonService->query($findrole);
//        if(!empty($roles)){
//            $roleid = $roles[0]->role_id;

            $findmenus = "select menu_id from tb_role_menu where role_id=".$user->role_id;
            $menus =$commonService->query($findmenus);
            $menusstr = array();
            foreach ($menus as $menu){
                $id = $menu->menu_id;
                $menusstr[] = $id;
            }
            return $menusstr;
    }

    public function loginout(){ 
    	$user_info = $_SESSION['aduser'];
    	require_once SERVICE.DS.'admin/AdminLogService.class.php';
    	$logSerivce = new AdminLogService($this->getDB());
    	$logSerivce->logInfo($user_info->id, "login out", "user logout!");
    	
		   	$_SESSION['aduser']= NULL;
            $_SESSION['usermenus'] = NULL;
            $_SESSION['siderMenu'] = NULL;
		   $smaryt = $this->getSmarty();  
	        $this->smarty->display("admin/login.tpl"); 
		   
	}
	 
}