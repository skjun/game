<?php
class SeoController extends Controller {
	public function index() {
		$this->getSmarty ();
			
		$sqlParamSql = "select id,name,value from tb_system_param where uiFlag='0'";
		require_once SERVICE . DS . 'admin/CommonService.class.php';
		$commonService = new CommonService( $this->getDB() );
		$params = $commonService->query($sqlParamSql);
		$paramkv = array();
		for($i=0;$i<count($params);$i++){
			$paramkv[$params[$i]->name] = $params[$i]->value;
		}
		 include_once 'common/Constant.class.php';
		$this->smarty->assign("str",Constant::loadAdminLanguange($_SESSION["adminlanguage"]));
		$this->smarty->assign("params",$paramkv);
		$this->smarty->display ( "admin/seo/seo.tpl" );
	}


	/**
	 * 用户网站SEO参数保存
	 * Enter description here ...
	 */
	public function saveSeoParams(){
		require_once SERVICE . DS . 'admin/CommonDataService.class.php';
		$service = new CommonDataService ( $this->getDB () );
		foreach($_POST as $key => $value)
		{
			$result = $service->getResult("select * from tb_system_param where name = '".$key."'");
			if($result == null){
				//插入操作
				$input_data = array (
	 			'name' => $key,
				'value' =>$value,
				'uiFlag'=>'0'
				);
				$service->add("tb_system_param", $input_data);
			}else{
				//更新操作
				$input_data = array (
						'value' => $value,
				);
				$input_condition = array (
						'name' => $key
				);
				$service->edit("tb_system_param", $input_data, $input_condition);
			}
		}
	
		$result = array("success"=>1);
		echo json_encode($result);
	}
	public function tongji() {
		$this->getSmarty ();
		require_once SERVICE . DS . 'admin/CommonService.class.php';
		$commonService = new CommonService( $this->getDB() );
		$params = $commonService->query("select id,name,value from tb_system_param  where name ='tongji'");
		$this->smarty->assign("param",$params[0]);
		$this->smarty->display ( "admin/seo/tongji.tpl" );
	}
	function updatetongji() {
		$this->getSmarty ();
		require_once SERVICE . DS . 'admin/CommonService.class.php';
		$commonService = new CommonService( $this->getDB() );
		if (! CommonBase::isPost ()) {
			$this->smarty->display ( "admin/seo/tongji.tpl" );
			return;
		}
		$tongji_value = '';
		extract ( $_POST, EXTR_IF_EXISTS );
		$commonService->query("update tb_system_param set value='".addslashes($tongji_value)."',paramdesc='网站统计代码' where name='tongji'");
		$result = array("success"=>true);

		echo json_encode($result);
	}



}