<?php
class SitelanguageController extends Controller{

	public function index(){
		$this->getSmarty ();
		include_once 'common/Constant.class.php';
		$this->smarty->assign("str",Constant::loadAdminLanguange($_SESSION["adminlanguage"]));
		
		$this->smarty->assign("languages",$this->listLanguage());
		$this->smarty->display ( "admin/sitelanguage.tpl" );
	}

	public function modifyDefaultLanguage(){
		$lan = $_POST['ln'];
		$this->saveLanParams($lan);
		echo "1";
	}

	public function listLanguage() {
		$key = 'defaultgamelanguage';
		require_once SERVICE . DS . "IndexDataService.class.php";
		include_once 'lang/admin/country.lang.php';
		require_once SERVICE . DS . 'admin/CommonDataService.class.php';
		$service = new CommonDataService ( $this->getDB () );
		$result = $service->getResult("select value from tb_system_param where name = '".$key."'");
		
		$common = new IndexDataService ( $this->getDB ());
		$datas=array();
		$language = $common->getGameLans();
		foreach ($language as $value){
			if($value->language == $result->value){
				$lreults=array(
					'lan'=>$value->language,
					'select'=>"selected",
					'name'=>$lnstr[$value->language]
				);
			}else {
				$lreults=array(
					'lan'=>$value->language,
					'name'=>$lnstr[$value->language]
				);
			}
			$datas[] = $lreults;
		}
		return $datas;
	}

	public function saveLanParams($lan){
		$key = 'defaultgamelanguage';
		require_once SERVICE . DS . 'admin/CommonDataService.class.php';
		$service = new CommonDataService ( $this->getDB () );
		$result = $service->getResult("select * from tb_system_param where name = '".$key."'");
		if($result == null){
			//插入操作
			$input_data = array (
	 			'name' => $key,
				'value' =>$lan
			);
			$service->add("tb_system_param", $input_data);
		}else{
			//更新操作
			$input_data = array (
						'value' => $lan,
			);
			$input_condition = array (
						'name' => $key
			);
			$service->edit("tb_system_param", $input_data, $input_condition);
		}

	}
}