<?php
class SitemapController extends Controller {
	public function index() {
		$this->getSmarty ();
        $sqlParamSql = "select * from sitemap";
        require_once SERVICE . DS . 'admin/CommonService.class.php';
        $commonService = new CommonService( $this->getDB() );
        $params = $commonService->query($sqlParamSql);

        $prioritys = array("1.0","0.9","0.8","0.7","0.6","0.5","0.4","0.3","0.2","0.1");
        $changefreqs = array("always","hourly","daily","weekly","monthly","yearly","never");

        $this->smarty->assign("sitemaps",$params);
        $this->smarty->assign("prioritys",$prioritys);
        $this->smarty->assign("changefreqs",$changefreqs);
        
        
        include_once 'common/Constant.class.php';
		$this->smarty->assign("str",Constant::loadAdminLanguange($_SESSION["adminlanguage"]));
		$this->smarty->display ( "admin/seo/sitemap.tpl" );
	}


	public function update() {
		$this->getSmarty ();
        require_once SERVICE . DS . 'admin/CommonService.class.php';
        $commonService = new CommonService( $this->getDB() );
        if (! CommonBase::isPost ()) {
            $this->smarty->display ( "admin/seo/seo.tpl" );
            return;
        }
        $index_changefreq = $index_priority = $category_changefreq = $category_priority = $tag_changefreq =
        $tag_priority = $post_changefreq = $post_priority = $otherpage_changefreq = $otherpage_priority ='';
        extract ( $_POST, EXTR_IF_EXISTS );
       $commonService->query("update sitemap set changefreq='$index_changefreq',priority='$index_priority' where id='index'");
        $commonService->query("update sitemap set changefreq='$category_changefreq',priority='$category_priority' where id='category'");
        $commonService->query("update sitemap set changefreq='$tag_changefreq',priority='$tag_priority' where id='tag'");
         $commonService->query("update sitemap set changefreq='$post_changefreq',priority='$post_priority' where id='post'");
        $commonService->query("update sitemap set changefreq='$otherpage_changefreq',priority='$otherpage_priority' where id='otherpage'");
         $result = array("success"=>true);


		echo json_encode($result);
	}
    public function mksitemap(){
        $this->getSmarty ();
        require_once SERVICE . DS . 'admin/SitemapService.class.php';
        $sitemapService = new SitemapService( $this->getDB() );
        $urls = $sitemapService->createSitemapData();
        var_dump($urls);
        $this->smarty->assign("sitemaps",$urls);
        $content = $this->smarty->fetch("sitemap.xml");
        var_dump($content);
        $file_name = PROJECT."/sitemap.xml";
        $fp = fopen($file_name, "w");
        fwrite($fp, $content);
        fclose($fp);
        $result = array("success"=>true);

        echo json_encode($result);
    }

	
	
}