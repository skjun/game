<?php
class TagsController extends  Controller{

	public function index(){
		$this->getSmarty();
		
		
		include_once 'common/Constant.class.php';
		$this->smarty->assign("str",Constant::loadAdminLanguange($_SESSION["adminlanguage"]));
		$this->smarty->display("admin/tags/tags.tpl");
	}
	public function datalist(){
        require_once SERVICE . DS . 'admin/CommonService.class.php';
        //hand where condition
        $where = $tagname = '';
        extract ( $_POST, EXTR_IF_EXISTS );
        if($tagname != '') {
            $where = ' where ';
            if($tagname !=''){
                $where .= " tag_name like '%".$tagname."%'";
            }
        }

        $common = new CommonService($this->getDB());
        $page = isset ( $_POST ['page'] ) ? intval ( $_POST ['page'] ) : 1;
        $rows = isset ( $_POST ['rows'] ) ? intval ( $_POST ['rows'] ) : 10;
        $offset = ($page - 1) * $rows;
        $result = array ();
        $row_count = $common->countNumWhere("tags",$where);
        $result ["total"] = ceil($row_count/$rows);
        $result ["records"] = $row_count;
        $result ["page"] = $page;
        $user_infos = $common->dataPageWhere('tags', $offset, $rows,$where);
        $items = array ();
        $result ["rows"] = $user_infos;
        echo json_encode ( $result );
    }

	/**
	 * 根据文章id获取标签tags
	 * @deprecated
	 * 暂时不使用个方法
	 */
	function getTags4PostsID(){
		$this->getSmarty();
		require_once SERVICE . DS . 'admin/TagsService.class.php';
		// START 数据库查询及分页数据
		$tagsService = new TagsService( $this->getDB () );

		$postsID = $_POST ['postsID'];

		$tags = $tagsService->getTags ($postsID);
		
		echo json_encode ( $tags );
	}

    /**
     * 添加 tag
     */
    function addTag(){
        require_once SERVICE . DS . 'admin/TagsService.class.php';
        // START 数据库查询及分页数据
        $tagsService = new TagsService( $this->getDB () );
        $tagname = $_POST ['tagname'];
        $desc =   $_POST ['desc'];
        $tagid = $tagsService->addTag($tagname,$desc);
        $data = array("id"=>$tagid,"name"=>$tagname);
        echo json_encode ( $data );
    }
    function delTag(){
        require_once SERVICE . DS . 'admin/TagsService.class.php';
        // START 数据库查询及分页数据
        $tagsService = new TagsService( $this->getDB () );
        $tagid = $_POST ['tagid'];
        $tagid = $tagsService->delTag($tagid);
        $data = array("success"=>$tagid);
        echo json_encode ( $data );
    }
    function updateTag(){
        $tagname = $_POST ['tagname'];
        $desc =   $_POST ['desc'];
        $tagid = $_POST ['tagid'];
        require_once SERVICE . DS . 'admin/CommonService.class.php';
        $common = new CommonService($this->getDB());
        $data = array("tag_name"=>$tagname,"description"=>$desc);
        $where = array("tag_id"=>$tagid);
        $common->update("tags",$data,$where);
        $data = array("success"=>$tagid);
        echo json_encode ( $data );

    }
}