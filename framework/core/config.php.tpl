<?php
error_reporting(E_ERROR | E_PARSE);
//error_reporting(E_ALL & ~E_STRICT & ~E_DEPRECATED);
//ini_set("display_errors", 0);
// define('SMARTY_DIR', PROJECT.'/framework/lib/Smarty-3.1.13/libs/');
//

define('ADMIN_TITLE', "game manager");
define('PROJECT_NAME', "game manager");
//define('WEBSITE_URL','http://localhost/');
define('WEBSITE_URL','${project_website_name}');

//for include smarty
ini_set('include_path', ini_get('include_path').PATH_SEPARATOR.SMARTY_DIR);
date_default_timezone_set('Asia/Shanghai');


$CONFIG['DB']= ARRAY(
	  'db_host'           =>      '${db_host}',
      'db_user'           =>      '${db_user}', 
      'db_password'       =>      '${db_password}',
      'db_database'       =>      '${db_database}',
      'db_table_prefix'   =>      'app_',
      'db_charset'        =>      'utf8'  
);

$CONFIG['DATA_SERVER']= ARRAY(
'SERVER_BASE_URL'           =>     'http://119.81.166.243:8080/gamesitebuilder/',
'SERVER_USERNAME'           =>      '${server_username}',
'SERVER_PWD'       =>      '${server_pwd}',
'SERVER_USER_ID'       =>      '${server_uid}',
'SERVER_WEBSITE_NAME'   =>      '${website_name}',
'SERVER_WEBSITE_ID'        =>      '${servier_wid}'
);
$CONFIG['SMARTY']= array(
    'templates'         =>      PROJECT.'/templates/',
    'templates_c'       =>      PROJECT.'/templates_c/',
    'configs'            =>      PROJECT.'/core/',
    'check'            =>      true,
    'cache'             =>      PROJECT.'/cache/',
    'left_delimiter'    =>      '{{',
    'right_delimiter'   =>      '}}',
    'debugging'         =>      false
// 'caching'           =>      Smarty::CACHING_OFF
);
$CONFIG['ROUTER']= array(
    "dir"               => "",
    "controler"         => "index",
    "method"            => "index",
    "param"             => ""
);

