<?php
/**
 *
 * Created by PhpStorm.
 * User: yuyue
 * Date: 2015/1/17
 * Time: 23:57
 */
require_once MODEL . DS . "UserDTO.php";

class StatisticReqDTO extends UserDTO {

    public $gameId;
    public $statisType;//0 为浏览 1 为下载或购买
} 