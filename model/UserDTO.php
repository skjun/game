<?php
/**
 * Created by PhpStorm.
 * User: think
 * Date: 2015/1/17
 * Time: 23:57
 *
 * 进行修改，添加user与site的主键，在后绪的接口交互中，还是使用主键最方便
 * 没有主键，后绪每次都还需要查询
 */
class UserDTO {
    public $username;
    public $password;
    public $userId;
    public $websiteName;
    public $websiteId;
} 