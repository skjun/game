/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50620
Source Host           : localhost:3306
Source Database       : game_client_dbs

Target Server Type    : MYSQL
Target Server Version : 50620
File Encoding         : 65001

Date: 2015-01-29 18:33:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `admin_log`
-- ----------------------------
DROP TABLE IF EXISTS `admin_log`;
CREATE TABLE `admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `log_type` varchar(255) NOT NULL,
  `log_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `log_desc` varchar(255) NOT NULL DEFAULT '',
  `admin_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin_log
-- ----------------------------

-- ----------------------------
-- Table structure for `admin_users`
-- ----------------------------
DROP TABLE IF EXISTS `admin_users`;
CREATE TABLE `admin_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `real_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `login_time` timestamp NULL DEFAULT NULL,
  `login_ip` varchar(255) DEFAULT NULL,
  `passwd` varchar(255) DEFAULT NULL,
  `user_desc` varchar(255) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `user_image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin_users
-- ----------------------------
INSERT INTO `admin_users` VALUES ('15', 'admin', null, 'admin', null, null, null, 'e10adc3949ba59abbe56e057f20f883e', null, null, null);

-- ----------------------------
-- Table structure for `connects`
-- ----------------------------
DROP TABLE IF EXISTS `connects`;
CREATE TABLE `connects` (
  `ID` varchar(128) NOT NULL,
  `NAME` varchar(250) DEFAULT NULL,
  `EMAIL` varchar(128) DEFAULT NULL,
  `PHONE` varchar(128) DEFAULT NULL,
  `WEBSITE` varchar(250) DEFAULT NULL,
  `MESSAGE` varchar(250) DEFAULT NULL,
  `DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of connects
-- ----------------------------

-- ----------------------------
-- Table structure for `emails`
-- ----------------------------
DROP TABLE IF EXISTS `emails`;
CREATE TABLE `emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `email_origin` varchar(255) NOT NULL,
  `create_date` date DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `post_code` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of emails
-- ----------------------------

-- ----------------------------
-- Table structure for `friendlinks`
-- ----------------------------
DROP TABLE IF EXISTS `friendlinks`;
CREATE TABLE `friendlinks` (
  `id` varchar(250) NOT NULL DEFAULT '',
  `siteurl` varchar(250) DEFAULT NULL,
  `sitename` varchar(250) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `sitelogo` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of friendlinks
-- ----------------------------
INSERT INTO `friendlinks` VALUES ('1', 'http://www.gamesitebuilder.com', 'gamesitebuilder', '2015-01-26 17:57:27', 'blogi1');

-- ----------------------------
-- Table structure for `game`
-- ----------------------------
DROP TABLE IF EXISTS `game`;
CREATE TABLE `game` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `game_id` varchar(128) NOT NULL DEFAULT '',
  `game_name` varchar(100) DEFAULT NULL,
  `family` varchar(300) DEFAULT NULL,
  `genre_name` varchar(50) DEFAULT NULL,
  `shortdesc` varchar(1024) DEFAULT NULL,
  `meddesc` text,
  `price` varchar(50) DEFAULT NULL,
  `gamerank` int(11) DEFAULT NULL,
  `releasedate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `gamesize` varchar(30) DEFAULT NULL,
  `language` varchar(20) DEFAULT NULL,
  `longdesc` text,
  `site` varchar(10) DEFAULT NULL,
  `gametype` varchar(10) DEFAULT NULL,
  `systemreq` varchar(300) DEFAULT NULL,
  `bullet1` varchar(300) DEFAULT NULL,
  `bullet2` varchar(300) DEFAULT NULL,
  `bullet3` varchar(300) DEFAULT NULL,
  `bullet4` varchar(300) DEFAULT NULL,
  `bullet5` varchar(1024) DEFAULT NULL,
  `img_small` varchar(300) DEFAULT NULL,
  `img_med` varchar(300) DEFAULT NULL,
  `img_subfeature` varchar(300) DEFAULT NULL,
  `img_feature` varchar(300) DEFAULT NULL,
  `img_thumb1` varchar(300) DEFAULT NULL,
  `img_thumb2` varchar(300) DEFAULT NULL,
  `img_thumb3` varchar(300) DEFAULT NULL,
  `img_screen1` varchar(300) DEFAULT NULL,
  `img_screen2` varchar(300) DEFAULT NULL,
  `img_screen3` varchar(300) DEFAULT NULL,
  `video` varchar(300) DEFAULT NULL,
  `flash` varchar(300) DEFAULT NULL,
  `downloadurl` varchar(300) DEFAULT NULL,
  `buyurl` varchar(300) DEFAULT NULL,
  `downloadiframe` varchar(300) DEFAULT NULL,
  `buyiframe` varchar(300) DEFAULT NULL,
  `foldername` varchar(300) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `img_dw_feature` varchar(300) DEFAULT NULL,
  `flash_dw_feature` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26987 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of game
-- ----------------------------

-- ----------------------------
-- Table structure for `game_catch`
-- ----------------------------
DROP TABLE IF EXISTS `game_catch`;
CREATE TABLE `game_catch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `g_id` int(11) DEFAULT NULL,
  `game_id` varchar(128) NOT NULL DEFAULT '',
  `language` varchar(20) DEFAULT NULL,
  `site` varchar(10) DEFAULT NULL,
  `gametype` varchar(10) DEFAULT NULL,
  `logo_url` varchar(200) DEFAULT NULL,
  `images_med` varchar(500) DEFAULT NULL,
  `tagline` varchar(100) DEFAULT NULL,
  `offer_start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `offer_end_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link` varchar(500) DEFAULT NULL,
  `price` varchar(50) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of game_catch
-- ----------------------------

-- ----------------------------
-- Table structure for `game_daily`
-- ----------------------------
DROP TABLE IF EXISTS `game_daily`;
CREATE TABLE `game_daily` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `g_id` int(11) DEFAULT NULL,
  `game_id` varchar(128) NOT NULL DEFAULT '',
  `language` varchar(20) DEFAULT NULL,
  `site` varchar(10) DEFAULT NULL,
  `gametype` varchar(10) DEFAULT NULL,
  `content` varchar(50) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `link` varchar(400) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  `description` text,
  `pub_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6472 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of game_daily
-- ----------------------------

-- ----------------------------
-- Table structure for `game_feature`
-- ----------------------------
DROP TABLE IF EXISTS `game_feature`;
CREATE TABLE `game_feature` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `g_id` int(11) DEFAULT NULL,
  `game_id` varchar(128) NOT NULL DEFAULT '',
  `language` varchar(20) DEFAULT NULL,
  `site` varchar(10) DEFAULT NULL,
  `gametype` varchar(10) DEFAULT NULL,
  `hasdwfeature` varchar(10) DEFAULT NULL,
  `dwwidth` varchar(10) DEFAULT NULL,
  `dwheight` varchar(10) DEFAULT NULL,
  `gamerank` varchar(10) DEFAULT NULL,
  `releasedate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=844 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of game_feature
-- ----------------------------

-- ----------------------------
-- Table structure for `mail_config`
-- ----------------------------
DROP TABLE IF EXISTS `mail_config`;
CREATE TABLE `mail_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `smtp` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `count_use` int(11) DEFAULT NULL,
  `count_total` int(11) DEFAULT NULL,
  `time` date DEFAULT NULL,
  `sitename` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `desc` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `port` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mail_config
-- ----------------------------

-- ----------------------------
-- Table structure for `posts`
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `post_author` int(11) DEFAULT NULL,
  `post_date` timestamp NULL DEFAULT NULL,
  `post_content` text,
  `post_title` varchar(1024) DEFAULT NULL,
  `post_excerpt` varchar(1024) DEFAULT NULL,
  `post_status` int(11) DEFAULT NULL,
  `post_file_location` varchar(1024) DEFAULT NULL,
  `post_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `post_category` int(11) DEFAULT NULL,
  `comment_count` int(11) DEFAULT NULL,
  `post_markdowncontent` text,
  `tags` varchar(255) DEFAULT NULL,
  `click_count` int(11) DEFAULT '0',
  `agree_count` int(11) DEFAULT '0',
  `post_link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=486 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of posts
-- ----------------------------

-- ----------------------------
-- Table structure for `post_category`
-- ----------------------------
DROP TABLE IF EXISTS `post_category`;
CREATE TABLE `post_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(1024) DEFAULT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `c_index` int(11) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of post_category
-- ----------------------------

-- ----------------------------
-- Table structure for `post_comments`
-- ----------------------------
DROP TABLE IF EXISTS `post_comments`;
CREATE TABLE `post_comments` (
  `comment_ID` int(11) NOT NULL AUTO_INCREMENT,
  `comment_post_ID` int(11) DEFAULT NULL,
  `comment_author_IP` varchar(64) DEFAULT NULL,
  `comment_date` timestamp NULL DEFAULT NULL,
  `comment_content` varchar(1024) DEFAULT NULL,
  `comment_approved` int(11) DEFAULT NULL,
  `comment_agent` varchar(1024) DEFAULT NULL,
  `comment_parent` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`comment_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of post_comments
-- ----------------------------

-- ----------------------------
-- Table structure for `post_tag`
-- ----------------------------
DROP TABLE IF EXISTS `post_tag`;
CREATE TABLE `post_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT NULL,
  `tag_id` int(11) DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1134 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of post_tag
-- ----------------------------

-- ----------------------------
-- Table structure for `relations`
-- ----------------------------
DROP TABLE IF EXISTS `relations`;
CREATE TABLE `relations` (
  `tag_id` int(11) NOT NULL,
  `obj_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of relations
-- ----------------------------

-- ----------------------------
-- Table structure for `sitemap`
-- ----------------------------
DROP TABLE IF EXISTS `sitemap`;
CREATE TABLE `sitemap` (
  `id` varchar(128) NOT NULL DEFAULT '',
  `type` varchar(128) DEFAULT NULL,
  `changefreq` varchar(128) DEFAULT NULL,
  `priority` varchar(20) DEFAULT NULL,
  `changtime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sitemap
-- ----------------------------

-- ----------------------------
-- Table structure for `tags`
-- ----------------------------
DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(100) NOT NULL DEFAULT '',
  `tag_img` varchar(200) DEFAULT NULL,
  `language` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8182 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tags
-- ----------------------------

-- ----------------------------
-- Table structure for `tb_system_param`
-- ----------------------------
DROP TABLE IF EXISTS `tb_system_param`;
CREATE TABLE `tb_system_param` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `paramdesc` varchar(255) DEFAULT NULL,
  `value` varchar(1024) DEFAULT NULL,
  `uiFlag` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_system_param
-- ----------------------------

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `userid` varchar(128) NOT NULL DEFAULT '',
  `username` varchar(128) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  `login_type` int(11) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `firstname` varchar(128) DEFAULT NULL,
  `lastname` varchar(128) DEFAULT NULL,
  `birthdate` timestamp NULL DEFAULT NULL,
  `desc` varchar(1024) DEFAULT NULL,
  `tworfb` varchar(256) DEFAULT NULL,
  `contact_email` varchar(128) DEFAULT NULL,
  `paypal` varchar(256) DEFAULT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `user_photo` varchar(1024) DEFAULT NULL,
  `reg_date` datetime DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
