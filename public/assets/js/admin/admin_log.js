$(function() {
	$.datepicker.regional['zh-CN'] = {
            closeText: '关闭',
            prevText: '<上月',
            nextText: '下月>',
            currentText: '今天',
            monthNames: ['一月','二月','三月','四月','五月','六月',
            '七月','八月','九月','十月','十一月','十二月'],
            monthNamesShort: ['一','二','三','四','五','六',
            '七','八','九','十','十一','十二'],
            dayNames: ['星期日','星期一','星期二','星期三','星期四','星期五','星期六'],
            dayNamesShort: ['周日','周一','周二','周三','周四','周五','周六'],
            dayNamesMin: ['日','一','二','三','四','五','六'],
            weekHeader: '周',
            dateFormat: 'yy-mm-dd',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: true,
            yearSuffix: '年'};
    $.datepicker.setDefaults($.datepicker.regional['zh-CN']);
    
    $("#startTime" ).datepicker();
	$("#endTime" ).datepicker(); 
	
	gridCompnent.init();
});
var gridCompnent = function(){
	
	var setting = {
			url:'/admin/adminlog/logList',
			datatype: "json",
			mtype: "POST",	
			height: '100%',
			width:1000,
			rowNum: 20,
			rowList: [10,20,30],
			colNames:['日志ID','用户','日志类型','描述','时间'],
			colModel:[
				{name:'id',index:'id', width:100,hidden:true,search:false},
				{name:'user_name',index:'user_name', width:100},
				{name:'log_type',index:'log_type', width:100},
				{name:'log_desc',index:'log_desc', width:100},
				{name:'log_time',index:'log_time', width:100}
			],
			pager: "#gridPager",
			viewrecords: true,
			caption: "用户日志"
	};
	
	return {
		init : function() {
			jQuery("#grid").jqGrid(setting);
			
			jQuery("#grid").navGrid('#gridPager',{
				edit:false,
				add:false,
				del:false,
				search:false
			});
			
			//注册搜索
			$("#form_search").click(function(){
				var logType = $("#log_type").val();
				var logDesc = $("#log_desc").val();
				var startTime = $("#startTime").val();
				var endTime = $("#endTime").val();
				
				$("#grid").jqGrid('setGridParam',{ 
					url:'/admin/adminlog/logList',
		            postData:{'logType':logType,'logDesc':logDesc,'startTime':startTime,'endTime':endTime},
		            page:1 
		        }).trigger("reloadGrid");

			});
			$("#reset").click(function(){
				$("#log_type").val('');
				$("#log_desc").val('');
				$("#startTime").val('');
				$("#endTime").val('');
			});

		}
	};
	
}();
