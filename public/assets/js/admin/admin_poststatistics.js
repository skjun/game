/**
 * Created by yuyang on 14-1-13.
 */
$(function(){
    //这里是一切程序的入口，把所有的代码都写在这里，看起来是不是很不舒服？
    //我以前写过，初级选手可以按object的方式封装，中级一点就是用上闭包
    grid.gridinit();
    grid.loadgrid();
    dataPickerLanguage();

});
//封装一个对grid统一操作的对象，先写出如下：
//var grid = (function(){})();
var grid = (function(){
    function gridinit(){
        $('#s_date_from').datepicker({
            format: 'yyyy-mm-dd'
        });
        $('#s_date_to').datepicker({
            format: 'yyyy-mm-dd'
        });
        $("#find_btn").click(function(){
            var user_name = $("#user_name").val();
            var date_from = $("#s_date_from").val();
            var date_to = $("#s_date_to").val();
            //$("#girdid").jqGrid('参数名',{相关参数}).trigger("reloadGrid");这个写法要理解一下，是搜索的时候重新载入数据
            $("#postdata").jqGrid('setGridParam',{
                url:"/admin/poststatistics/userPostCountList",
                postData:{'user_name':user_name,'date_from':date_from,'date_to':date_to}, //发送数据
                page:1
            }).trigger("reloadGrid"); //重新载入
        });
    }
    function loadData(){
        jQuery("#postdata").jqGrid({
            url:"/admin/poststatistics/userPostCountList",
            datatype:"json",
            mtype:"post",
            colNames:["作者","数量",'时间段','用户ID'],
            colModel:[
                {name:"user_name",index:"user_name"},
                {name:"postcount",index:"postcount"},
                {name:"userdate",index:"userdate",sortable:false},
                {name:"id",index:"id",hidden:true,search:false}
            ],
            pager:"#postcount",
            rowNum:10,
            rowList:[10,15,20],
            sortname:"postcount",
            sortorder:"desc",
            viewrecords:true,
            caption:"用户发表文章数量统计",
            width:1000,
            height:500,
            rownumbers:true,
            onCellSelect:function(rowid,iCol,cellcontent,e){
                   detail(rowid,iCol,cellcontent,e);
            }
        });
        jQuery("#postdata").navGrid('#postcount',{
            edit:false,
            add:false,
            del:false,
            search:false
        });
    }
 return {loadgrid:loadData,gridinit:gridinit};

})();
//点击后查询用户发表的文章的详细信息
function detail(rowid,iCol,cellcontent,e){
    var user_id = $("#postdata").getCell(rowid,4);
    var date_from = $("#s_date_from").val();
    var date_to = $("#s_date_to").val();

     var   postData={'user_id':user_id,'date_from':date_from,'date_to':date_to};
    $("#userpost").jqGrid('GridUnload');
    jQuery("#userpost").jqGrid({
        url:"/admin/poststatistics/getuserpost/user_id="+user_id+"&date_from="+date_from+"&date_to="+date_to,
        datatype:"json",
        mtype:"post",
        colNames:["标题","日期"," "],
        colModel:[
            {name:"post_title",index:"post_title",width:450},
            {name:"post_date",index:"post_date",width:190},
            {name:"href",index:"href",sortable:false,search:false,width:80}
        ],
        pager:"#postpage",
        rowNum:20,
        rowList:[20,30,40],
        sortname:"post_date",
        sortorder:"desc",
        viewrecords:true,
        width:768,
        height:500,
        rownumbers:true
    });

    jQuery("#userpost").navGrid('#userpost',{
        edit:false,
        add:false,
        del:false,
        search:false
    });
    $.layer({
        type:1,
        shade : [0.5 , '#000' , true],
        fadeIn: 300,
        title : ['详细信息' ,true],
        offset : ['200px' , '200px'],
        area : ['770px' , '600px'],
        fix : false,
        page: {
            dom: '#zhanshi'
        }
    });

}
//用户发表文件页面，关闭页面弹出窗口
function  close(id){
    $('#'+id).modal('hide');
}

function dataPickerLanguage(){
    $.datepicker.regional['zh-CN'] = {
        closeText: '关闭',
        prevText: '<上月',
        nextText: '下月>',
        currentText: '今天',
        monthNames: ['一月','二月','三月','四月','五月','六月',
            '七月','八月','九月','十月','十一月','十二月'],
        monthNamesShort: ['一','二','三','四','五','六',
            '七','八','九','十','十一','十二'],
        dayNames: ['星期日','星期一','星期二','星期三','星期四','星期五','星期六'],
        dayNamesShort: ['周日','周一','周二','周三','周四','周五','周六'],
        dayNamesMin: ['日','一','二','三','四','五','六'],
        weekHeader: '周',
        dateFormat: 'yy-mm-dd',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: true,
        yearSuffix: '年'};
    $.datepicker.setDefaults($.datepicker.regional['zh-CN']);
}