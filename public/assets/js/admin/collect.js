$(function() {
	$("#index").click(createOpt.createIndex);
});

var createOpt = (function(){
    function createIndex(){
        $("#loading").removeClass("hide");
        $.ajax({
            type: "post",
            url: "/admin/collect/collect",
            dataType: "json",
            success: function (data){
                if(data.success){
                    $("#loading").addClass("hide");
                    $.layer({
                        area : ['auto','auto'],
                        dialog : {msg:'collect success！',type : 1}
                    });
                }
            },
            error:function(obj){
                layer.msg("this form parameter have error ,can not setup database script ");
                $("#loading").addClass("hide");
            }
        });
    };
    
    return {
        "createIndex":createIndex
    }

})();