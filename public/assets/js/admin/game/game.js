$(function() {
	$.datepicker.regional['zh-CN'] = {
            closeText: '关闭',
            prevText: '<上月',
            nextText: '下月>',
            currentText: '今天',
            monthNames: ['一月','二月','三月','四月','五月','六月',
            '七月','八月','九月','十月','十一月','十二月'],
            monthNamesShort: ['一','二','三','四','五','六',
            '七','八','九','十','十一','十二'],
            dayNames: ['星期日','星期一','星期二','星期三','星期四','星期五','星期六'],
            dayNamesShort: ['周日','周一','周二','周三','周四','周五','周六'],
            dayNamesMin: ['日','一','二','三','四','五','六'],
            weekHeader: '周',
            dateFormat: 'yy-mm-dd',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: true,
            yearSuffix: '年'};
    $.datepicker.setDefaults($.datepicker.regional['zh-CN']);
    
    $("#startTime" ).datepicker();
	$("#endTime" ).datepicker(); 
	
	gridCompnent.init();
	
	select.init();

});
var select = function(){
	return {
		genreInit:function(language){
			$("#genre_name").html('');
			$.ajax({
	            type: "POST",
	            url: '/admin/game/getGenreName',
	            data:{'language':language},
	           	async:false,
	            dataType: "JSON",
	            success:function(data){
					var content = "<option value=''></option>";
					for(var key in data){
						content += "<option value='"+data[key].genre_name+"'>"+data[key].genre_name_num+"</option>";
					}
					$("#genre_name").html(content);
					
				},
	            error: function () {
	            }
	        });
		},
		init:function(){
			$.ajax({
	            type: "POST",
	            url: '/admin/game/getLanguage',
	           	async:false,
	            dataType: "JSON",
	            success:function(data){
					var content = "<option value=''></option>";
					var lang = null;
					for(var key in data){
						if(lang == null){
							lang = data[key].language;
						}
						content += "<option value='"+data[key].language+"'>"+data[key].showlanguage+"</option>";
					}
					$("#languge").html(content);
					
					select.genreInit(lang);
				},
	            error: function () {
	            }
	        });
			
			$("#languge").change(function (){
				var lang = $("#languge").val();
				select.genreInit(lang);
			});
			
		}
	}
}();

var gridCompnent = function(){
	
	window.mdfdesc = function(id){
		var rowObject = gridCompnent.dataSource[id];
		var gamename = rowObject[1];
		var shordesc = rowObject[6];
		var meddesc = rowObject[7];
		var longdesc = rowObject[8];
		$("#gameid").val(id);
		$("#gamename").val(gamename);
		$("#shortdesc").val(shordesc);
		$("#meddesc").val(meddesc);
		$("#longdesc").val(longdesc);
		
		$("#myModal2").modal('show');
	};
	
	var setting = {
			url:'/admin/game/gameList',
			datatype: "json",
			mtype: "POST",	
			height: '100%',
			width:1000,
			rowNum: 20,
			rowList: [10,20,30],
			colNames:['id','游戏名称','游戏类型','所属联盟','游戏价格','游戏语言','游戏平台','简短描述','中描述','长描述','操作'],
			colModel:[
				{name:'game_id',index:'game_id', width:100,hidden:true,search:false},
				{name:'game_name',index:'game_name', width:100},
				{name:'genre_name',index:'genre_name', width:100},
				{name:'site',index:'site', width:100},
				{name:'price',index:'price', width:100},
                {name:'language',index:'language', width:100},
				{name:'gametype',index:'game_type', width:100},
				{name:'shortdesc',index:'shortdesc', width:100,hidden:true},
				{name:'meddesc',index:'meddesc', width:100,hidden:true},
				{name:'longdesc',index:'longdesc', width:100,hidden:true},
				{name:'op',index:'op', width:80,search:false,sortable:false,
					formatter:function(cellvalue, options, rowObject){
						gridCompnent.dataSource[cellvalue] = rowObject;
						var res = "<a href=\"javascript:mdfdesc('"+cellvalue+"');\" >EDIT</a>   "+
						"<a href=\"index/loadGameItem/id="+gridCompnent.dataSource[cellvalue][0]+"\" target='_BLANK' >VIEW</a>";
						return res ;
					}
				}
			],
			pager: "#gridPager",
			viewrecords: true,
			caption: "游戏管理"
	};
	return {
		dataSource:new Array(),
		modalClose:function(){
			$("#myModal2").modal('hide');
		},
		save:function(){
			
			var parms = {shortdesc:$("#shortdesc").val(),meddesc:$("#meddesc").val(),
					longdesc:$("#longdesc").val(),gameid:$("#gameid").val()};
			$.ajax({
				type : "POST",
				url : '/admin/game/edit',
				data : parms,
				async : false,
				dataType : "JSON",
				success : function(json) {
					if(json.success){
						gridCompnent.modalClose();
						$.messager.alert('状态信息','操作成功!'); 
						$("#form_search").trigger("click");
					}else{
						$.messager.alert('状态信息','操作失败!'); 
					}
				},
				error : function() {
				}
			});
		},
		init : function() {
			jQuery("#grid").jqGrid(setting);
			jQuery("#grid").navGrid('#gridPager',{
				edit:false,
				add:false,
				del:false,
				search:false
			});
			//注册搜索
			$("#form_search").click(function(){
				var gameName = $("#game_name").val();
				var gameType = $("#game_type").val();
				var languge = $("#languge").val();
				var genreName = $("#genre_name").val();
				var startTime = $("#startTime").val();
				var endTime = $("#endTime").val();
				
				if(genreName == null){
					genreName = "";
				}
				gridCompnent.dataSource = new Array();
				$("#grid").jqGrid('setGridParam',{
					url:'/admin/game/gameList',
		            postData:{'gameName':gameName,'gameType':gameType,'startTime':startTime,'endTime':endTime,'languge':languge,'genreName':genreName},
		            page:1 
		        }).trigger("reloadGrid");

			});
			$("#reset").click(function(){
				$("#log_type").val('');
				$("#log_desc").val('');
				$("#startTime").val('');
				$("#endTime").val('');
			});

		}
	};
	
}();
