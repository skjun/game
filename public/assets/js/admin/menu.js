$(function() {

    treeCompnent.init();

    $("#newNode").click(treeCompnent.add);

    $("#modNode").click(treeCompnent.edit);

    $("#delNode").click(treeCompnent.remove);

    $("#menu_add_b").click(treeCompnent.add_action);

    $("#menu_edit_b").click(treeCompnent.edit_action);

});

var treeCompnent = function() {

    var tree,rMenu ;

    var setting = {
        async: {
            enable: true,
            url:"/admin/menu/getSubMenu",
            autoParam:["id"]
        },
        callback: {
            //onRightClick: onRightClick,
            onClick:onClick
        }
    };
    var nodes = [
        {id:'0',name: "菜单",isParent:true}
    ];

    function onClick(event,treeId,treeNode,clickFlag){
        var id = treeNode.id;
        if("0" == id){
            $("#menu_form_mdf_ctr").hide();
        }
        $("#menuId").val(id);
        $("#menuPId").val(id);
        $("#menu_name_m").val(treeNode.name);
        $("#menu_desc_m").val(treeNode.desc);
        $("#menu_href_m").val(treeNode.href);
        $("#menu_icon_m").val(treeNode.icon);
        $("#menu_class_m").val(treeNode.m_class);
    }
    function onRightClick(event, treeId, treeNode) {
        if (!treeNode &&
            event.target.tagName.toLowerCase() != "button" &&
            $(event.target).parents("a").length == 0) {

            tree.cancelSelectedNode();
            showRMenu("root", event.clientX, event.clientY);

        } else if (treeNode && !treeNode.noR) {

            tree.selectNode(treeNode);
            showRMenu("node", event.clientX, event.clientY);
        }
    }

    function addTreeNode() {
        hideRMenu();
        var newNode = { name:"增加"};
        if (tree.getSelectedNodes()[0]) {
            tree.addNodes(tree.getSelectedNodes()[0], newNode);
            //打开信息页
        }
    }

    function removeTreeNode() {
        hideRMenu();

        var nodes = tree.getSelectedNodes();
        if (nodes && nodes.length>0) {
            if (nodes[0].children && nodes[0].children.length > 0) {
                var msg = "要删除的节点是父节点，如果删除将连同子节点一起删掉。\n\n请确认！";
                if (confirm(msg)==true){
                    tree.removeNode(nodes[0]);
                }
            } else {
                tree.removeNode(nodes[0]);
            }
        }
    }

    function onBodyMouseDown(event){
        if (!(event.target.id == "rMenu" || $(event.target).parents("#rMenu").length>0)) {
            rMenu.css({"visibility" : "hidden"});
        }
    }

    function hideRMenu() {
        if (rMenu) rMenu.css({"visibility": "hidden"});
        $("body").unbind("mousedown", onBodyMouseDown);
    }

    function showRMenu(type, x, y) {
        $("#rMenu ul").show();

        rMenu.css({"top":y+"px", "left":x+"px", "visibility":"visible"});

        $("body").bind("mousedown", onBodyMouseDown);
    }


    return {
        init : function() {
            $.fn.zTree.init($("#tree"), setting,nodes);

            tree = $.fn.zTree.getZTreeObj("tree");
            rMenu = $("#rMenu");

            hideRMenu();

            $("#m_add").click(addTreeNode);
            $("#m_del").click(removeTreeNode);
        },
        add_action:function (){
        	
        	if(!$('#addform').form('validate')){
		        return;
		    }
        	
            var nodes = tree.getSelectedNodes();

            if (nodes && nodes.length != 1) {
                alert("请选择一个父节点");
                return ;
            }

            var node = nodes[0];

            tree.expandNode(node);
            $('#pid').val(node.id);
            $.ajax({
                type: "post",
                data:$('#addform').serialize(),
                url: "/admin/menu/addOrmdf",
                dataType: "json",
                success: function (data){

                    if(data.success){
                        tree.addNodes(node, {
                            id:data.id,
                            pId:data.pid,
                            name:data.name,
                            desc:data.desc,
                            m_calss:data.m_class,
                            href:data.href,
                            icon:data.icon,
                        });
                    }

                }
            });

            $("#menu_form_add_ctr").hide();

            $("#menu_name_a").val('');
            $("#menu_href_a").val('');
            $("#menu_desc_a").val('');
            $("#menu_icon_a").val('');
            $("#menu_class_a").val('');

        },
        add:function(){
            var nodes = tree.getSelectedNodes();
            if (nodes.length == 0) {
                alert("请先选择一个节点");
                return;
            }
            treeNode = nodes[0];
            $("#menu_form_mdf_ctr").hide();
            $("#menu_form_add_ctr").show();
        },
        edit:function(){
            var nodes = tree.getSelectedNodes();
            if (nodes.length == 0) {
                alert("请先选择一个节点");
                return;
            }
            treeNode = nodes[0];
            if("0" == treeNode.id){
                alert("不能编辑根节点");
                return ;
            }
            $("#menu_form_add_ctr").hide();
            $("#menu_form_mdf_ctr").show();
        },
        edit_action:function (){

        	if(!$('#editform').form('validate')){
		        return;
		    }
        	
            var nodes = tree.getSelectedNodes();
            if (nodes.length == 0) {
                alert("请先选择一个节点");
                return;
            }
            treeNode = nodes[0];
            if("0" == treeNode.id){
                alert("不能编辑根节点");
                return ;
            }

            $.ajax({
                type: "post",
                data:$('#editform').serialize(),
                url: "/admin/menu/addOrmdf",
                dataType: "json",
                success: function (data){

                    if(data.success){

                        treeNode.id = data.id;
                        treeNode.name = data.name;
                        treeNode.desc = data.desc;
                        treeNode.href = data.href;
                        treeNode.icon = data.icon;
                        treeNode.m_class = data.m_class;

                        tree.updateNode(treeNode);
                    }

                }
            });

            $("#menu_form_mdf_ctr").hide();

            $("#menuId").val('');
            $("#menu_name_m").val('');
            $("#menu_desc_m").val('');
            $("#menu_href_m").val('');
            $("#menu_icon_m").val('');
            $("#menu_class_m").val('');

        },

        remove:function(){
            var nodes = tree.getSelectedNodes();
            if (nodes.length == 0 || "0" == nodes[0].id) {
                alert("请先选择一个非根节点");
                return;
            }
            treeNode = nodes[0];
            $("#nodeRemove").modal("show");

            $("#del_ok").click(function (){
                $.ajax({
                    type: "post",
                    url: "/admin/menu/removeMenu",
                    data:{'id':treeNode.id},
                    dataType: "json",
                    success: function (data) {
                        if(data.success){
                            tree.removeNode(treeNode);
                        }
                    }
                });
            });

            $("#del_cancel").click(function(){
                $("#nodeRemove").modal("hide");
            });

        }
    };
}();
