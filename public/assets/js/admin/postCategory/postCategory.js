var PostCategory = {

	url : '',
		
	operate	: '',//操作类型（ADD,EDIT,REMOVE）
		
	buildPage : function(){
		treeCompnent.initTree();
	},
	addNode : function(){
		//增加树形结构
		PostCategory.operate = "ADD";
		//获取节点
		var selectedNode = treeCompnent.tree.getSelectedNodes();
		if (selectedNode.length == 0) {
			$.messager.alert('状态信息','请先选择一个节点!');   
			return;
		}
		PostCategory.writeData(selectedNode);
		
		$('#menu_operate').css('display', 'block')
	},
	editNode : function(){
		//编辑树形结构
		PostCategory.operate = "EDIT";
		//获取节点
		var selectedNode = treeCompnent.tree.getSelectedNodes();
		if (selectedNode.length == 0) {
			alert("请先选择一个节点");
			return;
		}
		var dataStr = JSON.stringify(selectedNode);
		var data = $.parseJSON(dataStr);
		if ("1" == data[0].id) {
			$.messager.alert('状态信息','请先选择一个节点!');   
			return;
		}
		PostCategory.writeData(selectedNode);
		
		$('#menu_operate').css('display', 'block')
	},
	removeNode : function(){
		//删除树形结构
		PostCategory.operate = "REMOVE";
		//获取节点
		var selectedNode = treeCompnent.tree.getSelectedNodes();
		if (selectedNode.length == 0) {
			$.messager.alert('状态信息','请先选择一个节点!');   
			return;
		}
		if (selectedNode[0].id == 1) {//根节点，不能删
			$.messager.alert('状态信息','根节点不能删除!');   
			return;
		}
		PostCategory.removeData(selectedNode);
	},
	writeData : function(selectedNode){
		var dataStr = JSON.stringify(selectedNode);
		var data = $.parseJSON(dataStr);
		if(PostCategory.operate=='ADD'){
			$('#category_id').val(data[0].id);
			$('#category_name').attr('value','');
			$('#description').attr('value','');
			$('#c_index').attr('value','');
			$('#parentId').val(data[0].pId);
			$('button[type="submit"]').text("新增分类");
		}
		if(PostCategory.operate=='EDIT'){
			$('#category_id').val(data[0].id);
			$('#category_name').val(data[0].name);
			$('#description').val(data[0].desc);
			$('#c_index').val(data[0].cIndex);
			$('#parentId').val(data[0].pId);
			$('button[type="submit"]').text("修改分类");
		}
	},
	postData : function() {
		var parms = {
				'category_id' : $('#category_id').val(),
				'category_name' : $('#category_name').val(),
				'description' : $('#description').val(),
				'c_index' : $('#c_index').val(),
				'parent' : $('#parentId').val()
			};
		if (PostCategory.operate == 'ADD') {
			PostCategory.url = "/admin/postCategory/addPostCategory";
		}
		if (PostCategory.operate == 'EDIT') {
			PostCategory.url = "/admin/postCategory/updatePostCategory";
		}
		$.ajax({
			type : "POST",
			url : PostCategory.url,
			data : parms,
			async : false,
			dataType : "JSON",
			success : function(json) {
				if(json='OK'){
					treeCompnent.tree.destroy();
					treeCompnent.initTree();
					$.messager.alert('状态信息','操作成功!');   
				}
			},
			error : function() {
			}
		});
	},
	removeData : function(selectedNode) {
		if(confirm("您确定要删除吗?")){
			var dataStr = JSON.stringify(selectedNode);
			var data = $.parseJSON(dataStr);
			var parms = {
					'category_id' : data[0].id
				};
			if(data[0].isParent==true){
				if(confirm("确定删除节点包含的子节点")){
					PostCategory.removePostCategory(parms);
				}
			}else{
				PostCategory.removePostCategory(parms);
			}
		}
	},
	removePostCategory : function(parms){
		$.ajax({
			type : "POST",
			url : "/admin/postCategory/removePostCategory",
			data : parms,
			async : false,
			dataType : "JSON",
			success : function(json) {
				if(json='OK'){
					treeCompnent.tree.destroy();
					treeCompnent.initTree();
					$.messager.alert('状态信息','操作成功!');   
				}
			},
			error : function() {
			}
		});
	}
};

/** 初始化一些事件, 或者加载页面 */
$(function() {
	PostCategory.buildPage();
	
	$("#newNode").click(PostCategory.addNode);
	
	$("#modNode").click(PostCategory.editNode);

	$("#delNode").click(PostCategory.removeNode);
	
	$("#submit").click(PostCategory.postData);
	
});



