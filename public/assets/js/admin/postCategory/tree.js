var treeCompnent = {

		tree : {},
		zNodes : [],	
		setting : {	
				data: {
					simpleData: {
						enable: true
					}
				},
				callback: {
					onClick: zTreeOnClick
				}
		},
	initTree : function(parms){
		treeCompnent.zNodes = [];//在destory之后重置zNodes
		treeCompnent.loadNodes(parms);// 获取tree json
		$.fn.zTree.init($("#tree"), treeCompnent.setting, treeCompnent.zNodes);
		treeCompnent.tree = $.fn.zTree.getZTreeObj("tree");
	},
	loadNodes : function(parms){
		$.ajax({
            type: "POST",
            url:"/admin/postCategory/getSubMenu",
           	data: parms,
           	async:false,
            dataType: "JSON",
            success:function(json){
            	for(var v in json){
            		treeCompnent.zNodes.push(json[v][0]);
            	}
		    },
            error: function () {
            }
        });
	}
};


function zTreeOnClick(event, treeId, treeNode) {
	var selectedNode = treeCompnent.tree.getSelectedNodes();
	if(PostCategory.operate=='ADD'){
		PostCategory.writeData(selectedNode);
	}
	if(PostCategory.operate=='EDIT'){
		if ("1" == selectedNode[0].id) {
			$('#menu_operate').css('display', 'none')
			alert("不能修改更节点");
			return;
		}
		$('#menu_operate').css('display', 'block')
		
		PostCategory.writeData(selectedNode);
	}
}
