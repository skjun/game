var Posts = {
		
	option : [],	
	operate : [
//	            '<div class="row-fluid">',
//	            '<select id="operate">',
//	            '<option value="0" selected="selected">批量操作</option>',
//	            '<option value="1">编辑</option>',
//	            '<option value="2">移至回收站</option>',
//	            '</select>',
//	            '<button id="application" class="btn btn-small" type="button">应用</button>',
//	            '<select id="classify">',
//	            '<option value="0">查看所有分类</option>',
//	            Posts.option.join(""),
//	            '<option value="1">未分类</option>',
//	            '</select>',
//	            '<button id="filter" class="btn btn-small" type="button">筛选</button>',
//	            '</div>'
	            ],
//    var status = [{type:'是',name:'是'},{type:'否',name:'否'}];

	columns : [[
	            {field:'ID', title:'文章ID', align: 'center', hidden: true},
	            {field:'href', title:'文章href', align: 'center', hidden: true},
	            {field:'post_title', title:'标题', width:200, align: 'center', editor: 'text'},
				{field:'post_author', title:'对应作者ID', width:100, align: 'center'},
				{field:'category_name', title:'分类目录', width:100, align: 'center'},
				{field:'tags', title:'标签', width:100, align: 'center'},
				{field:'comment_count', title:'评论总数', width:60, align: 'center', editor: 'text'},
				{field:'post_date', title:'发布时间', width:150, align: 'center', editor: 'text'},
				{field:'post_status', title:'文章状态', width:100, align: 'center', formatter: function(value){
					 if(value==0){value = "已发布"}else{value = "草稿"}
			         return value;  
				},editor: 'text'},
				
//				{field:'post_content', title:'正文', width:100, align: 'center', editor: 'text'},
//				{field:'post_excerpt', title:'摘录', width:80, align: 'center'},
//				{field:'post_file_location', title:'文章缩略名,缩略图所在的位置', width:100, align: 'center', editor: 'text'},
//				{field:'post_modified', title:'修改时间', width:100, align: 'center', editor: 'text'},
//				{field:'post_category', title:'文章类型', width:100, align: 'center', editor: 'text'},
//				{field:'post_markdowncontent', title:'文章markdown格式的内容', width:100, align: 'center', editor: 'text'},
				{field:'opt',title:'操作',width:213,align:'center',  
                    formatter:function(value,rec,index){  
//                        var add = '<a name="add" href="javascript:void(0);">添加</a> ';  
                    	var add = '<a name="posts_edit" href="javascript:void(0);" value="">编辑</a> ';
                        var more = '<a name="posts_more" href="javascript:void(0);" value="">查看</a> ';  
                        var remove = '<a name="posts_remove" href="javascript:void(0);" value="">删除</a>';
                        return add + more + remove;  
                    }  
                  }  
				]],

	buildPage : function() {// 初始化列表页面
		Posts.getOperate();
		
		$('#search').click(function(){
			var operate = $('select[id="classify"]').attr("value");
			var status = $('select[id="status"]').attr("value");
			var parms = {'POST_CATEGORY':operate, 'POST_TITLE':$('#postTitle').val(), 'POST_STATUS':status};
			$.ajax({
	            type: "POST",
	            url: '/admin/posts/getAllPosts',
	           	data: parms,
	           	async:false,
	            dataType: "JSON",
	            success:function(json){
	            	var data = $.parseJSON(data);
			    	$('#posts').datagrid('loadData',json);
			    },
	            error: function () {
	            }
	        });
		});
		
		$('#reload').click(function(){
			 $('#searchForm').form('clear');
			 $("#classify  option[value='']").attr("selected", true);
			 $("#status  option[value='']").attr("selected", true);
			 $('#posts').datagrid('reload');
		});
		
		$('#posts').datagrid(
				{
					width : 1060,
					height : 550,
					nowrap : false,
					striped : true,
					showFooter : true,
					collapsible : false,
					url : '/admin/posts/getAllPosts',
					queryParams : {},
					remoteSort : false,
					loadMsg : '正在加载....',
					columns : Posts.columns,
					pagination : true,// 是否显示分页工具栏
					rownumbers : true,
					singleSelect : true,
					pageNumber : 1,
					onLoadSuccess:function(){
						var data = $('#posts').datagrid("getRows");
						var posts_more = $('.datagrid a[name="posts_more"]');
						var posts_remove = $('.datagrid a[name="posts_remove"]');
						var posts_edit = $('.datagrid a[name="posts_edit"]');
						
						for ( var obj in data) {
							$(posts_more[obj]).attr("value", data[obj].ID);
							$(posts_remove[obj]).attr("value", data[obj].ID);
							$(posts_edit[obj]).attr("value", data[obj].ID);
							$(posts_more[obj]).attr({ href: data[obj].href, target: "_blank" });
						}
						
/*			        	 $('.datagrid a[name="posts_more"]').click(function() {
			        		 alert("没链接");
			 			 });*/
			        
			        	 $('.datagrid a[name="posts_remove"]').click(function() {
			        		 var id = $(this).attr("value");
							 Posts.removePosts(id);
			        	 });
			        	 $('.datagrid a[name="posts_edit"]').click(function() {
			        		 var id = $(this).attr("value");
							 window.open("admin/write/update?id="+id, "_blank");
						});
			         },onClickRow:function(index){ //暂时只是到用这种方式去改变选中一行是的背景颜色
			        	 $('.datagrid-row').removeClass('datagrid-row-selected').css({background:"",color:""});
			        	 $('#datagrid-row-r1-2-'+index).addClass('datagrid-row-selected').css({background:'#FBEC88',color:'#333333'});
			         }
				});
	},
	/** 动态加载分类 */
	getOperate : function(){
		$.ajax({
            type: "POST",
            url: '/admin/postCategory/getCategoryName',
           	data: {},
           	async:false,
            dataType: "JSON",
            success:function(json){
            	$.each( json, function(k, v){
            		Posts.option.push('<option value="'+v.category_id+'">'+v.category_name+'</option>');
            		});
            	 
            	Posts.operate.push('<div class="row-fluid">');
            	Posts.operate.push('<form class="form-inline" id="searchForm">');
            	Posts.operate.push('<span>');
            	Posts.operate.push('<select id="classify">');
            	Posts.operate.push('<option value="">查看所有分类</option>');
            	Posts.operate.push(Posts.option);
            	Posts.operate.push('</select>');
            	Posts.operate.push('</span>');
            	Posts.operate.push('<span>');
            	Posts.operate.push('<select id="status">');
            	Posts.operate.push('<option value="">所有状态</option>');
            	Posts.operate.push('<option value="0">已发布</option>');
            	Posts.operate.push('<option value="1">草稿</option>');
            	Posts.operate.push('</select>');
            	Posts.operate.push('</span>');
            	Posts.operate.push('<span>');
            	Posts.operate.push('<input id="postTitle" type="text" placeholder="文章标题">');
            	Posts.operate.push('</span>');
            	Posts.operate.push('<span>');
            	Posts.operate.push('<button id="search" class="btn" type="button">搜索</button>');
            	Posts.operate.push('</span>');
            	Posts.operate.push('<span>');
            	Posts.operate.push('<button id="reload" class="btn" type="button">重置</button>');
            	Posts.operate.push('</span>');
            	Posts.operate.push('</form>');
            	Posts.operate.push('</div>');
            	//列表筛选初始化
            	
        		$('#myTabContent').prepend(Posts.operate.join(""));
		    },
            error: function () {
            }
        });
	},
	removePosts : function(postsID){
		$.ajax({
            type: "POST",
            url: '/admin/posts/removePosts',
           	data: {'ID':postsID},
           	async:false,
            dataType: "JSON",
            success:function(json){
            	$.messager.alert('状态信息','操作成功!');   
            	$('#posts').datagrid('reload');
		    },	
            error: function () {
            }
        });
	},
	fillposthref : function(){
		$.ajax({
            type: "POST",
            url: '/admin/posts/removePosts',
           	data: {'ID':postsID},
           	async:false,
            dataType: "JSON",
            success:function(json){
            	$.messager.alert('状态信息','操作成功!');   
            	$('#posts').datagrid('reload');
		    },	
            error: function () {
            }
        });
	}
};

/** 初始化一些事件, 或者加载页面 */
$(function() {
	Posts.buildPage();
	$('#searchForm').keydown(function(event){
		if(event.keyCode==13){
			return false;
		}
	});
});

String.prototype.trim = function() {
	return this.replace(/(^\s*)|(\s*$)/g, "");
};