$(function() {

	gridCompnent.init();
	
	$('.modal').on('show', function() {
	    $(this).css({
	        'margin-top': function () {
	            return -($(this).height());
	        }
	    });
	});

	$("#add_cancel").click(function(){
		$("#role_add").modal("hide");
	});

	$("#add_save").click(function(){
		gridCompnent.add();
		$("#role_add").modal("hide");
	});
	
	$("#del_cancel").click(function(){
		$("#remove").modal("hide"); 
	});
	
	$("#del_ok").click(function (){
		var role_id = $("#grid").jqGrid('getGridParam','selrow');
		
		$.ajax({ 
            type: "post", 
            data:{'role_id':role_id},
            url: "/admin/role/delRole", 
            dataType: "json",  
            success: function (data){
            	if(data.success){
            		$("#grid").trigger("reloadGrid"); 
            	}
            }
		});
		
		$("#remove").modal("hide");
	});
	
});


var gridCompnent = function(){
	
	var tree;
	
	var setting = {
			url:'/admin/role/roleList',
			datatype: "json",
			mtype: "POST",	
			height: '100%',
			width:1000,
			rowNum: 20,
			rowList: [10,20,30],
			colNames:['角色ID','角色名','描述','修改日期','创建用户'],
			colModel:[
				{name:'role_id',index:'role_id', width:100,hidden:true,search:false},
				{name:'role_name',index:'role_name', width:100},
				{name:'role_desc',index:'role_desc', width:100},
				{name:'role_create_date',index:'role_create_date', width:100},
				{name:'user_name',index:'user_name', width:100}
			],
			pager: "#gridPager",
			viewrecords: true,
			caption: "角色管理" 
	};
	
	var treeSetting = {
			check: {
				enable: true
			},
			data: {
				simpleData: {
					enable: true
				}
			}
	};
	
	return {
		init : function() {
			jQuery("#grid").jqGrid(setting);
			
			jQuery("#grid").
			navGrid('#gridPager',{
				edit:false,
				add:false,
				del:false,
				search:false
			}).
			navButtonAdd('#gridPager',{   
				caption:"新增",    
				buttonicon:"ui-icon-add",    
				onClickButton: function(){
					
					$.ajax({ 
			            type: "post", 
			            url: "/admin/menu/menulist", 
			            dataType: "json",  
			            success: function (data){
			            	$.fn.zTree.init($("#tree_add"), treeSetting, data);
			            	tree = $.fn.zTree.getZTreeObj("tree_add");
			            }
					});
					$("#role_id").val('');
					$("#role_name").val('');
					$("#role_desc").val('');
					$("#role_op").html("角色新增:");
					$("#role_add").modal("show");
				},
				position:"last"  
			}).
			navButtonAdd('#gridPager',{   
				caption:"修改",    
				buttonicon:"ui-icon-add",    
				onClickButton: function(){
					
					var role_id = $("#grid").jqGrid('getGridParam','selrow'); 
					if(role_id == null){
						alert("请选中一行");
						return ;
					}
					
					$.ajax({ 
			            type: "post", 
			            url: "/admin/menu/menulist", 
			            dataType: "json",
			            data:{role_id:role_id},
			            success: function (data){
			            	$.fn.zTree.init($("#tree_add"), treeSetting, data);
			            	tree = $.fn.zTree.getZTreeObj("tree_add");
			            }
					});
					
					var rowDatas = $("#grid").jqGrid('getRowData', role_id);
					$("#role_name").val(rowDatas['role_name']);
					$("#role_desc").val(rowDatas['role_desc']);
					$("#role_id").val(rowDatas['role_id']);
					$("#role_op").html("角色修改:");
					$("#role_add").modal("show");
				},
				position:"last"  
			}).
			navButtonAdd('#gridPager',{   
				caption:"删除",    
				buttonicon:"ui-icon-add",    
				onClickButton: function(){
					
					var role_id = $("#grid").jqGrid('getGridParam','selrow'); 
					if(role_id == null){
						alert("请选中一行");
						return ;
					}
					
					$("#remove").modal("show");
				},
				position:"last"  
			});

		},
		add : function(){
			var nodes = tree.getCheckedNodes(true);
			
			var menus = "";
	
			if(nodes.length > 0){
				menus += nodes[0].id;
			}
			if(nodes.length > 1){
				for(var key = 1 ; key < nodes.length ;key++ ){
					menus += ","+ nodes[key].id;
				}
			}
			
			$.ajax({ 
	            type: "post", 
	            url: "/admin/role/addRole", 
	            data:{roleId:$("#role_id").val(),roleName:$("#role_name").val(),roleDesc:$("#role_desc").val(),menu:menus},
	            dataType: "json",  
	            success: function (data){
	            	if(data.success){
	            		$("#grid").trigger("reloadGrid"); 
	            	}
	            	$("#role_add").modal("hide");
	            }
			});
		}
	};
	
}();
