$(function() {
	gridCompnent.init();
});

var gridCompnent = function(){
	var setting = {
			url:'/admin/websites/datalist',
			datatype: "json",
			mtype: "POST",	
			height: '100%',
			width:1000,
			rowNum: 20,
			rowList: [10,20,30],
			colNames:['id','站点名称','用户名称','站点描述','站点地址','创建日期','更新时间'],
			colModel:[
				{name:'id',index:'id', width:100,hidden:true,search:false},
				{name:'site_name',index:'site_name', width:100},
				{name:'user_name',index:'user_name', width:100},
				{name:'site_desc',index:'site_desc', width:100},
                {name:'site_url',index:'site_url', width:100},
				{name:'create_date',index:'create_date', width:100},
                {name:'update_time',index:'update_time', width:100}
			],
			pager: "#gridPager",
			viewrecords: true,
			caption: "卫星站管理"
	};
	
	return {
		init : function() {
			jQuery("#grid").jqGrid(setting);
			jQuery("#grid").navGrid('#gridPager',{
				edit:false,
				add:false,
				del:false,
				search:false
			});
			//注册搜索
			$("#form_search").click(function(){
				var user_name = $("#user_name").val();
				var site_name = $("#site_name").val();
				var site_i = $("#site_i").val();

				if(user_name == null){
					user_name = "";
				}
				
				$("#grid").jqGrid('setGridParam',{
					url:'/admin/websites/datalist',
		            postData:{'user_name':user_name,'site_i':site_i},
		            page:1 
		        }).trigger("reloadGrid");

			});
			$("#reset").click(function(){
				$("#user_name").val('');
				$("#site_i").val('');
				$("#site_name").val('');
			});

		}
	};
	
}();

