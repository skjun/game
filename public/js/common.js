/**
 * Created by yuyue on 13-12-2.
 */

// 统一程序的入口。
$(function() {
	adaptive();
	$(window).resize(function() {
		adaptive();
	});
	$('#inner').nav({
		t : 22000,
		a : 1000
	});
	// 处理登录弹出窗口
//	$("#opendialog").on('click', openLogin);
//	$(".openlogin").on('click', openLogin);
//	setloginbanner();
//	$("#loginbtn").on('click', userlogin);
	//加载广告内容
	getContent("首页顶部", "topcontent");
	getContent("首页右侧", "rightContent");
	getContent("文章中间", "postcontent");
	
	/**
	  *add by shiwei
	  */
	$("#emailOrder111").click(function(){
		var email = $('#emailAddress').val();
		var reg = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
		if(reg.test(email)){ 
	        $.ajax({
	             url:'http://localhost/emailGather/save',
	             type : "post",
	             dataType:"jsonp",
	             data:{email:email},
	             jsonp:"callback",
	             jsonpCallback:"success_jsonpCallback",
	             success:function(data){
					windows.href = "http://localhost/emailsub";
	             }
	        });
				
		}else{
			alert("email格式错误！");
		}
	});
	/**
	  *add end by shiwei
	  */
})

// 加载广告信息
function getContent(type, position) {
	if (document.getElementById(position) == null) {
		return;
	}
	$.ajax({
		type : "POST",
		data : {
			type : type
		},
		url : weburl + 'admin/advertising/getContent',
		success : function(data) {
		    var obj = eval('(' + data + ')');
			if (obj.type == 1) {
				 $("#"+position).html(obj.result );
			}
		},
		error : function() {
		}
	});
}
/**
 * 获取首页顶部广告内容
 */
function adcontent_top() {

	$.ajax({
		type : "POST",
		url : weburl + 'admin/advertising/getToContent',
		success : function(data) {
			document.getElementById("topcontent").innerHTML = data;
		},
		error : function() {
		}
	});
}
/**
 * 获取首页顶部广告内容
 */
function adcontent_right() {
	$.ajax({
		type : "POST",
		url : weburl + 'admin/advertising/getHomeRightContent',
		success : function(data) {
			document.getElementById('rightContent').innerHTML = data;
		},
		error : function() {
		}
	});
}

/**
 * 适配窗口大小的变化
 */
function adaptive() {
	if ($("body").width() > 1080) {
		$(".wrapper").css("width", "1080px");
	} else {
		$(".wrapper").css("width", "95%");
	}
}
/*var layout1;
function openLogin() {
	// 将 div变成一个弹出层。将其弹出来，里面的参数可以自己设置一下，看看效果
	layout1 = $.layer({
		type : 1,
		title : false,
		closeBtn : true,
		border : [ 0 ],
		area : [ '450px', '266px' ],
		page : {
			dom : '#login'
		}
	});
	$("#logincolse").on('click', function() {
		// console.log("click");
		layer.close(layout1);
	});
}
function userlogin() {
	var username = $("#login_username").val();
	var password = $("#login_password").val();
	// logintip,<br><font color="#a52a2a">用户名，密码不能为空</font>
	$("#logintip").html("");
	if (username === '' || password == '') {
		$("#logintip").html("<br><font color=\"#a52a2a\">用户名，密码不能为空</font>");
	} else {
		$.post(weburl + 'login/loginAajx', {
			"user_name" : username,
			"password" : password
		}, function(data) {
			if (data.result) {
				$("#logintip").html(
						"<br><font color=\"#a52a2a\">用户名或密码错误</font>");
			} else {
				setloginbanner();
				layer.close(layout1);
			}
		}, "json");
	}
}
function setloginbanner() {
	//
	$.post(weburl + 'login/isLogin', {
		"a" : "a"
	}, function(data) {
        var userStr ;

		if (data.result) {
            if(data.user){
                userStr = data.user.username;
            }else{
                userStr =data.user.email;
            }
			$($("#userloginbanner li").get(0)).html(
					'Welcome <a href="' + weburl+ 'profile" class="username">'
							+ userStr + '</a>|<a href="' + weburl
							+ 'login/loginout" class="exit">退出</a>');
		} else {
			$($("#userloginbanner li").get(0)).html(
					'Welcome <a href="javascript:void(0);" class="openlogin" onclick="openLogin()"> (Log In)</a>');
		}
	}, "json");

}*/
