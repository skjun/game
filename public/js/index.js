var index = {

	operator : "date",
		
	loadPosts : function() {
		// 设置请求地址
		ListBox.url = 'index/loadPosts';
		// 设置请求地址参数，没有可以不写
		ListBox.parms = {};

		ListBox.init();
	},

	loadHotPost : function(){
		$.ajax({
			type : "POST",
			url : "index/loadHotPost",
			data : {},
			cache : false,
			async : false,
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert("系统繁忙，请稍候再试");
			},
			success : function(data, textStatus) {
				index.writeHotPost(data);
			}
		});
	},
	
	writeHotPost : function(data){
		if(data=="empty"){
			return;
		}
		var json = eval('(' + data + ')');
		var target = jQuery('#hotlistshowList'), length = json.length, html="";
		for ( var i = 0; i < length; i++) {
			var listLi = [];
			if (i < 5) {
				listLi = [
							'<li id='+json[i].ID+' data-role="content">',
							'<a href="'+json[i].href+'">',
						    '<div><img src="'+json[i].post_file_location+'" onerror="this.src=\''+weburl+'public/images/error.png\';" /></div>'+json[i].post_title+'',
							'</a>',
							'</li>'
				          ];
			}else{
				listLi = [
							'<li id='+json[i].ID+' class="dn" data-role="content">',
							'<a href="'+json[i].href+'" >',
						    '<div><img src="'+json[i].post_file_location+'"  onerror="this.src=\''+weburl+'public/images/error.png\';"/></div>'+json[i].post_title+'',
							'</a>',
							'</li>'
			               ];
			}
			html += listLi.join("");
		}
		
		target.html(html);
	},
	
    comparatorDate : function(a, b) {
        return $(a).data('date') < $(b).data('date') ? -1 : 1;
    },
    comparatorPopularity :  function(a, b) {
        return $(a).data('popularity') > $(b).data('popularity') ? -1 : 1;
    },
	onClickSortBy : function(event) {
		event.preventDefault();
	
		$currentSortby = $(this);

		switch ($currentSortby.data('sortby')) {
		case 'date':
			index.operator = "date";
			ListBox.options.comparator = index.comparatorDate;
			break;
		case 'popularity':
			index.operator = "popularity";
			ListBox.options.comparator = index.comparatorPopularity;
			break;
		}
		
		//重置分页条件
		
		ListBox.size = 30;
		if(index.operator == "date"){
			//请求参数
			ListBox.currSize = 0;
			ListBox.parms = {};
		}
		if(index.operator == "popularity"){
			//请求参数
			ListBox.currSize = 0;
			ListBox.parms = {"order": "click_count"};
		}
		//清除ul数据
		$('#tiles').empty();
		//写数据
		ListBox.loadData();
		
		$('#tiles li img').imagesLoaded(function(instance) {
			var img = instance.images;
			$.each( img, function(i, n){
				if(!n.isLoaded){
					$(n.img).css("height", "280px");
				}
			});
			var $handler = $('#tiles li'), $sortbys = $('#sortbys a');
			ListBox.handler = $handler;
			$sortbys.removeClass('acit');
			$currentSortby.addClass('acit');
			
			if (ListBox.handler.wookmarkInstance){
	            ListBox.handler.wookmarkInstance.clear();
	        }
			ListBox.handler.wookmark(ListBox.options);
	     });
        //重置参数
        ListBox.loadStatus = true;
        ListBox.currSize = 30;
        ListBox.size = 10;
	}
};

/** 初始化一些事件, 或者加载页面 */
$(function() {
	index.loadHotPost();
	jQuery('#control').pagination(5, function() {});
	
	// 改为模板
	index.loadPosts();
	
	$('#sortbys a').click(index.onClickSortBy);
});
