//程序的开始，最好还是放在文档加载完以后
$(document).ready(function(){
    $("#savelanguage").click(modifyDefaultLanguage);
});

 
function modifyDefaultLanguage(){
    $.post('admin/sitelanguage/modifyDefaultLanguage', {"ln":$("#languagegroup option:selected").attr("value")}, function( data ) {
    	 $.layer({
             area : ['auto','auto'],
             dialog : {msg:'Success！',type : 1}
         });
    }, "json");
}

 