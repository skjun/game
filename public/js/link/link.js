/**
 * Created by yuyue on 13-11-26.
 */
    //程序的开始，最好还是放在文档加载完以后
$(document).ready(function(){
    grid.loadgrid();
    $("#addbtn").click(grid.addtag);
    $("#addnew").click(grid.addNewSite);
    
});

var grid = (function (){
 function load(){
     $("#list2").jqGrid({
         url:weburl+'admin/friendlinks/datalist',
         datatype: "json",
         mtype: 'POST',
         colNames:['','Name','Url','Logo','CreateTime','Edit'],
         colModel:[
             {name:'id',  hidden:true},
             {name:'sitename',  width:150,sortable:false},
             {name:'siteurl',  width:250,sortable:false},
             {name:'sitelogo',  width:250,sortable:false},
             {name:'create_time',  width:250,sortable:false},
             {width:200,sortable:false,formatter:customFmatter}
         ],
         autowidth: true, // 自动匹配宽度
         height:500,   // 设置高度
         gridview:true, // 加速显示
         viewrecords: true,  // 显示总记录数
         rowNum:'20',
         pager: '#pager2',
         viewrecords: true
     });
     jQuery("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false},{},{},{},{multipleSearch:true, multipleGroup:true});
 }
    function customFmatter(cellvalue, options, rowObject){
        // cellvalue - 当前cell的值
        // options - 该cell的options设置，包括{rowId, colModel,pos,gid}
        // rowObject - 当前cell所在row的值，如{ id=1, name="name1", price=123.1, ...}
        return "<button class='btn btn-primary' id='"+rowObject.id+"' sitename='"+rowObject.sitename+"' siteurl='"+rowObject.siteurl+"' sitelogo='"+rowObject.sitelogo+"' onclick=\"grid.updatetag(this)\">Modify</buttion><button class='btn' data='"+rowObject.id+"'  onclick=\"grid.deleteData(this)\">Delete</buttion> "
    }
    function deleteData(ele){

        if(confirm('确定将此记录删除?')){
        $.post( weburl+'admin/friendlinks/delete', {"id":$(ele).attr('data')}, function( data ) {
            search();
        }, "json");

        }
    }
    
    function addNewSite(){
    	$("#friendLinkModal").modal('show');
    	$("#sitename").val('');
     	$("#siteurl").val('');
     	$("#sitelogo").val('');
     	$("#id").val('');
    }
    function addtag(){
        var tagname = $("#tag_name").val();
        var tag_img = $("#tag_img").val();
        //这里会根据
        $.post( weburl+'admin/gametags/addTag', {"tagname":tagname,"tagimg":tag_img}, function( data ) {
        	if(data.id != null ){
        		alert("新增成功！");
        	}else{
        		alert("新增失败！");
        	}
        	$("#tag_img").val('');
        	$("#tag_name").val('');
            search();
         }, "json");
    }
    function updatetag(ele){
        $("#friendLinkModal").modal('show');
        $("#sitename").val($(ele).attr('sitename'));
        $("#siteurl").val($(ele).attr('siteurl'));
        $("#sitelogo").val($(ele).attr('sitelogo'));
        $("#id").val($(ele).attr('id'));
    }
    function search(){
        $("#list2").jqGrid('setGridParam',{
            url:weburl+'admin/friendlinks/datalist',
            page:1
        }).trigger("reloadGrid"); //重新载入
    }
    function updatetagform(){
        $.post(weburl+'admin/friendlinks/addOrModify', {"id": $("#id").val(),"sitename": $("#sitename").val(),"siteurl": $("#siteurl").val(),"sitelogo": $("#sitelogo").val()}, function( data ) {
            search();
            winclose();
        }, "json");
    }
    function winclose(){
        $("#friendLinkModal").modal('hide');
    }
 return {
     loadgrid:load,
     addNewSite:addNewSite,
     deleteData:deleteData,
     updatetag:updatetag,
     winclose:winclose,
     updatetagform:updatetagform
 }
})();