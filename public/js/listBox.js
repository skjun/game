var ListBox = {
    handler : null,
    currSize : 30, // 不需要更改
    size : 10, // 设置一次请求多少数据

    url : '', // url请求的地址

    parms : {}, // url请求参数

    loadStatus : true,	//是否还需要继续加载

    options : {
        autoResize : true,
        container : $('#main'),
        offset : 15,
        itemWidth : 250
        //comparator :  comparatorDate
    },
    scrollTimer : undefined,

    onScroll : function(event) {
        if(ListBox.scrollTimer) {
            clearTimeout(ListBox.scrollTimer);
            ListBox.scrollTimer = undefined;
        }
        ListBox.scrollTimer = setTimeout(function(){
            var _windowHeight = window.innerHeight ? window.innerHeight : $window.height();
            var closeToBottom = ($(window).scrollTop() + _windowHeight > $(document).height() - 100);
             if (closeToBottom) {
                if (ListBox.loadStatus){
                    ListBox.loadData();
                }
                if (ListBox.handler.wookmarkInstance){
                    ListBox.handler.wookmarkInstance.clear();
                }
                ListBox.handler = $('#tiles li');
                ListBox.handler.wookmark(ListBox.options);
            }
        },300);
    },

    setStyle : function() {
        ListBox.handler = $('#tiles li');
        ListBox.handler.wookmark(ListBox.options);
    },

    init : function() {
        $('#tiles li img').imagesLoaded(function(instance) {
			var img = instance.images;
			$.each( img, function(i, n){
				if(!n.isLoaded){
					$(n.img).css("height", "318px");
				}
			});
            $(window).bind('scroll', ListBox.onScroll);
            ListBox.setStyle();
        });
    },

    loadData : function() {
        var Mparms = {
            "page" : ListBox.currSize,
            "rows" : ListBox.size
        };
        ListBox.currSize = ListBox.size + ListBox.currSize;
        ListBox.parms = ListBox.mergeObj(ListBox.parms, Mparms);
        $.ajax({
            type : "POST",
            url : ListBox.url,
            data : ListBox.parms,
            async : false,
            dataType : "JSON",
            success : function(json) {
                if(json!="empty"){
                    ListBox.writeTable(json);
                }else{
                    ListBox.loadStatus = false;
                }
            },
            error : function() {
            }
        });
    },
    writeTable : function(json) {
        var ulHtml = '';
        var length = json.length;
        for ( var i = 0; i < length; i++) {
            var tags = "", excerpt = "", popularity="";
            if(json[i].tag_id!=null&&json[i].tag_id!=""){
                var tagsArr = json[i].tag_name.split(",");
                var tagsIDArr = json[i].tag_id.split(",");
                for(var k=0; k<tagsArr.length; k++){
                    if(tagsArr[k]!=''){
                        tags += "<a href=\"tag/showtag/"+tagsIDArr[k]+"\">"+tagsArr[k]+"</a>";
                    }
                }
            }
            if(json[i].post_excerpt!=null&&json[i].post_excerpt!=""){
                excerpt = json[i].post_excerpt;
            }
            if(json[i].sort_popularity!=null&&json[i].sort_popularity!=""){
            	popularity = json[i].sort_popularity;
            }
            var html = [
                '<li data-date="'+json[i].sort_date+'" data-popularity="'+popularity+'">',
                '<a href="'+json[i].href+'"><img src="'+json[i].post_file_location+'" class="f_l" onerror="this.src=\''+weburl+'public/images/error.png\';"/></a>',
                '<div class="f_l">',
                '<a href="'+json[i].href+'">',
                '<h4>'+json[i].post_title+'</h4>',
                '<span><font>'+json[i].post_author+'</font> publised <time>'+json[i].post_date+'</time></span>',
                '<p>'+excerpt+'</p>',
                '</a>',
                '<div class="texttag">'+tags+'</div>',
                '</div>', '</li>' ];
            ulHtml += html.join('');
        }

        $('#tiles').append(ulHtml);
    },
    mergeObj : function(Mparms, parms) {
        if (parms == undefined || parms == null || parms == "") {
            return Mparms;
        } else {
            return jQuery.extend(Mparms, parms);
        }
    }
};