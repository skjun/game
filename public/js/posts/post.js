//程序的开始，最好还是放在文档加载完以后
$(document).ready(function() {
    $("[href^='#']").click(function (){location.hash=$(this).attr("href"); return false;});
	$("#post_comments").click(function() {
		posts.savePostComments();
	});
    $("#zanyixiabtn").click(function() {
        posts.updateAgreeCounts();
    });
	posts.updateLoadCounts();
    posts.updateZanCounts();
});

var posts = (function() {
	// get the values
	var postid, postmessage,key;
	function updateAgreeCounts() {
        if($.cookie(key)==="ok"){
            $.layer({
                area : ['auto','auto'],
                dialog : {msg:'您已经赞过这篇文章了哦!',type : 0}
            });
        }else{
            // 这里会根据
            $.post( 'post/updateAgreeCounts', {
                id : parms.id
            }, function(data) {
                 if(data.result == -1){
                     $.layer({
                          area : ['auto','auto'],
                          dialog : {msg:'您已经赞过这篇文章了哦!',type : 0}
                      });
                 }else{
                     $("#agreecount").html(data.result );
                     $.cookie(data.key, "ok",{ expires: 30 });
                 }
            }, "json");
        }
	}
    function   updateZanCounts(){
        // 这里会根据
        $.post( 'post/getAgreeCounts', {
            id : parms.id
        }, function(data) {
            if(data.result == -1){
                $("#agreecount").html(0);
                key = data.key;
            }else{
                $("#agreecount").html(data.result );
                key = data.key;
            }
        }, "json");
    }
	function getComments() {
		// 这里会根据
		$.post(weburl + 'admin/post/getComments', {
			id : parms.id
		}, function(data) {
			// console.log(data.id);
		}, "json");
	}
	function updateLoadCounts() {
		$.post(weburl + 'post/updateLoadCounts', {
			id : parms.id
		}, function(data) {
            $("#clicknumspan").html(data.clickNum );
		}, "json");
	}

	function savePostComments() {
		var obj = collectionData();
		// 这里会根据
		$.post(weburl + 'post/savePostComments',obj, function(data) {

			if(data.success){
                writeNewComment(data.comment);
                $.post( weburl+'admin/staticmgr/postcreatebyid?id='+obj.id, obj,
                    function( data ) {
                      //  console.log("文章已生成");
                    },
                    "json");
            }else{
              //如果失败，弹出登录框
                openLogin();
            }
		}, "json");
	}
   function writeNewComment(comment){
          var comentHtml = '<li><div class="textcommen"><div><img  src="'+comment.user_poto+'"'+
       'width="79" height="79" /> <span>'+comment.username+'</span>'+
           '<time>'+comment.comment_date+'</time>'+
        '<p>'+comment.comment_content+'</p>'+
        '<a href="#postMessage" class="f_r">回复</a> </div>  </div></li>';
           $("#postcomentsul").prepend(comentHtml);
    }
	function collectionData() {
		postMessage = $("#postMessage").val();
		postid = parms.id;
		return {
			id : postid,
			postMessage : postMessage
		};
	}
	return {
		updateAgreeCounts : updateAgreeCounts,
		getComments : getComments,
		updateLoadCounts : updateLoadCounts,
		savePostComments : savePostComments,
        updateZanCounts:updateZanCounts
	};
})();
