var grid="grid";
var key="id";
var searchform="searchform";
var dataform="dataform";
var url;
function getSelectItem() {
	var select = $("#" + grid).datagrid('getSelections');
	if (select == null || select.length == 0) {
		return null;
	}
	return select;
}

function getSingleSelectItem() {
	var select = $("#" + grid).datagrid('getSelected');
	return select;
}

var param={};

function convertObj(serialize){
	if(serialize == null){
		return;
	}
	var splits = serialize.split("&");
	if(splits == null){
		return;
	}
	var data={};
	for(var i=0; i<splits.length;i++){
		var item = splits[i];
		var varray = item.split("=");
		data[varray[0]] =varray[1]; 
	}
	param = data;
	return data;
}
function search(){
	
	var param = convertObj( $("#"+searchform).serialize());
	$('#'+grid).datagrid('load',param);
//    $.ajax({
//      type: "POST",
//      url: url+'/getpage',
//      data: $("#"+searchform).serialize(),
//      success:function(data){
//          var data = $.parseJSON(data);
//	    	$('#'+grid).datagrid('loadData',data);
//	    },
//      error: function () {
//      }
//  });
}

 function   clearSearch(){
	 param = {};
	  $('#'+searchform).form('clear');
	  search();
 }

function addOrModifyItem(){
	$.ajax({
        type: "POST",
        url: url+'/addOrModify',
       	data: $("#"+dataform).serialize(),
        success:function(data){
        	t_modelhide();
        	$('#'+grid).datagrid('reload',param);
	    },
        error: function () {
        }
    });
}


function removeitem(){
	$.messager.confirm('删除确定','确定要删除当前选择的记录!',function(r){
	    if (r){
	    	var select = getSelectItem();
			if(select == null || select.length == 0 ){
				$.messager.alert('错误','请选择要删除的记录!');
				return;
			}
			$.ajax({  
	            type: "POST",  
	            dataType: "json",  
	            url: url+'/delete',
	            data: { id: select[0][key] },  
	            success: function (data) {  
	             	$('#'+grid).datagrid('reload',param); 
	            },  
	            error: function () {  
	            }  
	        });  
	    }
	});
}

function model(model,type){ 
	 if(type == 0){
		 $('#'+model).modal('show');
	 }else{
		 $('#'+model).modal('hide');
	 }
	
}