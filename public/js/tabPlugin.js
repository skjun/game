jQuery.fn.pagination = function(size, func){
    this.each(function(){
        var target = jQuery(this).attr('data-target'),
            currentNo = 0,
			pageSize = size,
            items = jQuery('#' + target).find('[data-role="content"]'),
            length = items.length,
            previousBtn = jQuery(this).find('[data-role="previous"]'),
            nextBtn = jQuery(this).find('[data-role="next"]'),
            tabBtn = jQuery(this).find('[data-role="tab"]');
        items.each(function(index){
            if(!jQuery(this).hasClass('dn')){
                currentNo = index;
            }
    	});

    	if(length<=size){
    		jQuery(this).addClass("dn");
    	}
    	
    	previousBtn.unbind("click");
    	previousBtn.click(function(event){
            event.preventDefault();
            if(previousBtn.hasClass('disabled')){
                return;
            }
			for(var i=currentNo-pageSize+1; i<currentNo+1; i++){
				if(i<=(length - 1)){
					jQuery(items[i]).addClass('dn');
				}
			}
            
            currentNo -= pageSize;
			
			for(var i=currentNo-pageSize+1; i<currentNo+1; i++){
				 jQuery(items[i]).removeClass('dn');
			}
			
            if(currentNo <= pageSize-1){
				currentNo = pageSize-1;
                previousBtn.addClass('disabled');
            }else{
                previousBtn.removeClass('disabled');
            }
            nextBtn.removeClass('disabled');
            func();
        });
		nextBtn.unbind("click");
        nextBtn.click(function(event){
            event.preventDefault();
            if(nextBtn.hasClass('disabled')){
                return;
            }

			for(var i=currentNo-pageSize+1; i<currentNo+1; i++){
				 jQuery(items[i]).addClass('dn');
			}
			
            currentNo += pageSize;
			
			for(var i=currentNo-pageSize+1; i<currentNo+1; i++){
				if(i<=(length - 1)){
					jQuery(items[i]).removeClass('dn');
				}
			}

			if(currentNo >= (length - 1)){
                nextBtn.addClass('disabled');
            }else{
                nextBtn.removeClass('disabled');
            }
            previousBtn.removeClass('disabled');
            func();
        });

    });
    return this;
};