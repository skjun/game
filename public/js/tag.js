var index = {

	operator : "date",
		
	loadPosts : function() {
		var searchParms = index.getArgs();
		if(searchParms.categoryid!=""){
			// 设置请求地址参数
			ListBox.parms = {"tagid": searchParms.tagid};
		}
		// 设置请求地址
		ListBox.url = 'tag/loadPosts';

		ListBox.init();
	},

    comparatorDate : function(a, b) {
        return $(a).data('date') < $(b).data('date') ? -1 : 1;
    },
    comparatorPopularity :  function(a, b) {
        return $(a).data('popularity') > $(b).data('popularity') ? -1 : 1;
    },
	onClickSortBy : function(event) {
		event.preventDefault();
		var searchParms = index.getArgs();

		$currentSortby = $(this);

		switch ($currentSortby.data('sortby')) {
		case 'date':
			index.operator = "date";
			ListBox.options.comparator = index.comparatorDate;
			break;
		case 'popularity':
			index.operator = "popularity";
			ListBox.options.comparator = index.comparatorPopularity;
			break;
		}
		
		//重置分页条件
		
		ListBox.size = 30;
		if(index.operator == "date"){
			//请求参数
			ListBox.currSize = 0;
			if(searchParms.tagid!=""){
				ListBox.parms = {"tagid": searchParms.tagid};
			}else{
				ListBox.parms = {};
			}
		}
		if(index.operator == "popularity"){
			//请求参数
			ListBox.currSize = 0;
			if(searchParms.tagid!=""){
				ListBox.parms = {"tagid": searchParms.tagid, "order": "click_count"};
			}else{
				ListBox.parms = {"order": "click_count"};
			}
		}
		//清除ul数据
		$('#tiles').empty();
		//写数据
		ListBox.loadData();
		
		$('#tiles li img').imagesLoaded(function(instance) {
			var img = instance.images;
			$.each( img, function(i, n){
				if(!n.isLoaded){
					$(n.img).css("height", "318px");
				}
			});
			var $handler = $('#tiles li'), $sortbys = $('#sortbys a');
			ListBox.handler = $handler;
			$sortbys.removeClass('acit');
			$currentSortby.addClass('acit');
			
			if (ListBox.handler.wookmarkInstance){
	            ListBox.handler.wookmarkInstance.clear();
	        }
			ListBox.handler.wookmark(ListBox.options);
	     });
        //重置参数
        ListBox.loadStatus = true;
        ListBox.currSize = 30;
        ListBox.size = 10;
	},
	getArgs : function(){
	    var args = {};
	    var match = null;
	    var search = window.location.href;
	    var reg = /[0-9]*$/g;
	    match = reg.exec(search);
	    args.tagid = match[0];
	    return args;
	}
};

/** 初始化一些事件, 或者加载页面 */
$(function() {
	// 改为模板
	index.loadPosts();
	
	$('#tiles').imagesLoaded(function() {
		$('#sortbys a').click(index.onClickSortBy);
  });
});
