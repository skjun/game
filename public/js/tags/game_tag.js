/**
 * Created by yuyue on 13-11-26.
 */
    //程序的开始，最好还是放在文档加载完以后
$(document).ready(function(){
    grid.loadgrid();
    $("#find_btn").click(grid.searchtag);
    $("#addbtn").click(grid.addtag);
    loadTagLans();
    initIndex();
//    //文件上传组件处理
//  	var button = jQuery('#selectfile');//绑定事件
//  	var load = new AjaxUpload(button, {//绑定AjaxUpload
//  		action: weburl+"admin/fileupload/fileup",//文件要上传到的处理页面,语言可以PHP,ASP,JSP等
//  		type:"POST",//提交方式
//  		data:{//还可以提交的值
//          	module:"ajaxupload",
//          	type:jQuery("#__mstype__").attr("value"),
//      	},
//      	autoSubmit:true,//选择文件后,是否自动提交.这里可以变成true,自己去看看效果吧.
//      	name:'msUploadFile',//提交的名字
//      	onChange:function(file,ext){//当选择文件后执行的方法,ext存在文件后续,可以在这里判断文件格式
//      		$("#tag_img").attr("value",file);
//      	},
//      	onSubmit: function (file, ext) {//提交文件时执行的方法
//      		if(ext && /^(jpg|jpeg|png|gif)$/.test(ext)){
//      			//ext是后缀名
//      			button.value = "正在上传…";
//      			button.disabled = "disabled";
//      		}else{	
//      			alert("不支持的文件格式！只能上传jpg|jpeg|png|gif") ;
//      			return false;
//      		}
//      	},
//      	onComplete: function (file, response) {//文件提交完成后可执行的方法
//      		button.text('浏览');
//      		this.enable();
//      		var data=eval("("+response+")");
//      		if(data.type==0){
//      			alert(data.message);
//      		}else{
//      			$('#tag_img').val(data.filename);
//      		}
//      	}
//  	});
  	
  	
  	var m_button = jQuery('#m_selectfile');//绑定事件
  	var load = new AjaxUpload(m_button, {//绑定AjaxUpload
  		action: weburl+"admin/fileupload/fileup",//文件要上传到的处理页面,语言可以PHP,ASP,JSP等
  		type:"POST",//提交方式
  		data:{//还可以提交的值
          	module:"ajaxupload",
          	type:jQuery("#__mstype__").attr("value"),
      	},
      	autoSubmit:true,//选择文件后,是否自动提交.这里可以变成true,自己去看看效果吧.
      	name:'msUploadFile',//提交的名字
      	onChange:function(file,ext){//当选择文件后执行的方法,ext存在文件后续,可以在这里判断文件格式
      		$("#m_tagimg").attr("value",file);
      	},
      	onSubmit: function (file, ext) {//提交文件时执行的方法
      		if(ext && /^(jpg|jpeg|png|gif)$/.test(ext)){
      			//ext是后缀名
      			m_button.value = "正在上传…";
      			m_button.disabled = "disabled";
      		}else{	
      			alert("不支持的文件格式！只能上传jpg|jpeg|png|gif") ;
      			return false;
      		}
      	},
      	onComplete: function (file, response) {//文件提交完成后可执行的方法
      		m_button.text('浏览');
      		this.enable();
      		var data=eval("("+response+")");
      		if(data.type==0){
      			alert(data.message);
      		}else{
      			$('#m_tagimg').val(data.filename);
      		}
      	}
  	});

});
function initIndex(){
	var content = "<option value=''></option>";
	var lang = null;
	for(var i=1;i<=20;i++){
		content += "<option value='"+i+"'>"+i+"</option>";
	}
	$("#index").html(content);
	
}
function loadTagLans(){
	$.post( weburl+'admin/gametags/getTagLans', {}, function( data ) {
		var content = "<option value=''></option>";
		var lang = null;
		for(var key in data){
			if(lang == null){
				lang = data[key].language;
			}
			content += "<option value='"+data[key].language+"'>"+data[key].showlanguage+"</option>";
		}
		$("#searchlanguage").html(content);
		
    	}, "json");
}
var grid = (function (){
 function load(){
     $("#list2").jqGrid({
         url:weburl+'admin/gametags/datalist',
         datatype: "json",
         mtype: 'POST',
         colNames:['ID','LANGUAGE','TAG_NAME','TAG_IMG','GAME_COUNT','INDEX','EDIT'],
         colModel:[
             {name:'tag_id',  width:150,sortable:false},
             {name:'language',  width:150,sortable:false},
             {name:'tag_name',  width:150,sortable:false},
             {name:'tag_img',  width:250,sortable:false},
             {name:'num',  width:100,sortable:false},
             {name:'tag_index',  width:100,sortable:false},
             {name:'tag_id',  width:200,sortable:false,formatter:customFmatter}

         ],
         autowidth: true, // 自动匹配宽度
         height:500,   // 设置高度
         gridview:true, // 加速显示
         viewrecords: true,  // 显示总记录数
         rowNum:'20',
         pager: '#pager2',
         viewrecords: true
     });
     jQuery("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false},{},{},{},{multipleSearch:true, multipleGroup:true});
 }
    function customFmatter(cellvalue, options, rowObject){
        // cellvalue - 当前cell的值
        // options - 该cell的options设置，包括{rowId, colModel,pos,gid}
        // rowObject - 当前cell所在row的值，如{ id=1, name="name1", price=123.1, ...}
        return "<button class='btn btn-primary' tagid='"+rowObject.tag_id+"' tagname='"+rowObject.tag_name+"' tagimg='"+rowObject.tag_img+"' index='"+rowObject.tag_index+"' onclick=\"grid.updatetag(this)\">修改</buttion><button class='btn' data='"+rowObject.tag_id+"'  onclick=\"grid.deleteData(this)\">删除</buttion> "
    }
    function deleteData(ele){

        if(confirm('Delete?')){
        $.post( weburl+'admin/gametags/delTag', {"tagid":$(ele).attr('data')}, function( data ) {
            search();
        }, "json");

        }
    }
    function search(){
            var tagname = $("#find_txt").val();
            var language = $("#searchlanguage").val();
            $("#list2").jqGrid('setGridParam',{
                url:weburl+'admin/gametags/datalist',
                postData:{'tagname':tagname,'language':language}, //发送数据
                page:1
            }).trigger("reloadGrid"); //重新载入

    }
    function addtag(){

        var tagname = $("#tag_name").val();
        var tag_img = $("#tag_img").val();
        //这里会根据
        $.post( weburl+'admin/gametags/addTag', {"tagname":tagname,"tagimg":tag_img}, function( data ) {
        	if(data.id != null ){
        		alert("新增成功！");
        	}else{
        		alert("新增失败！");
        	}
        	$("#tag_img").val('');
        	$("#tag_name").val('');
            search();
         }, "json");
    }
    function updatetag(ele){
        $("#myModal2").modal('show');
        $("#m_tagname").val($(ele).attr('tagname'));
        $("#m_tagid").val($(ele).attr('tagid'));
        $("#m_tagimg").val($(ele).attr('tagimg'));
        $("#index").val($(ele).attr('index'));

    }
    function updatetagform(){
        $.post(weburl+'admin/gametags/updateTag', {"tagname": $("#m_tagname").val(),"tagimg": $("#m_tagimg").val(),"tagid": $("#m_tagid").val(),"index":$("#index").val()}, function( data ) {
            search();
            winclose();
        }, "json");
    }
    function winclose(){
        $("#myModal2").modal('hide');
    }
 return {
     loadgrid:load,
     searchtag:search,
     addtag:addtag,
     deleteData:deleteData,
     updatetag:updatetag,
     winclose:winclose,
     updatetagform:updatetagform
 }
})();