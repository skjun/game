/**
 * Created by yuyue on 13-11-26.
 */
    //程序的开始，最好还是放在文档加载完以后
$(document).ready(function(){
    grid.loadgrid();
    $("#find_btn").click(grid.searchtag);
    $("#addbtn").click(grid.addtag);
});

var grid = (function (){
 function load(){
     $("#list2").jqGrid({
         url:weburl+'admin/tags/datalist',
         datatype: "json",
         mtype: 'POST',
         colNames:['标签名称','标签描述','点击数','创建时间','操作'],
         colModel:[
             {name:'tag_name',  width:150,sortable:false},
             {name:'description',  width:250,sortable:false},
             {name:'count',  width:100,sortable:false},
             {name:'c_date',  width:200,sortable:false},
             {name:'tag_id',  width:200,sortable:false,formatter:customFmatter}

         ],
         autowidth: true, // 自动匹配宽度
         height:500,   // 设置高度
         gridview:true, // 加速显示
         viewrecords: true,  // 显示总记录数
         rowNum:'20',
         pager: '#pager2',
         viewrecords: true
     });
     jQuery("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false},{},{},{},{multipleSearch:true, multipleGroup:true});
 }
    function customFmatter(cellvalue, options, rowObject){
        // cellvalue - 当前cell的值
        // options - 该cell的options设置，包括{rowId, colModel,pos,gid}
        // rowObject - 当前cell所在row的值，如{ id=1, name="name1", price=123.1, ...}
        return "<button class='btn btn-primary' tagid='"+rowObject.tag_id+"' tagname='"+rowObject.tag_name+"' desc='"+rowObject.description+"' onclick=\"grid.updatetag(this)\">修改</buttion><button class='btn' data='"+rowObject.tag_id+"'  onclick=\"grid.deleteData(this)\">删除</buttion> "
    }
    function deleteData(ele){

        if(confirm('确定将此记录删除?')){
        $.post( weburl+'admin/tags/delTag', {"tagid":$(ele).attr('data')}, function( data ) {
            search();
        }, "json");

        }
    }
    function search(){
            var tagname = $("#find_txt").val();
            $("#list2").jqGrid('setGridParam',{
                url:weburl+'admin/tags/datalist',
                postData:{'tagname':tagname}, //发送数据
                page:1
            }).trigger("reloadGrid"); //重新载入

    }
    function addtag(){
        var tagname = $("#tag_name").val();
        var tag_desc = $("#tag_desc").val();
        //这里会根据
        $.post( weburl+'admin/tags/addTag', {"tagname":tagname,"desc":tag_desc}, function( data ) {
            search();
         }, "json");
    }
    function updatetag(ele){
        $("#myModal2").modal('show');
        $("#m_tagname").val($(ele).attr('tagname'));
        $("#m_tagid").val($(ele).attr('tagid'));
        $("#mtag_desc").val($(ele).attr('desc'));

    }
    function updatetagform(){
        $.post(weburl+'admin/tags/updateTag', {"tagname": $("#m_tagname").val(),"desc": $("#mtag_desc").val(),"tagid": $("#m_tagid").val()}, function( data ) {
            search();
            winclose();
        }, "json");
    }
    function winclose(){
        $("#myModal2").modal('hide');
    }
 return {
     loadgrid:load,
     searchtag:search,
     addtag:addtag,
     deleteData:deleteData,
     updatetag:updatetag,
     winclose:winclose,
     updatetagform:updatetagform
 }
})();