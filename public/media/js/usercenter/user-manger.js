var UsManage = function() {
	var userData;
	/** save user msg */
	function saveUserMsg() {
		if($('#userMsgForm').valid()){
			$.post('userinfo/saveUserMsg', collectionUserMsg(), function(data) {
				if (data.result == 1) {
					alert("modify success");
					
					//重置form校验器
			        $("label.error").hide();
			        $(".error").removeClass("error"); 
			        $("label.success").hide();
			        $(".success").removeClass("success"); 
			        $(".help-inline").remove();
					$('#userMsgForm').validate().resetForm();
				} else {
					  window.location.href = "login";
				}
			}, "json");
		}
		

	}
	/** reset user msg */
	function resetUserMsg() {
		// set ajax sysny
		getUserMsg();
	}
	function collectionUserMsg() {
		// set ajax sysny
		return {
			username : $("#username").attr('value'),
			email : $("#email").attr('value'),
			contact_email : $("#contact_email").attr('value'),
			phone : $("#phone").attr('value'),
			tworfb : $("#tworfb").attr('value')
		};
	}

	/** get user msg */
	function getUserMsg() {
		//重置form校验器
        $("label.error").hide();
        $(".error").removeClass("error"); 
        $("label.success").hide();
        $(".success").removeClass("success"); 
        $(".help-inline").remove();
        
		$.post('userinfo/getMsg', {}, function(data) {
			$("#username").attr('value', data.username);
			$("#email").attr('value', data.email);
			$("#contact_email").attr('value', data.contact_email);
			$("#phone").attr('value', data.phone);
			$("#tworfb").attr('value', data.tworfb);
		}, "json");
	}
	function initUserMgs() {
		getUserMsg();
		$("#usermsgsave").click(saveUserMsg);
		$("#usermsgback").click(resetUserMsg);
	}

	
	//用户个人网站信息列表页. 使用的方法列表
	function deleteUserSite(element){
		$.post('usersiteinfo/deleteUserSite', {id:element.id}, function(data) {
			if (data == 1) {
				alert("Success");
			}  
			 location.reload();
		}, "json");
	}
	function editUserSite(element){
		window.location.href = "usersiteinfo/addNewPage?id="+element.id;
	}
	function addUserSite(){
 		  window.location.href = "usersiteinfo/addNewPage";
	}
 
 
	
	return {
		initUserMgs : initUserMgs
	};

}();