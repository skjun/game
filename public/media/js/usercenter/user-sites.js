var UserSitesManage = function () {
    return {
        //main function to initiate the module
        init: function () {
    		//初始化table
    		initTable();
    		//
    		jQuery("#addnew").click(addNewSite);
    		jQuery("#edititem").click(editCurSite);
    		jQuery("#delitem").click(deleteUserSite);
    		
    		//网站编辑列表
    		$("#usreSiteInfoSave").click(saveUserSite);
    		$("#usreSiteInfoClose").click(closeUserSite);
    		//
    		//用户网站信息列表.
//    		editUserSite:editUserSite,
//    		deleteUserSite:deleteUserSite,
//    		addUserSite:addUserSite,
    		//用户网站信息的添加,修改
//    		initUserSiteInfo:initUserSiteInfo
        }
    };
    function addNewSite(){
    	$("#siteid").attr('value',"");
    	$("#sitename").attr('value',"");
		$("#siteurl").attr('value',"");
		$("#sitedesc").attr('value',"");
      
        
		resetForm();
    	jQuery("#usersites_mymodal").modal('show');
    	
    }
    function resetForm()
    {  
    	//重置form校验器
        $("label.error").hide();
        $(".error").removeClass("error"); 
        $("label.success").hide();
        $(".success").removeClass("success"); 
        $(".help-inline").remove();
    	
    }
 
    function closeUserSite(){
    	jQuery("#usersites_mymodal").modal('hide');
    }
    
    function editCurSite(){
    	var ids = getSelectIds();
    	if(ids == null){
    		alert("请选择一条记录进行修改");
    		return;
    	}
    	var idArr = ids.split(',');
    	if(idArr.length != 1){
    		alert("请选择一条记录进行修改");
    		return;
    	}
    	
    	$.post('usersiteinfo/getSingleUserSite', {id:idArr[0]}, function(data) {
    		resetForm();

    		$("#siteid").attr('value',data.id);
        	$("#sitename").attr('value',data.site_name);
    		$("#siteurl").attr('value',data.site_url);
    		$("#sitedesc").attr('value',data.site_desc);
        	jQuery("#usersites_mymodal").modal('show');
        	
//			 location.reload();
		}, "json");
    	
    }
  //用户个人网站信息列表页. 使用的方法列表
	function deleteUserSite(){
		var ids = getSelectIds();
		if(ids == ""){
			alert("请选择要删除的记录列表");
			return;
		}
		$.post('usersiteinfo/deleteUserSite', {ids:ids}, function(data) {
			 location.reload();
		}, "json");
	}
	function getSelectIds(){
		var ids;
		 var set = jQuery('#usersites_table .group-checkable').attr("data-set");
         jQuery(set).each(function () {
               if( $(this).attr("checked") =="checked"){
            	   if(ids ==null){
            		   ids=$(this).attr("value");
            	   }else{
            		   ids+=","+$(this).attr("value");
            	   }
            	   
               }
         });
         return ids;
	}
	function editUserSite(element){
		window.location.href = "usersiteinfo/addNewPage?id="+element.id;
	}
	function saveUserSite(){
		if($('#user_site_form').valid()){
			var cdata = collectionUserSiteInfo();
			$.post('usersiteinfo/saveUserSite',cdata, function(data) {
					jQuery("#usersites_mymodal").modal('hide');
//						$('#usersites_table').dataTable().fnAddData([
//					                                      '<input type="checkbox" class="checkboxes" value="'+data.id+'" />',
//					                                      data.site_name,
//					                                      data.site_url,
//					                                      data.site_desc, data.create_date]
//					                                    );
						
					location.reload();
			}, "json");
			
		}
	}
	
	function collectionUserSiteInfo() {
		// set ajax sysny
		return {
			siteid : $("#siteid").attr('value'),
			name : $("#sitename").attr('value'),
			siteurl : $("#siteurl").attr('value'),
			desc : $("#sitedesc").attr('value') 
		};
	}
 
	
    function initTable() {
        
        if (!jQuery().dataTable) {
            return;
        }
        // begin second table
        $('#usersites_table').dataTable({
            "aLengthMenu": [
                [10, 30, 50, -1],
                [10, 30, 50, "All"] // change per page values here
            ],
            // set the initial value
            "iDisplayLength": 10,
            "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "sPaginationType": "bootstrap",
            "oLanguage": {
                "sLengthMenu": "_MENU_ per page",
                "oPaginate": {
                    "sPrevious": "Prev",
                    "sNext": "Next"
                }
            },
            "aoColumnDefs": [{
                    'bSortable': false,
                    'aTargets': [0]
                }
            ]
        });

        jQuery('#usersites_table .group-checkable').change(function () {
            var set = jQuery(this).attr("data-set");
            var checked = jQuery(this).is(":checked");
            jQuery(set).each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                } else {
                    $(this).attr("checked", false);
                }
            });
            jQuery.uniform.update(set);
        });

//        jQuery('#usersites_table_wrapper .dataTables_filter input').addClass("m-wrap small"); // modify table search input
//        jQuery('#usersites_table_wrapper .dataTables_length select').addClass("m-wrap small"); // modify table per page dropdown
//        jQuery('#usersites_table_wrapper .dataTables_length select').select2(); // initialzie select2 dropdown
       
    }

}();