<?php
class IndexDataService{

	public  $dbutil;
	function __construct($dbutil){
		$this->dbutil =  $dbutil;
	}


	/**
	 * get User Basic Config
	 * Enter description here ...
	 */
	function getUserConfig(){
		$config['ln'] = "cn";
		$config['theme'] = "default";
		return $config;
	}
	//初始化游戏语言列表
	function getGameLans(){
		$sql = "select g.language,count( g.language) count  from game g GROUP BY g.language ORDER BY count desc";
		return $this->dbutil->get_results($sql);
	}
	//获取默认的语言
	function getDefaultLan(){
		$dln;
		$result = $this->dbutil->get_row("select value from tb_system_param where name = 'defaultgamelanguage'");
		if(!empty($result) &&!empty($result->value) ){
			$dln = $result->value;
		}else {
			$sql = "select g.language,count( g.language) count  from game g GROUP BY g.language ORDER BY count desc";
		 	$resultlist = $this->dbutil->get_row($sql);
		 	if(!empty($resultlist)){
		 		$dln = $resultlist->language;
		 	}
		}
		return $dln;
	}
	function  loadMainNavs(){
		$sql = "SELECT DISTINCT g.gametype FROM game g";
		return $this->dbutil->get_results($sql);
	}
	//获取类型下的 子类型列表
	function loadSubMainItem($titlle,$cacheln){
		$sql = "SELECT g.genre_name FROM game g where g.gametype='$titlle' and g.language='$cacheln' and g.genre_name != '' GROUP BY g.genre_name ORDER BY count(g.genre_name) desc limit 10";
		$results = $this->dbutil->get_results($sql);

        $main = $_SESSION['mainNavItem'];
        $submain =  $_SESSION['subNavItem'];
        if($main == $titlle){
            $state = $submain;
        }

		$datas =array();

		foreach ($results as $item){

            if($item->genre_name == $state){
                $datas[] = array(
                    'genre_name'=>$item->genre_name,
                    'href' =>WEBSITE_URL."index/show/m".$titlle."_s".$item->genre_name.".html",
                    'state'=>'active'
                );
            }else{
                $datas[] = array(
                    'genre_name'=>$item->genre_name,
                    'href' =>WEBSITE_URL."index/show/m".$titlle."_s".$item->genre_name.".html",
                    'state'=>''
                );
            }


		}
		return $datas;
	}

	/**
	 *  加载游戏分页数据
	 * Enter description here ...
	 * @param unknown_type $start
	 * @param unknown_type $page_size
	 * @param unknown_type $query
	 */
	public function loadLanGames($start, $page_size,$query) {
		$limit = "";
		if ($page_size) {
			$limit = " limit $start,$page_size ";
		}
		$sql = "select game_id,game_name,img_feature img from game ";
		if(!empty($query)){
			$sql.=" where 1=1 ".$query;
		}
		$sql.=" order by id $limit";

		return $this->dbutil->get_results ( $sql );
	}
	public function countNum($query) {
		$sql = "select count(id) countnum from game";
		if(!empty($query)){
			$sql.=" where 1=1 ".$query;
		}
		$resutrnarry = $this->dbutil->get_results ( $sql );
		return $resutrnarry [0]->countnum;
	}
	public function countTagGames($query) {
		$sql = "select   count(id) countnum from relations t   RIGHT JOIN game g on  t.obj_id = g.id where  1=1 ".$query;
		$resutrnarry = $this->dbutil->get_results ( $sql );

		return $resutrnarry [0]->countnum;
	}
	/**
	 * 通过gameid 获取game信息
	 * Enter description here ...
	 * @param $id
	 */
	public function getGameItem($id){
		$sql = "select g.id,g.site,g.gametype,g.longdesc,g.video,g.price,g.game_id,g.game_name,g.genre_name,CEIL(g.gamesize/1024/1024) gamesize,g.systemreq,g.shortdesc, date_format(g.releasedate,'%Y-%m-%d') releasedate ,g.img_thumb1,g.img_thumb2,g.img_thumb3 ,g.img_screen1,g.img_screen2,g.img_screen3 , g.downloadurl, g.downloadiframe,g.buyurl from game g where g.game_id ='".$id."'";
		$game = $this->dbutil->get_row($sql);
		//	"OS: Windows XP/Windows Vista/Windows 7/Windows 8 / CPU: 1.8 GHz / RAM: 512 / DirectX: 8.1 / 515"
		$rarr = array(
			'0'=>'OS:',
			'1'=>'CPU:',
			'2'=>'RAM:',
			'3'=>'DirectX:'
			);
			foreach ($rarr as $item){
				$game->systemreq = str_replace($item,'!@#$'.$item,$game->systemreq);
			}
			$results = explode('!@#$',$game->systemreq);
			foreach ($results as $str){
				if($str == null){
					continue;
				}
				$keyvalues = explode(":",$str);
				$game->$keyvalues[0] = $keyvalues[1];
			}
			return $game;
	}
	
	public function prepareGameAffMsg($id){
		$sql = "select  * from game g where g.game_id ='".$id."'";
		$game = $this->dbutil->get_row($sql);
		return $game;
	}
	
	/**
	 * 获取热门标签列表
	 * Enter description here ...
	 */
	public function getHotTags(){
		$cacheln = $_SESSION["language"];
		$sql = "select t.tag_img,t.tag_name, r.tag_id,count(r.tag_id) cc from relations r ,tags t where t.tag_id = r.tag_id and t.language='".$cacheln."' GROUP BY r.tag_id order by IF(ISNULL(t.tag_index),1,0),t.tag_index ,cc desc limit 14";
		$results = $this->dbutil->get_results($sql);

		foreach ($results as $item){
            $item->href = WEBSITE_URL."index/listTagGames/t".$item->tag_id."_".$item->tag_name.".html";
			if($item->tag_img == null){
				$item->tag_img = WEBSITE_URL."theme/photo/tagicon1.jpg";
			}
		}
		return $results;
	}
	/**
	 * 加载标签下对应的游戏列表
	 * Enter description here ...
	 * @param unknown_type $start
	 * @param unknown_type $page_size
	 * @param unknown_type $query
	 */
	public function loadTagGames($start, $page_size,$query) {
		$limit = "";
		if ($page_size) {
			$limit = " limit $start,$page_size ";
		}
		$sql = "select tag_id,game_id,game_name,img_feature img from (select  * from relations t   RIGHT JOIN game g on  t.obj_id = g.game_id where  1=1 ".$query.$limit.") ng";
		//$sql = "select  * from relations t , game g where t.obj_id = g.game_id ".$query.$limit;
		return $this->dbutil->get_results ( $sql );
	}

	/**
	 * 加载热门游戏列表
	 * Enter description here ...
	 * @param unknown_type $query
	 */
	public function loadLanHotGames($query){
		$sql = "select game_id,game_name,img_feature img from   game g  where  1=1 ".$query." limit 24";
		return $this->dbutil->get_results ( $sql );
	}
	public function getGameTags($gid){
		$sql = "select * from relations g, tags t   where g.tag_id = t.tag_id and g.obj_id = ".$gid;
		$results = $this->dbutil->get_results($sql);
		foreach ($results as $item){
			$item->href = WEBSITE_URL."index/listTagGames/t".$item->tag_id."_".$item->tag_name.".html";
			if($item->tag_img == null){
				$item->tag_img = WEBSITE_URL."theme/photo/tagicon1.jpg";
			}
		}
		return $results;
	}
	public function getTagItem($tag_id){
		$sql = "select t.tag_name from tags t where t.tag_id = '".$tag_id."'";
		$result = $this->dbutil->get_row($sql);
		return $result;
	}
	public function getFriendLinks(){
		$sql = "SELECT sitename,siteurl,sitelogo FROM friendlinks  ORDER BY create_time desc limit 8";
		$result = $this->dbutil->get_results($sql);
		return $result;
	}
	


}