<?php
class CommonDataService {
	var $dbutil;
	function __construct($dbutil) {
		$this->dbutil = $dbutil;
	}
	 
	public function resourcePage($table,$start, $page_size, $condition) {
		$limit = "";
		if ($page_size) {
			$limit = " limit $start,$page_size ";
		}
		if (empty ( $condition )) {
			$sql = "select * from ".$table." ". $condition . " ".$limit;
		} else {
				$sql = "select * from ".$table." where ". $condition . " ".$limit;
		}
		// $sql = "select * from TCA_RESOURCE_INFO order by DEVICE_ID $limit";
	
		//print($sql);
		return $this->dbutil->get_results ( $sql );
	}
	
	public function countNum($table,$condition) {
		if (empty ( $condition )) {
			$sql = "select count(*) countnum from  ".$table." " . $condition;
		} else {
			$sql = "select count(*) countnum from  ".$table." where " . $condition;
		}
    	$resutrnarry=$this->dbutil->get_results($sql);
    	return $resutrnarry[0]->countnum;
	}
	public function delete($table,$condition) {
		$sql = "delete from ".$table." where ".$condition;
		return $this->dbutil->get_results($sql);
	}
	public function edit($table,$data, $conditions) {
		return $this->dbutil->update ($table , $data, $conditions );
	}
	
	public function exSql($sql) {
		return $this->dbutil->query($sql);
	}
	public function getResults($sql) {
		return $this->dbutil->get_results($sql);
	}
	public function getResult($sql) {
		return $this->dbutil->get_row($sql);
	}
	
	public function add($table,$data){
	    $this->dbutil->insert($table, $data);
        return $this->dbutil->getInsertId();
	}
	
}
?>