<?php
class CommonService{
	
	public  $dbutil;
    function __construct($dbutil){
	  	$this->dbutil =  $dbutil;
	}  
	
	public function dataPage($table,$start, $page_size ){
		$limit ="";
		if($page_size){
			$limit =" limit $start,$page_size ";
		}
		$sql = "select * from $table   $limit";
		return $this->dbutil->get_results($sql);
	}
	
	public function execute($sql){
		return $this->dbutil->get_results($sql);
	}
	
	public function dataPageSql($sql,$start, $page_size){
		$limit ="";
		if($page_size){
			$limit =" limit $start,$page_size ";
		}
		 
		$sql .= $limit;
		return $this->dbutil->get_results($sql);
	}
	public function dataPageWhere($table,$start, $page_size,$where){
		$limit ="";
		if($page_size){
			$limit =" limit $start,$page_size ";
		}
		 
		$sql = "select * from $table $where   $limit";
		 
		return $this->dbutil->get_results($sql);
	}
	public function countNum($table){
		$sql = "select count(*) countnum  from $table";
    	$resutrnarry=	$this->dbutil->get_results($sql); 
    	return $resutrnarry[0]->countnum;
	}
   public function countNumWhere($table,$where){
   	    $sql = "select count(*) countnum  from $table";
   	    if($where != ''){
   	    	$sql .=  " $where";
   	    } 
    	$resutrnarry=	$this->dbutil->get_results($sql); 
    	return $resutrnarry[0]->countnum;
	}
    public function countNumSql($sql){
        $resutrnarry=	$this->dbutil->get_results($sql);
        return $resutrnarry[0]->countnum;
    }
	public function insert($table,$data){
		$this->dbutil->insert($table,$data);
		return $this->dbutil->insert_id;
	}
    public function update( $table, $data, $where){
		$this->dbutil->update($table,$data, $where);
	}
	public function del($sql){
		$this->dbutil->query($sql);
		 
	}
	 
	public function delete($table,$condition) {
		$sql = "delete from ".$table." where ".$condition;
		return $this->dbutil->get_results($sql);
	}
	public function executeSql($sql){
		$this->dbutil->query($sql);
	}
	public function query($sql ){
        return $this->dbutil->get_results ( $sql );
    }
    public function getResult($sql ){
        return $this->dbutil->get_row( $sql );
    }

	public function insertItem($table,$column,$name){
		$sql = "insert into ".$table." (".$column.") values('" . $name . "')";
		if (self::countTableNum ( $table,$column,$name ) == 0) {
			$this->dbutil->get_results ( $sql );
			return 0;
		}else{
			return 1;
		}
	}
	
	public function countTableNum($table, $column, $where) {
		$sql = "select count(" . $column . ") countnum from " . $table . " where " . $column . " = '" . $where . "'";
		$resutrnarry = $this->dbutil->get_results ( $sql );
		return $resutrnarry [0]->countnum;
	}
}