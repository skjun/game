<?php

class LogService {
	
	private $logFile;

	private $logErrorFile;
	
	private $logContext = "";
		
	private $logErrorContext = "";
	
   	function __construct($prefix){
		$this->logFile = PROJECT."/logs/cms".$prefix.date("YmdHHis").".log";
		$this->logErrorFile = PROJECT."/logs/cms_error_".$prefix.date("YmdHHis").".log";
	}
	
	private function logCommon($file,$log,$wc){
		if(strlen($log) > $wc){		
			$fp = fopen($file, "a");
			$result = fwrite($fp, $log);
			fclose($fp);
			return $result;
		}else{
			return false;;
		}
	}
	
	function logInfo($log,$wc = 2048){
		$this->logContext = $this->logContext.$log;
		$result = $this->logCommon($this->logFile,$this->logContext,$wc);
		
		if($result){
			$this->logContext = "";
		}
		return $result;
	}
	
	function logError($log,$wc = 0){
		$this->logErrorContext = $this->logErrorContext.$log;
		$result = $this->logCommon($this->logErrorFile,$this->logErrorContext,$wc);
		if($result){
			$this->logErrorContext = "";
		}
		return $result;
	}
	
	function millisecond(){

		$time = explode (" ", microtime());
		$time = $time [1] .($time [0] * 1000);

		$time2 = explode ( ".", $time);
		$time = $time2 [0];

		return floatval($time);
	}

	function flush(){
		$result = $this->logCommon($this->logFile,$this->logContext,0);
		if($result){
			$this->logContext = "";
		}

		$result = $this->logCommon($this->logErrorFile,$this->logErrorContext,0);
		if($result){
			$this->logErrorContext = "";
		}
	}
}
?>