<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 14-10-29
 * Time: 下午9:22
 */

class PointsApprovalService {



    public  $dbutil;
    function __construct($dbutil){
        $this->dbutil =  $dbutil;
    }

    /**
     * @param $data
     * @param $id
     * @return mixed审批积分申请
     */
    public function approval($data,$id){
        $sql = "update user_point_apply set reply_userid='".$data['reply_userid'].
            "',reply_username='".$data['reply_username']."',state=".$data['state'].
            ",points=".$data['points'].",apply_time=sysdate() where id =".$id;
        return $this->dbutil->query($sql);
    }

    /**
     * 审批完积分申请后，增加一条用户积分消费记录
     * @param $record
     * @return mixed
     */
    public function addUserPointsRecord($record){
        return	$this->dbutil->insert("user_points_record", $record);
    }

    /**
     * 审核通过，邀请者积分增加
     * @param $record
     */
    public function addInviteUserPointsRecord($points,$id){
        $increase = $points*0.1;
        $sql = "update user_points_record a,users b set a.points = a.points +".$increase." ,a.record_type=0,a.points_type=4  where a.userid=b.invite_userid and b.userid='".$id."'";
        $this->dbutil->query($sql);
    }


    /**
     * 获取用户积分申请记录
     */
    public function getPointsApply($condition,$start,$pageSize){
        $sql = "select * from user_point_apply ";
        if(!empty($condition)){
            $sql .= " where ".$condition;
        }
        $sql .= " order by reply_time desc, state limit ".$start.",".$pageSize;
        return $this->dbutil->get_results($sql);
    }

    public function getPointsApplyCount($condition){
        $sql = "select count(1)num from user_point_apply ";
        if(!(empty($condition))){
            $sql .= " where ".$condition;
        }
        $resutrnarry=$this->dbutil->get_results($sql);
        return $resutrnarry[0]->num;
    }
}

?>