<?php
class PostCommentsService{
	
	public  $dbutil;
    function __construct($dbutil){
	  	$this->dbutil =  $dbutil;
	} 
	
	public function resourcePage($start, $page_size, $condition) {
		$limit = "";
		if ($page_size) {
			$limit = " limit $start,$page_size ";
		}
		if (empty ( $condition )) {
			$sql = "SELECT p.*,u.username,po.post_title FROM post_comments p LEFT JOIN users u ON p.user_id = u.userid LEFT JOIN posts po ON p.comment_post_ID = po.id  " . $condition . " order by comment_id $limit";
		} else {
			$sql = "SELECT p.*,u.username,po.post_title FROM post_comments p LEFT JOIN users u ON p.user_id = u.userid LEFT JOIN posts po ON p.comment_post_ID = po.id  where " . $condition . " order by comment_id $limit";
		}
		// $sql = "select * from TCA_RESOURCE_INFO order by DEVICE_ID $limit";
	
// 		print($sql);
		return $this->dbutil->get_results ( $sql );
	}
/**
	 * 根据用户名获取用户信息
	 * Enter description here ...
	 * @param unknown_type $name
	 */
	function getAdminByName($name){
		return $this->dbutil->get_row("select * from post_comments where user_name='".$name."'");
	}
	/**
	* 根据用户名获取用户信息
	* Enter description here ...
	* @param unknown_type $name
	*/
	function getAdminByID($comment_id){
		return $this->dbutil->get_row("select * from post_comments where comment_id='".$comment_id."'");
	}
	/**
	 * 检查登录
	 * @param unknown_type $name
	 * @param unknown_type $password
	 */
	function checkLogin($name, $password){
		$admin = $this->getAdminByName($name);
		if($admin){
			if($admin->admin_passwd == $password){
				return $admin;
			}else{
				return false;
			}
		}else{
			return false;
		}
		
	}
	
	
	public function userPage($start , $page_size ){
		$limit ="";
		if($page_size){
			$limit =" limit $start,$page_size ";
		}
		$sql = "
		SELECT 	p.*,post.post_title,u.username
		 FROM 
		 post_comments p LEFT JOIN posts post ON p.comment_post_id=post.id LEFT JOIN users u ON p.user_id = u.userid ORDER BY comment_id $limit";
		return $this->dbutil->get_results($sql);
	
	 
	}
	public function countNum(){
		$sql = "select count(comment_id) countnum from post_comments";
    	$resutrnarry=	$this->dbutil->get_results($sql);
    	return $resutrnarry[0]->countnum;
	}
	
	public function addAdUser($data){
		return	$this->dbutil->insert("post_comments", $data);
	}

	public function deleteUser($data){
		$sql = " delete from post_comments where comment_id= $data";
		return $this->dbutil->get_results($sql);
	}
	public function edit($data,$conditions){
		return $this->dbutil->update("post_comments",$data,$conditions);
	}
	public function getallrole() {
		$sql = "SELECT 	role_comment_id,  role_name,  role_desc, role_create_user_comment_id,  role_create_date FROM tb_roles  GROUP BY role_name ";
		return $this->dbutil->get_results ( $sql );
	}
}