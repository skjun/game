<?php
class SitemapService{

	public  $dbutil;
	function __construct($dbutil){
		$this->dbutil =  $dbutil;
	}

	public function createSitemapData(){
        $sql = "select * from sitemap";
        $sitemaps =	$this->dbutil->get_results($sql);
        $paramkv = array();
        for($i=0;$i<count($sitemaps);$i++){
            $paramkv[$sitemaps[$i]->id] = $sitemaps[$i];
        }
        $indexsitemap = $paramkv["index"];
        $urls = array();
        //index urls
        $indexUrls = array("loc"=>WEBSITE_URL,"lastmod"=>date('Y-m-d H:i:s'),"changefreq"=>$indexsitemap->changefreq,"priority"=>$indexsitemap->priority);
        //category urls
        $categoryUrls = $this->createCategoryUrls($paramkv["category"]);
        //tags urls
        $tagUrls = $this->createTagUrls($paramkv["tag"]);
        //posts urls
        $postUrls  = $this->createPostUrls($paramkv["post"]);
        //other pages urls

        $urls[] = $indexUrls;
        $urls = array_merge($urls,$categoryUrls);
        $urls = array_merge($urls,$tagUrls);
        $urls = array_merge($urls,$postUrls);
        return $urls;
	}
    public function createCategoryUrls($categorysitemap){
        $file_name = PROJECT."/model/cache/nav";
        $handle = fopen($file_name, "r");
        $contents = fread($handle, filesize($file_name));
        fclose($handle);
        $array = unserialize($contents);


        $categoryUrls = array();
        foreach($array as $pcategory){
            $purl = WEBSITE_URL.$pcategory['parent']->category_id."/index.html";
            $categoryUrls[]=array("loc"=>$purl,"lastmod"=> date('Y-m-d H:i:s'),"changefreq"=>$categorysitemap->changefreq,"priority"=>$categorysitemap->priority);
            if(!empty($pcategory['childres'])){
                foreach($pcategory['childres'] as $category){
                    $curl = WEBSITE_URL.$pcategory['parent']->category_id."/".$category->category_id."/index.html";
                    $categoryUrls[]=array("loc"=>$curl,"lastmod"=>date('Y-m-d H:i:s'),"changefreq"=>$categorysitemap->changefreq,"priority"=>$categorysitemap->priority);
                }
            }
        }

        return $categoryUrls;
    }
    public function createTagUrls($tagsitemap){
        $sql = "SELECT tag_id  FROM tags ";
        $result = $this->dbutil->get_results ( $sql );
        $tagUrls = array();
        foreach($result as $tag){
            $tagurl = WEBSITE_URL."tag/showtag/$tag->tag_id";
            $tagUrls[]=array("loc"=>$tagurl,"lastmod"=>date('Y-m-d H:i:s'),"changefreq"=>$tagsitemap->changefreq,"priority"=>$tagsitemap->priority);
        }
        return $tagUrls;
    }
    public function createPostUrls($postsitemap){
        $sql = "SELECT post_category,ID,post_date,post_modified  FROM posts where  post_status=0  ";
        $result = $this->dbutil->get_results ( $sql );
        $postUrls = array();
        require_once SERVICE . DS . 'IndexService.class.php';
        $indexService = new IndexService( $this->dbutil );
        $indexService->fillPostHref($result);
        foreach($result as $post){
             $postdate = $post->post_date;
            if(!empty($post->post_modified)){
                $postdate = $post->post_modified;
            }
            $postUrls[]=array("loc"=>WEBSITE_URL.$post->href,"lastmod"=>$postdate,"changefreq"=>$postsitemap->changefreq,"priority"=>$postsitemap->priority);
        }
        return $postUrls;
    }
}