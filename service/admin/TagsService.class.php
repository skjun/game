<?php
class TagsService{

	public  $dbutil;
	function __construct($dbutil){
		$this->dbutil =  $dbutil;
	}
	/**
	 *
	 * 总结果集
	 */
	public function postsPage($start , $page_size ,$condition){
		$limit ="";
		if($page_size){
			$limit =" limit $start,$page_size ";
		}
		if(empty($condition)){
			$sql = "SELECT post_title,  post_author, comment_count, post_date,  post_status, category_name FROM posts, post_category WHERE post_author=category_id".$condition." order by ID $limit";
		}else{
			$sql = "SELECT post_title,  post_author, comment_count, post_date,  post_status, category_name FROM posts, post_category WHERE post_author=category_id".$condition." order by ID $limit";
		}
		return $this->dbutil->get_results($sql);
	}
	/**
	 *
	 * 总记录数
	 */
	public function countNum() {
		$sql = "select count(1) countnum from posts";
		$resutrnarry = $this->dbutil->get_results ( $sql );
		return $resutrnarry [0]->countnum;
	}
	/**
	 *
	 * 获取所有分类的名称
	 * @deprecated
	 * 暂时不使用个方法
	 */
	public function getTags($postsID) {
		$sql = "SELECT post_id, tag_name  FROM tags t , post_tag pt WHERE t.tag_id = pt.tag_id and pt.post_id IN(".$postsID.")";
		return $this->dbutil->get_results($sql);
	}

    /**
     * @param $tagname
     * @return boolean if have this tag return tagid,else retrun false
     */
    public function getTagByTagName($tagname){
        $sql  = "select * from tags where tag_name ='$tagname'";
        $tags = $this->dbutil->get_results($sql);
        if(!empty($tags)){
            return $tags[0]->tag_id;
        }
        return false;
    }

	/**
     * @param $tagname
     */
    public function addGameTag($tagname,$tagimg)
    {
      $tagid = $this->getTagByTagName($tagname);
      if(!$tagid){
          $data = array("tag_name"=>$tagname,"tag_img"=>$tagimg);
          $this->dbutil->insert("tags",$data);;
          return $this->dbutil->insert_id;
      }
        return $tagid;
    }
    
    /**
     * @param $tagname
     */
    public function addTag($tagname,$desc)
    {
      $tagid = $this->getTagByTagName($tagname);
      if(!$tagid){
          $data = array("tag_name"=>$tagname,"description"=>$desc,"c_date"=>date("Y-m-d H:i",time()));
          $this->dbutil->insert("tags",$data);;
          return $this->dbutil->insert_id;
      }
        return $tagid;
    }
    /**
     * @param $tagid
     */
    public function delTag($tagid)
    {
        if($tagid){
            $deltagsql = "delete from tags where tag_id=".$tagid;

            $this->dbutil->query($deltagsql);
            $delpostsql = "delete from post_tag where tag_id=".$tagid;

            $this->dbutil->query($delpostsql);;
        }

    }
    
 /**
     * @param $tagid
     */
    public function delGameTag($tagid)
    {
        if($tagid){
            $deltagsql = "delete from tags where tag_id=".$tagid;

            $this->dbutil->query($deltagsql);
            $delpostsql = "delete from relations where tag_id=".$tagid;

            $this->dbutil->query($delpostsql);;
        }

    }
    
}