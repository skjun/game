{{include file ="admin/header.tpl"}}
{{include file ="admin/navibar.tpl"}}
{{include file ="admin/sidebar.tpl"}}
<script src="{{$smarty.const.WEBSITE_URL}}public/assets/js/admin/collect.js"></script>
<div class="content">

    <!-- 表格头部开始 -->
    <div class="header">
        <div class="stats"><p class="stat"></p>
        </div>
        <h1 class="page-title">Collect Manager</h1>
    </div>
    <!-- 表格头部结束 -->

	<div class="container-fluid">
		<div class="row-fluid">

            <div class="block">
                <a href="#page-stats" class="block-heading" data-toggle="collapse">Operation</a>
                <div id="page-stats" class="block-body collapse in">
                    <div class="stat-widget-container">
                        <div class="stat-widget">
                            <div class="stat-button">
                                <img id="loading" class="img-thumbnail hide" src="{{$smarty.const.WEBSITE_URL}}public/assets/images/loading2.gif" />
                                <button id='index' class="btn btn-primary">Collect Game</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>
<script src="{{$smarty.const.WEBSITE_URL}}public/assets/layer/layer.min.js"></script>

{{include file="admin/footer.tpl" }}

