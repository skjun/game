{{include file ="admin/header.tpl"}}
{{include file ="admin/navibar.tpl"}}
{{include file ="admin/sidebar.tpl"}}


<script src="{{$smarty.const.WEBSITE_URL}}public/assets/js/admin/game/game.js"></script>

<style type="text/css">
    div#rMenu {position:absolute; visibility:hidden; top:0; background-color: #555;text-align: left;padding: 2px;}
    div#rMenu ul li{
        margin: 1px 0;
        padding: 0 5px;
        cursor: pointer;
        list-style: none outside none;
        background-color: #DFDFDF;
    }
</style>

<div class="content">

    <!-- 表格头部开始 -->
    <div class="header">
        <div class="stats"><p class="stat"></p>
        </div>
        <h1 class="page-title">游戏管理</h1>
    </div>
    <!-- 表格头部结束 -->

    <ul class="breadcrumb">     </ul>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="block">
                <div id="page-menu" class="block-body collapse in">
                    <br />
                    <form class="form-search" method="post" id="searchform">
                        <div class="input-prepend">
                            <span class="add-on">游戏名称:</span> <input class="input-medium"
                                                                     type="text" placeholder="游戏名称" name="game_name" id="game_name"
                                                                     style="width: 80px;">
                        </div>
                        <div class="input-prepend">
                            <span class="add-on">平台类型:</span>
                            <select name="gametype" id="game_type" style="width: 80px;">
                                <option value="pc">pc</option>
                                <option value="og">online game</option>
                                <option value="mac">mac</option>
                                <option value="andriod">andriod</option>
                            </select>
                        </div>
                        <div class="input-prepend">
                            <span class="add-on">语言:</span>
                            <select name="languge" id="languge" style="width: 80px;">
                            </select>
                        </div>
                        <div class="input-prepend">
                            <span class="add-on">类型:</span>
                            <select name="genre_name" id="genre_name" style="width: 80px;">
                            </select>
                        </div>
                        <div class="input-prepend">
                            <span class="add-on">发布日期从:</span>
                            <input class="input-medium" type="text" placeholder="时间从" readonly="readonly" name="startTime" id="startTime" style="width: 100px;">
                            <span class="add-on">到:</span> <input class="input-medium" readonly="readonly" type="text" placeholder="到" name="endTime" id="endTime" style="width: 100px;">
                        </div>

                        <button type="button" class="btn btn-primary" id="form_search">查询</button>
                        <button type="button" class="btn " id="reset">重置</button>
                    </form>
                </div>
            </div>
        </div>

        <div class="row-fluid">
            <div class="bb-alert alert alert-info" style="display: none;">
                <span>操作成功</span>
            </div>


            <div id="myTabContent" class="tab-content">
                <table id="grid"></table>
                <div id="gridPager"></div>
            </div>
        </div>
        
        <!-- Modal -->
        <div  id="myModal2" class="modal hide fade">
        	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
             	<h3>修改游戏</h3>
           	</div>
         	<div class="modal-body">
          		<form class="form-horizontal">
                	<div class="control-group">
                		<input type="hidden"" id="gameid" name="gameid" />
	                	<label class="control-label" for="inputEmail">游戏名称</label>
	                  	<input type="text" id="gamename" name="gamename"  readonly="readonly"/>
					</div>
					<div class="control-group">
						<label class="control-label">简短描述</label>
	                    <textarea name="shortdesc" id="shortdesc">
	                    </textarea> 
	              	</div>
	              	<div class="control-group">
	                    <label class="control-label">中描述</label>
	                    <textarea type="textarea" name="meddesc" id="meddesc"/> 
	                    </textarea>
	                </div>
	                <div class="control-group">
                    	<label class="control-label">长描述</label>
                    	<textarea type="textarea" name="longdesc" id="longdesc"/></textarea>
                    </div>
              	</form>
          	</div>
       		<div class="modal-footer">
       			<a href="javascript:void(0)" onclick="gridCompnent.modalClose()" class="btn">关闭</a>
				<a href="javascript:void(0)" onclick="gridCompnent.save();" class="btn btn-primary">保存</a>
      		</div>
        </div>
      	<!-- Modal end -->
    </div>
</div>


{{include file="admin/footer.tpl" }}
