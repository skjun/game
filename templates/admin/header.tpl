<!DOCTYPE html>
<html>
  	<head>
  		<base href="{{$smarty.const.WEBSITE_URL}}" />
   		<meta charset="utf-8">
    	<title>{{$page_title}} - {{$smarty.const.ADMIN_TITLE}} - </title>
    	<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	<meta name="description" content="">
    	<meta name="author" content="">
    	
		
		<link rel="stylesheet" href="{{$smarty.const.WEBSITE_URL}}public/assets/lib/bootstrap/css/bootstrap.css">
 <!--	<link rel="stylesheet" href="{{$smarty.const.WEBSITE_URL}}public/assets/stylesheets_blacktie/theme.css"> -->
    	<link rel="stylesheet" href="{{$smarty.const.WEBSITE_URL}}public/assets/stylesheets_{{$smarty.session.template}}/theme.css">
    	<link rel="stylesheet" href="{{$smarty.const.WEBSITE_URL}}public/assets/lib/font-awesome/css/font-awesome.css">
		<link rel="stylesheet" href="{{$smarty.const.WEBSITE_URL}}public/assets/css/other.css">
		<link rel="stylesheet" href="{{$smarty.const.WEBSITE_URL}}public/assets/css/jquery-ui.css" />
		<link rel="stylesheet" href="{{$smarty.const.WEBSITE_URL}}public/assets/css/jquery.ui.datepicker.css" />
		<link rel="stylesheet" href="{{$smarty.const.WEBSITE_URL}}public/assets/lib/jquery.jqGrid-4.5.2/css/ui.jqgrid.css" />
		
		<link rel="stylesheet" href="{{$smarty.const.WEBSITE_URL}}public/assets/lib/zTree-v3.5.15/css/zTreeStyle/zTreeStyle.css" type="text/css">
			
<!-- 	<link rel="stylesheet" href="{{$smarty.const.WEBSITE_URL}}public/assets/css/jquery-ui-custom.css" /> -->
    	<script src="{{$smarty.const.WEBSITE_URL}}public/assets/lib/jquery-1.8.1.min.js" ></script>
<!--	<!script src="http://www.cnbootstrap.com/assets/js/jquery.js"></script>-->
		<script src="{{$smarty.const.WEBSITE_URL}}public/assets/lib/bootstrap/js/bootbox.min.js"></script>
		<script src="{{$smarty.const.WEBSITE_URL}}public/assets/lib/bootstrap/js/bootstrap-modal.js"></script>
		<script src="{{$smarty.const.WEBSITE_URL}}public/assets/js/other.js"></script>
		<script src="{{$smarty.const.WEBSITE_URL}}public/assets/js/jquery-ui.js"></script>
		<script src="{{$smarty.const.WEBSITE_URL}}public/assets/js/jquery.ui.datepicker.js"></script>
		<script src="{{$smarty.const.WEBSITE_URL}}public/assets/lib/jquery.jqGrid-4.5.2/js/i18n/grid.locale-cn.js"></script>
		<script src="{{$smarty.const.WEBSITE_URL}}public/assets/lib/jquery.jqGrid-4.5.2/js/jquery.jqGrid.min.js"></script>

		<script type="text/javascript" src="{{$smarty.const.WEBSITE_URL}}public/assets/lib/zTree-v3.5.15/js/jquery.ztree.core-3.5.js"></script>
		<script type="text/javascript" src="{{$smarty.const.WEBSITE_URL}}public/assets/lib/zTree-v3.5.15/js/jquery.ztree.excheck-3.5.js"></script>
	  	<script type="text/javascript" src="{{$smarty.const.WEBSITE_URL}}public/assets/lib/zTree-v3.5.15/js/jquery.ztree.exedit-3.5.js"></script>
    	
    	<!-- Demo page code -->
		<!-- easyui 引入 -->
	 	<link rel="stylesheet" type="text/css" href="{{$smarty.const.WEBSITE_URL}}public/assets/lib/jquery-easyui-1.3.4/themes/default/easyui.css">
   	 	<link rel="stylesheet" type="text/css" href="{{$smarty.const.WEBSITE_URL}}public/assets/lib/jquery-easyui-1.3.4/themes/icon.css">
   	 	<link rel="stylesheet" type="text/css" href="{{$smarty.const.WEBSITE_URL}}public/assets/lib/jquery-easyui-1.3.4/themes/bootstrap/easyui.css">
		<script type="text/javascript" src="{{$smarty.const.WEBSITE_URL}}public/assets/lib/jquery-easyui-1.3.4/jquery.easyui.min.js"></script>
	 	<script type="text/javascript" src="{{$smarty.const.WEBSITE_URL}}public/assets/lib/jquery-easyui-1.3.4/jquery.easyui.min.js"></script>
        <style type="text/css">
            #line-chart {
                height:300px;
                width:800px;
                margin: 0px auto;
                margin-top: 1em;
            }
            .brand { font-family: georgia, serif; }
            .brand .first {
                color: #ccc;
                font-style: italic;
            }
            .brand .second {
                color: #fff;
                font-weight: bold;
            }
            input.ui-pg-input {
                width: auto;
                padding: 0px;
                margin: 0px;
                line-height: normal
            }
            select.ui-pg-selbox {
                width: auto;
                padding: 0px;
                margin: 0px;
                line-height: normal
            }
        </style>
  	</head>
    {{if empty($smarty.session.aduser)}}
    <script language='javascript' type='text/javascript'>
        window.location.href='{{$smarty.const.WEBSITE_URL}}admin/login';
    </script>

    {{/if}}
