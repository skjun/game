{{include file ="admin/header.tpl"}}
{{include file ="admin/navibar.tpl"}}
{{include file ="admin/sidebar.tpl"}}
<script src="{{$smarty.const.WEBSITE_URL}}public/assets/highcharts3/js/highcharts.js"></script>
<script src="{{$smarty.const.WEBSITE_URL}}public/assets/highcharts3/js/modules/exporting.js"></script>
<script language="javascript">
    $(function () {
        $('#postreporte').highcharts({
            chart: {
                type: 'line'
            },
            title: {
                text: 'Game Collect Report'
            },
            subtitle: {
                text: 'Game，Tag'
            },
            xAxis: {
                categories: [
                   "games"
                ]
            },
            yAxis: {
                title: {
                    text: 'count'
                }
            },
            tooltip: {
                enabled: true,
                formatter: function() {
                    return '<b>'+ this.series.name +'</b><br/>'+
                    this.x +': '+ this.y ;
                }
            },

            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },
            series: [{
                name: 'games',
                data: [ 55,333]
            } ]
        });



        $('#userreporte').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'Language game count'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        color: '#000000',
                        connectorColor: '#000000',
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'language count',
                data: [
                    {{foreach from=$languagecount item=data}}
                      ['{{$data->language}}',{{$data->lcount}}],
                    {{/foreach}}]

            }]
        });
    });

</script>
<div class="content">
    <div class="header">
        <div class="stats">
            <p class="stat"><!--span class="number"></span--></p>
        </div>
        <h1 class="page-title">Game Site Monitor</h1>
    </div>
    <div class="row-fluid">
        <div class="block">
            <a href="#page-stats" class="block-heading" data-toggle="collapse">Count</a>
            <div id="page-stats" class="block-body collapse in">
                <div class="stat-widget-container">
                    <div class="stat-widget">
                        <div class="stat-button">
                            <p class="title">{{$datacount->gamecount}}</p>
                            <p class="detail">Game Count</p>
                        </div>
                    </div>
                    <div class="stat-widget">
                        <div class="stat-button">
                            <p class="title">{{$datacount->tagcount}}</p>
                            <p class="detail">Tag Count</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="block span6">
            <a href="#tablewidget" class="block-heading" data-toggle="collapse">Collect </a>
            <div id="postreporte" class="block-body collapse in">

            </div>
        </div>
        <div class="block span6">
            <a href="#widget1container" class="block-heading" data-toggle="collapse">Language & Games </a>
            <div id="userreporte" class="block-body collapse in">

            </div>
        </div>
    </div>
    <!-- END 以下内容不需更改，请保证该TPL页内的标签匹配即可 -->
{{include file="admin/footer.tpl" }}