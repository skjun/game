{{include file ="admin/header.tpl"}}
{{include file ="admin/navibar.tpl"}}
{{include file ="admin/sidebar.tpl"}}
<link href="{{$smarty.const.WEBSITE_URL}}public/assets/pagedown/demo.css" rel="stylesheet">
<style>
<!--
	#friendLinkModal {
		margin: -500px 0 0 -280px;
		/* PLAY THE WITH THE VALUES TO SEE GET THE DESIRED EFFECT */
	}
-->
</style>
<div class="content">

    <div class="header">
        <div class="stats">
            <p class="stat"><!--span class="number"></span--></p>
        </div>
        <h1 class="page-title">{{$str['9-1']}}</h1>
    </div>

    <div class="container-fluid">
        <div class="row-fluid">
            <div class="bb-alert alert alert-info" style="display: none;">
                <span>操作成功</span>
            </div>

            {{$admin_action_alert}}
            {{$admin_quick_note}}

            <div id="myTabContent" class="tab-content">
                <!-- 实际写文章内容的布局开始 -->
                <div class="row-fluid">
                    <div class="alert alert-success span12" id="p_tip">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                       {{$str['9-5']}}
                    </div>
                    <div class="container-fluid">
                        <div class="span12   ">
                        	 <div>
                     			 <a id="addnew" class="btn btn-primary"><i class="icon-pencil"></i>{{$str['0-6']}}</a>
                    		</div>
                            <div class="p">
                                <table id="list2"></table>
                                <div id="pager2"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <script language="JavaScript">
                    var weburl = '{{$smarty.const.WEBSITE_URL}}';
                </script>
                <script src="{{$smarty.const.WEBSITE_URL}}public/js/link/link.js"></script>

                <!-- 实际写文章内容的布局结束-->
            </div>
        </div>
          <!-- Modal -->
                        <div  id="friendLinkModal" class="modal hide fade">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h3>{{$str['9-1']}}</h3>
                            </div>
                            <div class="modal-body">
                                <form id="sitefrom" class="form-horizontal">
	                                <div class="control-group">
									  	<label class="control-label">{{$str['9-2']}} </label>
					                  	<div class="controls">
					                        <input id="sitename" name="sitename" type="text"   />
					                  	</div>
									</div>
									 <div class="control-group">
									  	<label class="control-label">{{$str['9-3']}} </label>
					                  	<div class="controls">
					                        <input id=siteurl name="siteurl" type="text" />
					                         <span class="help-block">e.g: http://www.demo.com or http://demo.com</span> 
					                  	</div>
									</div>
									<div class="control-group">
									  	<label class="control-label">{{$str['9-4']}} </label>
					                  	<div class="controls">
					                        <input id="sitelogo" name="sitelogo" type="text" />
					                  	</div>
									</div>
                                   <input type="hidden" id="id"/>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <a href="javascript:void(0)" onclick="grid.winclose()" class="btn">{{$str['0-4']}}</a>

                                <a href="javascript:void(0)" onclick="grid.updatetagform();" class="btn btn-primary">{{$str['0-3']}}</a>
                            </div>
                        </div>
                        <!-- Modal end -->
        <!-- END 以下内容不需更改，请保证该TPL页内的标签匹配即可 -->
{{include file="admin/footer.tpl" }}