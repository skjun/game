{{include file ="admin/header.tpl"}}
{{include file ="admin/navibar.tpl"}}
{{include file ="admin/sidebar.tpl"}}

<div class="content">

    <!-- 表格头部开始 -->
    <div class="header">
        <div class="stats"><p class="stat"></p>
        </div>
        <h1 class="page-title">SEO</h1>
    </div>
    <!-- 表格头部结束 -->

    <div class="container-fluid">
        <div class="row-fluid">

            <div class="alert  alert-info fade in">


                <pre>
                    &lt;title&gt;game site  | game site &lt;/title&gt;
                    &lt;meta name="description" content="pc game"  /&gt;
                    &lt;meta name="keywords" content="pc game"  /&gt;
                </pre>
            </div>

            <ul class="nav nav-tabs" id="myTab">
                <li class="active">
                    <a href="#home" data-toggle="tab">Home</a>
                </li>
                <li><a href="#game" data-toggle="tab"> Game Page</a></li>
                <li><a href="#down" data-toggle="tab">Down/Play Page</a></li>
                <li><a href="#tag" data-toggle="tab">Tag Page</a></li>
            </ul>

            <form class="form-horizontal" id="seo_params_form">
                <div id="myTabContent" class="tab-content">
                    <div class="tab-pane fade active in" id="home">

                        <div class="control-group">

                            <div class="controls">
                                <textarea rows="10" style="width:500px" id="home_seo">{{$params.home_seo}}</textarea>
                            </div>
                        </div>

                    </div>
                    <div class="tab-pane fade" id="game">


                        <div class="control-group">

                            <div class="controls">
                                <textarea rows="10" style="width:500px" id="game_seo">{{$params.game_seo}}</textarea>
                            </div>
                        </div>

                    </div>
                    <div class="tab-pane fade" id="down">

                        <div class="control-group">

                            <div class="controls">
                                <textarea rows="10" style="width:500px" id="down_seo">{{$params.down_seo}}</textarea>
                            </div>
                        </div>


                    </div>
                    <div class="tab-pane fade" id="tag">

                        <div class="control-group">

                            <div class="controls">
                                <textarea rows="10" style="width:500px" id="tag_seo">{{$params.tag_seo}}</textarea>
                            </div>
                        </div>

                    </div>


                    <div class="control-group">
                        <div class="controls">
                            <button type="button" class="btn btn-primary" onclick="updateSeo();">Update</button>
                        </div>
                    </div>
            </form>
        </div>

    </div>
</div>
</div>

<script src="{{$smarty.const.WEBSITE_URL}}public/assets/layer/layer.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("[href^='#']").click(function (){location.hash=$(this).attr("href"); return false;});
        $('#myTab a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        })
    });
    function updateSeo(){
        var datas = new Object();
        $("#seo_params_form input,textarea").each(function () {
            datas[$(this).attr('id')]= $(this).attr('value');
        });
        $.post('admin/seo/saveSeoParams', datas,
                function( data ) {
                    if(data.success=="1"){
                        $.layer({
                            area : ['auto','auto'],
                            dialog : {msg:"SUCCESS",type : 1}
                         });
                 }else{
                     $.layer({
                         area : ['auto','auto'],
                         dialog : {msg:"FAIL",type : 1}
                     });
                 }
             },  "json");
        }
</script>
{{include file="admin/footer.tpl" }}

