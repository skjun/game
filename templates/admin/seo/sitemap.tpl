{{include file ="admin/header.tpl"}}
{{include file ="admin/navibar.tpl"}}
{{include file ="admin/sidebar.tpl"}}

<div class="content">

    <!-- 表格头部开始 -->
    <div class="header">
        <div class="stats"><p class="stat"></p>
        </div>
        <h1 class="page-title">Sitemap生成器</h1>
    </div>
    <!-- 表格头部结束 -->

	<div class="container-fluid">
		<div class="row-fluid">
         <br/>
            <div id="myTabContent" class="tab-content">
                <div class="tab-pane fade active in" id="home">
                    <p>
                    <form class="form-horizontal">
                        {{foreach from=$sitemaps item=sitemap}}
                        <div class="control-group">
                            <label class="control-label" for="{{$sitemap->id}}">
                                <span class="label label-success">{{$sitemap->type}}</span>
                            </label>

                            <div class="controls">
                                优先级(priority):
                                <select class="span2" id="{{$sitemap->id}}_priority">
                                    {{foreach from=$prioritys item=priority}}
                                      <option value="{{$priority}}" {{if $priority eq $sitemap->priority }}selected="" {{/if}}>{{$priority}}</option>
                                    {{/foreach}}
                                </select>&nbsp;&nbsp;
                                更新频率(changefreq):
                                <select id="{{$sitemap->id}}_changefreq"  class="span2">
                                    {{foreach from=$changefreqs item=changefreq}}
                                    <option value="{{$changefreq}}" {{if $changefreq eq $sitemap->changefreq }}selected="" {{/if}}>{{$changefreq}}</option>
                                    {{/foreach}}
                                </select>
                            </div>
                        </div>
                        {{/foreach}}

                        <div class="control-group">
                            <div class="controls">
                                <button type="button" class="btn btn-primary" onclick="updateSitemap();">生成网站SiteMap</button>
                            </div>
                        </div>
                    </form>
                    </p>
                </div>



        </div>
	</div>
</div>

<script src="{{$smarty.const.WEBSITE_URL}}public/assets/layer/layer.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("[href^='#']").click(function (){location.hash=$(this).attr("href"); return false;});

    });
 function updateSitemap(){
     var obj={};


         obj.index_changefreq = $("#index_changefreq").val();
         obj.index_priority = $("#index_priority").val();
         obj.tag_changefreq = $("#tag_changefreq").val();
         obj.tag_priority = $("#tag_priority").val();
     obj.post_changefreq = $("#post_changefreq").val();
     obj.post_priority = $("#post_priority").val();
     obj.category_changefreq = $("#category_changefreq").val();
     obj.category_priority = $("#category_priority").val();
     obj.otherpage_changefreq = $("#otherpage_changefreq").val();
     obj.otherpage_priority = $("#otherpage_priority").val();

        $.post('admin/sitemap/update', obj,
             function( data ) {
                 if(data.success){
                     $.post('admin/sitemap/mksitemap', {},
                             function( data ) {
                                 $.layer({
                                     area : ['auto','auto'],
                                     dialog : {msg:'设置成功',type : 1}
                                 });
                             }
                   ,"json");

                 }else{
                     $.layer({
                         area : ['auto','auto'],
                         dialog : {msg:'设置失败',type : 1}
                     });
                 }
             },  "json");
 }
</script>
{{include file="admin/footer.tpl" }}

