{{include file ="admin/header.tpl"}}
{{include file ="admin/navibar.tpl"}}
{{include file ="admin/sidebar.tpl"}}

<div class="content">

    <!-- 表格头部开始 -->
    <div class="header">
        <div class="stats"><p class="stat"></p>
        </div>
        <h1 class="page-title">网站统计代码</h1>
    </div>
    <!-- 表格头部结束 -->

	<div class="container-fluid">
		<div class="row-fluid">

            <div class="alert  alert-info fade in">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>设置提示!</strong>
                <p>可以放置多个站点的统计代码。注意程序不会在代码上添加< script > 标签。所以，设置的统计代码应该至少包括一对javascript的声明标签</p>
            </div>


            <div id="myTabContent" class="tab-content">
                <div class="tab-pane fade active in" id="home">
                    <p>
                    <form class="form-horizontal">

                        <div class="control-group">
                            <label class="control-label" for="indexkeyword">统计代码</label>
                            <div class="controls">
                                <textarea rows="10" style="width:450px" id="stats_code">{{$param->stats_code}}</textarea>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="controls">
                                <button type="button" class="btn btn-primary" onclick="updateSeo(0);">更新</button>
                            </div>
                        </div>
                    </form>
                    </p>
                </div>

        </div>
	</div>
</div>

<script src="{{$smarty.const.WEBSITE_URL}}public/assets/layer/layer.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("[href^='#']").click(function (){location.hash=$(this).attr("href"); return false;});

    });
 function updateSeo(formtype){
     var obj={};
         obj.tongji_value = $("#tongji_value").val();


        $.post('admin/seo/updatetongji', obj,
             function( data ) {
                 if(data.success){
                         $.layer({
                             area : ['auto','auto'],
                             dialog : {msg:'设置成功',type : 1}
                         });
                 }else{
                     $.layer({
                         area : ['auto','auto'],
                         dialog : {msg:'设置失败',type : 1}
                     });
                 }
             },  "json");
 }
</script>
{{include file="admin/footer.tpl" }}

