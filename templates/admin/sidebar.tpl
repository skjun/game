<div class="sidebar-nav">
    <a href="#1" class="nav-header collapsed" data-toggle="collapse">
        <i class="icon-th"></i>{{$str['1']}}<i class="icon-chevron-up"></i>
    </a>
    <ul id="1" class="nav nav-list collapse in">
        <li><a href="{{$smarty.const.WEBSITE_URL}}admin/seo">{{$str['2']}}</a></li>
        <li><a href="{{$smarty.const.WEBSITE_URL}}admin/sitemap">{{$str['3']}}</a></li>
        <li><a href="{{$smarty.const.WEBSITE_URL}}admin/adminlog">{{$str['4']}}</a></li>
    </ul>
    <a href="#2" class="nav-header collapsed" data-toggle="collapse">
        <i class="icon-th"></i>{{$str['5']}}<i class="icon-chevron-up"></i>
    </a>
    <ul id="2" class="nav nav-list collapse in">
        <li><a href="{{$smarty.const.WEBSITE_URL}}admin/game">{{$str['6']}}</a></li>
        <li><a href="{{$smarty.const.WEBSITE_URL}}admin/gametags">{{$str['7']}}</a></li>
        <li><a href="{{$smarty.const.WEBSITE_URL}}admin/collect">{{$str['8']}}</a></li>
        <li><a href="{{$smarty.const.WEBSITE_URL}}admin/friendlinks">{{$str['9']}}</a></li>
        <li><a href="{{$smarty.const.WEBSITE_URL}}admin/sitelanguage">{{$str['10']}}</a></li>
    </ul>
 </div>