{{include file ="admin/header.tpl"}}
{{include file ="admin/navibar.tpl"}}
{{include file ="admin/sidebar.tpl"}}
<div class="content">

    <!-- 表格头部开始 -->
    <div class="header">
        <div class="stats"><p class="stat"></p>
        </div>
        <h1 class="page-title">{{$str[10]}}</h1>
    </div>
    <!-- 表格头部结束 -->

	<p>
	<div class="container-fluid">
		<form class="form-horizontal">
			<div class="control-group">
			    <label class="control-label" >Select:</label>
			    <div class="controls">
			      	<select id="languagegroup">
			      	
				      	{{foreach from=$languages key=k item=itemlanguage}} 
							{{if $itemlanguage['select'] eq 'selected'}}
								<option value="{{$itemlanguage['lan']}}"
								 	selected="selected"
									 >{{$itemlanguage['name']}}</option>
							{{else}}
								<option value="{{$itemlanguage['lan']}}"
									 >{{$itemlanguage['name']}}</option>
							{{/if}} 
						{{/foreach}}
 
					</select>
			    </div>
			 </div>
			 <div class="control-group">
			 	<div class="controls">
  					<button type="button"  id="savelanguage" class="btn btn-primary">{{$str['0-7']}}</button>
			 	</div>
			  </div>	
	 	</form>
            	
	</div>
<script src="{{$smarty.const.WEBSITE_URL}}public/js/lan/lan.js"></script>
<script src="{{$smarty.const.WEBSITE_URL}}public/assets/layer/layer.min.js"></script>
{{include file="admin/footer.tpl" }}

