{{include file ="admin/header.tpl"}}
{{include file ="admin/navibar.tpl"}}
{{include file ="admin/sidebar.tpl"}}
<link href="{{$smarty.const.WEBSITE_URL}}public/assets/pagedown/demo.css" rel="stylesheet">
<script type="text/javascript" src="{{$smarty.const.WEBSITE_URL}}public/assets/pagedown/Markdown.Converter.js"></script>
<script type="text/javascript" src="{{$smarty.const.WEBSITE_URL}}public/assets/pagedown/Markdown.Sanitizer.js"></script>
<script type="text/javascript" src="{{$smarty.const.WEBSITE_URL}}public/assets/pagedown/Markdown.Editor.js"></script>
<script type="text/javascript"	src="{{$smarty.const.WEBSITE_URL}}public/assets/lib/ajaxupload.js"></script>
<style>
<!--
	#myModal2 {
		margin: -400px 0 0 -280px;
		/* PLAY THE WITH THE VALUES TO SEE GET THE DESIRED EFFECT */
	}
-->
</style>
<div class="content">

    <div class="header">
        <div class="stats">
            <p class="stat"><!--span class="number"></span--></p>
        </div>
        <h1 class="page-title">管理游戏标签 </h1>
    </div>

    <div class="container-fluid">
        <div class="row-fluid">
            <div class="bb-alert alert alert-info" style="display: none;">
                <span>操作成功</span>
            </div>

            {{$admin_action_alert}}
            {{$admin_quick_note}}

            <div id="myTabContent" class="tab-content">
                <!-- 实际写文章内容的布局开始 -->
                <div class="row-fluid">
                    <div class="alert alert-success span12" id="p_tip">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <p><strong>提示!</strong>删除标签时，标签与游戏的关联也将会被删除！</p>
                    </div>
                    <div class="container-fluid">
<!--                        <div class="span4 breadcrumb">-->
<!--                            <label>标签名称</label>-->
<!--                            <input type="text" id="tag_name" placeholder="标签名称">-->
<!---->
<!--                            <label class="textarea">标签图片</label>-->
<!--                            <input type="text" id="tag_img" name="tag_img" readonly="readonly"-->
<!--									placeholder="图片地址位置" class="input-style4 textinput-w3" /> -->
<!--							<input type="button" class="btn btn-small" id="selectfile"  value="图片选择" />-->
<!--							<label class="button"> <button type="button" class="btn" id="addbtn">新增标签</button> </label>-->
<!---->
<!--                        </div>-->
                        <div class="span12   ">

                            <div class="breadcrumb">
                                <form class="form-search">
                                   
                                	<div class="input-prepend">
                           		 		<span class="add-on">Tag Name:</span>
                           				 <input type="text" class="input-medium search-query" id="find_txt">
                        			</div>
                                	<div class="input-prepend">
                           		 		<span class="add-on">Language:</span>
                           				<select name="searchlanguage" id="searchlanguage" style="width: 80px;">
                            			</select>
                        			</div>
                                    <button type="button" class="btn"  id="find_btn">Search</button>
                                    <!-- <button type="button" class="btn btn-primary" id="addbtn">Add</button>-->
                                </form>
                            </div>
                            <div class="p">
                                <table id="list2">
                                </table>
                                <div id="pager2"></div>
                            </div>


                        </div>
                        <!-- Modal -->
                        <div  id="myModal2" class="modal hide fade">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h3>Edit</h3>
                            </div>
                            <div class="modal-body">
                                <form class="form-horizontal">
                                 <input type="hidden" id="m_tagid"/>
                                  <div class="control-group">
                                  	 	<label class="control-label" for="inputEmail">Tag Name: </label>
                                   		<input type="text" id="m_tagname"/>
                                  </div>
 								  <div class="control-group">
	                                    <label class="control-label">Image: </label>
	                                    <!-- <input id="tag_img" type="file" name="files[]"  placeholder="标签图片" multiple> -->
	                                    <input type="text" id="m_tagimg" name="tag_img" readonly="readonly"
										placeholder="image path" class="input-style4 textinput-w3" /> 
										<input type="button" class="btn btn-small" id="m_selectfile"  value="Chose" />
                                 </div>
                                 <div class="control-group">
                                  	 	<label class="control-label"  >Index: </label>
                                   		<select  id="index" >
                                   			<option value=''></option>
                                   			<option value='1'>1</option>
                                   			<option value='2'>2</option>
                                   			<option value='3'>3</option>
                                   			<option value='4'>4</option>
                                   			<option value='5'>5</option>
                                   			<option value='6'>6</option>
                                   		</select>
                                  </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <a href="javascript:void(0)" onclick="grid.winclose()" class="btn">Close</a>

                                <a href="javascript:void(0)" onclick="grid.updatetagform();" class="btn btn-primary">Save</a>
                            </div>
                        </div>
                        
<!--                         <div  id="myModal2" class="modal hide fade">-->
<!--                            <div class="modal-header">-->
<!--                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
<!--                                <h3>修改标签</h3>-->
<!--                            </div>-->
<!--                            <div class="modal-body">-->
<!--                                <form class="form-horizontal">-->
<!--                                  <label class="control-label" for="inputEmail">标签名称</label>-->
<!--                                   <input type="text" id="m_tagname"/>-->
<!--                                   <input type="hidden" id="m_tagid"/>-->
<!---->
<!--                                    <label class="control-label">标签图片</label>-->
<!--                                     <input id="tag_img" type="file" name="files[]"  placeholder="标签图片" multiple> -->
<!--                                    <input type="text" id="m_tagimg" name="tag_img" readonly="readonly"-->
<!--									placeholder="图片地址位置" class="input-style4 textinput-w3" /> -->
<!--									<input type="button" class="btn btn-small" id="m_selectfile"  value="图片选择" />-->
<!--                                </form>-->
<!--                            </div>-->
<!--                            <div class="modal-footer">-->
<!--                                <a href="javascript:void(0)" onclick="grid.winclose()" class="btn">关闭</a>-->
<!---->
<!--                                <a href="javascript:void(0)" onclick="grid.updatetagform();" class="btn btn-primary">保存</a>-->
<!--                            </div>-->
<!--                        </div>-->
                    </div>
                </div>
                <script language="JavaScript">
                    var weburl = '{{$smarty.const.WEBSITE_URL}}';
                </script>
                <script src="{{$smarty.const.WEBSITE_URL}}public/js/tags/game_tag.js"></script>

                <!-- 实际写文章内容的布局结束-->
            </div>
        </div>
        <!-- END 以下内容不需更改，请保证该TPL页内的标签匹配即可 -->
{{include file="admin/footer.tpl" }}