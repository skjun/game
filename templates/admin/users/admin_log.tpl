{{include file ="admin/header.tpl"}}
{{include file ="admin/navibar.tpl"}}
{{include file ="admin/sidebar.tpl"}}


<script src="{{$smarty.const.WEBSITE_URL}}public/assets/js/admin/admin_log.js"></script>

<style type="text/css">
div#rMenu {position:absolute; visibility:hidden; top:0; background-color: #555;text-align: left;padding: 2px;}
div#rMenu ul li{
	margin: 1px 0;
	padding: 0 5px;
	cursor: pointer;
	list-style: none outside none;
	background-color: #DFDFDF;
}
</style>

<div class="content">

	<!-- 表格头部开始 -->
	<div class="header">
		<div class="stats"><p class="stat"></p>
		</div>
		<h1 class="page-title">用户登陆日志</h1>
	</div>
	<!-- 表格头部结束 -->
	
	<ul class="breadcrumb">     </ul>
	<div class="container-fluid">
			<div class="row-fluid">
				<div class="block">
					<div id="page-menu" class="block-body collapse in">
						<br />
						<form class="form-search" method="post" id="searchform">
							<div class="input-prepend">
								<span class="add-on">日志类型:</span> <input class="input-medium"
									type="text" placeholder="日志类型" name="log_type" id="log_type"
									style="width: 80px;">
							</div>
							<div class="input-prepend">
								<span class="add-on">日志描述:</span> <input class="input-medium"
									type="text" placeholder="日志描述" name="log_desc" id="log_desc"
									style="width: 120px;">
							</div>
							<div class="input-prepend">
								<span class="add-on">时间从:</span> 
								<input class="input-medium" type="text" placeholder="时间从" readonly="readonly" name="startTime" id="startTime" style="width: 100px;">
								<span class="add-on">到:</span> <input class="input-medium" readonly="readonly" type="text" placeholder="到" name="endTime" id="endTime" style="width: 100px;">
							</div>
	
							<button type="button" class="btn btn-primary" id="form_search">查询</button>
							<button type="button" class="btn " id="reset">重置</button>
						</form>
					</div>
				</div>
			</div>
			
		<div class="row-fluid">
			<div class="bb-alert alert alert-info" style="display: none;">
				<span>操作成功</span>
			</div>

  
			<div id="myTabContent" class="tab-content">
				<table id="grid"></table>
				<div id="gridPager"></div>
    		</div>
		</div>
	</div>
 </div>


{{include file="admin/footer.tpl" }}
