var Index =  function(){ 
		  function topnav(){
		    $(".site-nav li").click(function(e) {            
			//更改箭头位置
			  $(".site-nav_selected").remove();
			    $(this).children("a").append("<span class='site-nav_selected'></span>");
				//切换导航内容
				var currentidname = $(this).children("a").attr("data");			
				$(".sub-nav_mark").removeClass("active").addClass("hidden");
				$(currentidname).addClass("active").removeClass("hidden");
			   });
		   }	
		  function listitemHover(){
		    $(".listitem").hover(function(evt){  
			   $(this).children('.item-hover').css('display',"block");
			   },function(){
				   $(this).children('.item-hover').css('display',"none");
				 });
		 }
		 function updateLeftandRightIcon(count){
		   if(count == 0){
			   $(".leftarr").css("background","url(./img/arrowsleft.png)  no-repeat");
		   }else{
			   $(".leftarr").css("background","url(./img/arrowsleftred.png)  no-repeat");
		   }
		   if(count == 2){
			   $(".rightarr").css("background","url(./img/arrowsright.png) no-repeat ");
		   }else{
			   $(".rightarr").css("background","url(./img/arrowsrightred.png) no-repeat ");
		   }
		 }
		 
		  function arraction(){
			  
			var count=0 ; 
			  updateLeftandRightIcon(count);
		    $(".hotgame .arr a .leftarr").click(function(){
			  var current = $(".hotgame .active"); 
			  if(count>0){
				 var prev=$(current).prev(".hotgamelist");
			 
			    $(current).removeClass("active").addClass("hidden");
			    $(prev).addClass("active").removeClass("hidden");
			    --count;
			  }else{
				count=0;
			 
			  }
			 updateLeftandRightIcon(count);
			});
			$(".hotgame .arr a .rightarr").click(function(){
			 
			  var current = $(".hotgame .active"); 
			  if(count<$(".hotgamelist").length-1){
				   var next=$(current).next(); 
				$(current).removeClass("active").addClass("hidden");
			    $(next).addClass("active").removeClass("hidden");
			    ++count;
			  }else{ 
			    count=$(".hotgamelist").length-1;
			  }
			 updateLeftandRightIcon(count);
			});
		  }
		  return {
			topnav:topnav,
			listitemHover:listitemHover,
			arraction:arraction
		   };
		   	
}();// JavaScript Document