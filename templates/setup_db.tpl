<!DOCTYPE html>
<html>
  	<head>
  		<base href="{{$smarty.const.WEBSITE_URL}}" />
   		<meta charset="utf-8">
    	<title>{{$page_title}} - {{$smarty.const.ADMIN_TITLE}} - </title>
    	<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	<meta name="description" content="">
    	<meta name="author" content="">

		<link rel="stylesheet" href="{{$smarty.const.WEBSITE_URL}}public/assets/lib/bootstrap/css/bootstrap.css">
		<link rel="stylesheet" href="{{$smarty.const.WEBSITE_URL}}public/assets/stylesheets_default/theme.css">
		<style type="text/css">
			.help-inline{
				color: darkred;
			}
		</style>
		<script src="{{$smarty.const.WEBSITE_URL}}public/assets/lib/jquery-1.8.1.min.js" ></script>
		<script src="{{$smarty.const.WEBSITE_URL}}public/assets/js/jquery.validate.min.js"></script>
		<script src="{{$smarty.const.WEBSITE_URL}}public/assets/layer/layer.min.js"></script>

    	<script>
    		$(function(){
					// binds form submission and fields to the validation engine
					$('#formID').validate({
						errorElement: 'span', //default input error message container
						errorClass: 'help-inline', // default input error message class
						focusInvalid: false, // do not focus the last invalid input
						ignore: "",
						onfocusout: false,
						onkeyup: false,
						onclick: false,
						rules: {
							ip: {
								minlength: 6,
								required: true
							},
							database: {
								required: true
							},
							user: {
								required: true
							},
							pwd: {
								required: true
							}
						},
						messages: {},
						errorPlacement: function (error, element) { // render error placement for each input type
							if (element.attr("name") == "education") { // for chosen elements, need to insert the error after the chosen container
								error.insertAfter("#form_2_education_chzn");
							} else if (element.attr("name") == "membership") { // for uniform radio buttons, insert the after the given container
								error.addClass("no-left-padding").insertAfter("#form_2_membership_error");
							} else if (element.attr("name") == "service") { // for uniform checkboxes, insert the after the given container
								error.addClass("no-left-padding").insertAfter("#form_2_service_error");
							} else {
								error.insertAfter(element); // for other inputs, just perform default behavoir
							}
						},
						invalidHandler: function (event, validator) { //display error alert on form submit

						},
						highlight: function (element) { // hightlight error inputs
							$(element)
									.closest('.help-inline').removeClass('ok'); // display OK icon
							$(element)
									.closest('.control-group').removeClass('success').addClass('error'); // set error class to the control group
						},
						unhighlight: function (element) { // revert the change dony by hightlight
							$(element)
									.closest('.control-group').removeClass('error'); // set error class to the control group
						},
						success: function (label) {
							if (label.attr("for") == "service" || label.attr("for") == "membership") { // for checkboxes and radip buttons, no need to show OK icon
								label
										.closest('.control-group').removeClass('error').addClass('success');
								label.remove(); // remove error label here
							} else { // display success icon for other inputs
								label
										.addClass('valid').addClass('help-inline ok') // mark the current input as valid and display OK icon
										.closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
							}
						},
						submitHandler: function (form) {
							setup();
							return false;
						}
					});
        	});
			//submit
			function setup(){
				var ip = $('#ip').val();
				var database = $('#database').val();
				var user = $('#user').val();
				var pwd = $('#pwd').val();
				$.layer({
					type: 1,
					title: false, //不显示默认标题栏
					shade: [0] //不显示遮罩
				});
				$("#loading").removeClass("hide");
				$.ajax({
					url:"/setup/execute",
					type:'post',
					dataType:'json',
					data:{'ip':ip,'pwd':pwd,'database':database,'user':user},
					success:function(obj){
						layer.msg("",2,-1);
						$.layer({
							area : ['auto','auto'],
							dialog : {msg:'create success ,please login administer and collection game！',type : 1}
						});
						window.location.href="{{$smarty.const.WEBSITE_URL}}admin";
					},
					error:function(obj){
						layer.msg("this form parameter have error ,can not setup database script ",1);
						$("#loading").addClass("hide");
				   }
				});

			};
    	</script>
  	</head>
	<body>
		<!-- 
		<h1>填写数据库信息</h1>
		<div>
			<div>数据库ip：<input id="ip" name="ip" type="text"></div>
			<div>库名称：<input id="database" name="database" type="text"></div>
			<div>用户：<input id="user" name="user" type="text"></div>
			<div>密码：<input id="pwd" name="pwd" type="text"></div>
			<div><input id="setup" type="button" value="提交" ></div>
		</div> -->
		<div class="container-fluid">	    
    <div class="row-fluid">	
	
    <div class="dialog">
        <div class="block">
            <p class="block-heading">Setup Database</p>
            <div class="block-body">
                <form name="loginForm" method="post" action="" id="formID">
                    <label>Mysql IP or Server access url</label>
                    <input type="text" class="span12" name="ip" id="ip" value="" required="true" autofocus="true">
                    <label>Database Name</label>
                    <input type="text" class="span12" id="database" name="database" value=""  >
                    <label>Database Username</label>
                    <input type="text" class="span12" id="user" name="user" value=""   >
                    <label>Database Password</label>
                    <input type="text" class="span12" id="pwd" name="pwd" value=""  >
                    
					<div class="clearfix">
						<img id="loading" class="img-thumbnail  pull-right hide" src="{{$smarty.const.WEBSITE_URL}}public/assets/images/loading2.gif" />
						<input type="submit" id="exe" class="btn btn-primary pull-right" name="loginSubmit" value="Setup"/>
					</div>
					
                </form>
            </div>
        </div>
     
    </div>
	</body>

</html>