<!DOCTYPE html>
<html>
  	<head>
  		<base href="{{$smarty.const.WEBSITE_URL}}" />
   		<meta charset="utf-8">
    	<title>{{$page_title}} - {{$smarty.const.ADMIN_TITLE}} - </title>
    	<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	<meta name="description" content="">
    	<meta name="author" content="">
		<link rel="stylesheet" href="{{$smarty.const.WEBSITE_URL}}public/assets/lib/bootstrap/css/bootstrap.css">
		<link rel="stylesheet" href="{{$smarty.const.WEBSITE_URL}}public/assets/stylesheets_default/theme.css">
         <script src="{{$smarty.const.WEBSITE_URL}}public/assets/lib/jquery-1.8.1.min.js" ></script>
        <script src="{{$smarty.const.WEBSITE_URL}}public/assets/js/jquery.validate.min.js"></script>

        <style type="text/css">
            .help-inline{
                color: darkred;
            }
        </style>
        <script type="application/javascript">
            jQuery(document).ready(function(){
                // binds form submission and fields to the validation engine
                $('#formID').validate({
                    errorElement: 'span', //default input error message container
                    errorClass: 'help-inline', // default input error message class
                    focusInvalid: false, // do not focus the last invalid input
                    ignore: "",
                    onfocusout: false,
                    onkeyup: false,
                    onclick: false,
                    rules: {
                        user_name: {
                            minlength: 6,
                            required: true,
                            userCheck:true
                        },
                        password: {
                            required: true

                        },
                        websitename: {
                            required: true,
                            url:true
                        }
                    },
                    messages: { // custom messages for radio buttons and checkboxes
                        userCheck: {
                            required: "Your username or password not validate"
                        }
                    },
                    errorPlacement: function (error, element) { // render error placement for each input type
                        if (element.attr("name") == "education") { // for chosen elements, need to insert the error after the chosen container
                            error.insertAfter("#form_2_education_chzn");
                        } else if (element.attr("name") == "membership") { // for uniform radio buttons, insert the after the given container
                            error.addClass("no-left-padding").insertAfter("#form_2_membership_error");
                        } else if (element.attr("name") == "service") { // for uniform checkboxes, insert the after the given container
                            error.addClass("no-left-padding").insertAfter("#form_2_service_error");
                        } else {
                            error.insertAfter(element); // for other inputs, just perform default behavoir
                        }
                    },
                    invalidHandler: function (event, validator) { //display error alert on form submit

                    },

                    highlight: function (element) { // hightlight error inputs
                        $(element)
                                .closest('.help-inline').removeClass('ok'); // display OK icon
                        $(element)
                                .closest('.control-group').removeClass('success').addClass('error'); // set error class to the control group
                    },
                    unhighlight: function (element) { // revert the change dony by hightlight
                        $(element)
                                .closest('.control-group').removeClass('error'); // set error class to the control group
                    },
                    success: function (label) {
                        if (label.attr("for") == "service" || label.attr("for") == "membership") { // for checkboxes and radip buttons, no need to show OK icon
                            label
                                    .closest('.control-group').removeClass('error').addClass('success');
                            label.remove(); // remove error label here
                        } else { // display success icon for other inputs
                            label
                                    .addClass('valid').addClass('help-inline ok') // mark the current input as valid and display OK icon
                                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                        }
                    },
                    submitHandler: function (form) {
                        $("#loading").removeClass("hide");
                        form.submit();
                    }
                });
                // 用户是否存在
                $.validator.addMethod("userCheck", function(value, element) {
                    var user_name = $("#user_name").val();
                    var password = $("#password").val();
                    var websitename = $("#websitename").val();
                    var result = false;
                    // Send the request
                    $.ajaxSetup({async: false});
                    $.post('{{$smarty.const.WEBSITE_URL}}setup/checkUserAndSiteId',
                            {"user_name":user_name,"password":password,"websitename":websitename},
                             function(data) {
                                 if(data.result){
                                     result =  true;
                                 }else{
                                     return false;
                                 }
                             }, 'json');

                    return result;

                },"Your username or password can login gamesitebuilder? ");

            });

        </script>
  	</head>
	<body>

		<div class="container-fluid">	    
    <div class="row-fluid">	
	
    <div class="dialog">
        <div class="block">
            <p class="block-heading">Start up</p>
            <div class="block-body">
                <form name="signupForm" id="formID" method="post" action="/setup/validate">
                    <label>Your Username on server(http://gamesitebuilder.com/)</label>
                    <input type="text" class="span12" name="user_name"  id="user_name"  autofocus="true">
                    <label>Your Password on server</label>
                    <input type="password" class="span12" name="password"   id="password"   >
                    <label>Your Website (must equals server)</label>
                    <input type="text" class="span12" name="websitename"    id="websitename"  >
					<div class="clearfix">
                        <img id="loading" class="img-thumbnail  pull-right hide" src="{{$smarty.const.WEBSITE_URL}}public/assets/images/loading2.gif" />
						<input type="submit" class="btn btn-primary pull-right submit" name="loginSubmit" value="Next"/>
					</div>
                </form>
            </div>
        </div>
     
    </div>
	</body>

</html>