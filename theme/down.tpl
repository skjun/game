{{include file = "theme/header.tpl"}}
<div id="crumbread">
    {{foreach from=$crumbreadlist key=k item=item}}
    <a href="{{$item['href']}}"
            {{if $item['append'] == null}}
       onclick="return false;"  class="subnav_on_click"
            {{/if}}
            >
        {{$item['name']}}
    </a>
    {{$item['append']}}
    {{/foreach}}
</div>
{{include file = "theme/hotgames.tpl"}}
<!--begin container -->

<div id="container">
    <div id="main">
        <!-- begin sidebar-->
        {{include file = "theme/navs.tpl"}}
        <!-- end sidebar -->
        <!-- begin right lists -->
        <div class="lists f_l">
            <div>
                <h1 class="ml10">{{$game->game_name}}</h1>
                <div  class="f_r ddthis_sharing_toolbox ">
                    <div class="addthis_sharing_toolbox   pull-left">
                    </div>
                    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5343ad843f8ba58f" async="async"></script>
                </div>
            </div>
            <div>

                {{if $systype eq 'og'}}
                    {{include file='theme/flash.tpl'}}
                {{else}}
                    {{include file='theme/play.tpl'}}
                {{/if}}


            <div id="admid">
                <div id="topad1" class="Down_center_970x90">
                    <h4 class="h4">-Advertisement-</h4>
                    <div>
                        <img src="{{$smarty.const.WEBSITE_URL}}theme/photo/maintopad.jpg" />
                    </div>
                </div>
            </div>
            <!-- begin hot tags -->
            <div  id="hottags" class="lists f_l  ">
                <h1>HOT TAGS</h1>
                <div id="allgamelists" >

                    {{foreach from=$navs key=k item=item}}
                    {{if $k lt 14}}
                    <div class="listitemsmall">
                        <a href="{{$item->href}}"> <img src="{{$item->tag_img}}" /> <span>{{$item->tag_name}}</span>
                        </a>
                    </div>
                    {{/if}}
                    {{/foreach}}

                </div>
            </div>
            <!-- end hot tags -->

            <!-- begin hot games -->
            <div class="lists f_l">
                <h1>Hot games</h1>
                <div id="allgamelists">
                    {{foreach from=$hotgames key=k item=item}}
                    {{if $k lt 12}}
                    <div class="listitem"><a href="{{$item['href']}}"> <img src="{{$item['img']}}" /> </a>
                        <a href="{{$item['href']}}" class="item-hover">
                            <div class="item-hover-bg"> <div class="title">{{$item['game_name']}}</div> </div>
                        </a></div>
                    {{/if}}
                    {{/foreach}}

                </div>
            </div>
            <!-- end hot games -->
        </div>

    </div>
</div>
</div>
{{include file = "theme/root.tpl"}}