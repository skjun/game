<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
{{$seoMsg}}
<meta http-equiv="X-UA-Compatible" content="IE=8">

<base href="{{$smarty.const.WEBSITE_URL}}" />
<link href="{{$smarty.const.WEBSITE_URL}}theme/css/style.css" rel="stylesheet">
<link href="{{$smarty.const.WEBSITE_URL}}theme/css/reset.css" rel="stylesheet">
<link href="{{$smarty.const.WEBSITE_URL}}theme/css/page.css" rel="stylesheet">
<!-- dropdown selector plugin -->
<link href="{{$smarty.const.WEBSITE_URL}}theme/css/dd.css" rel="stylesheet">
<link href="{{$smarty.const.WEBSITE_URL}}theme/css/skin2.css" rel="stylesheet">
<link href="{{$smarty.const.WEBSITE_URL}}theme/css/flags.css" rel="stylesheet">
<script src="{{$smarty.const.WEBSITE_URL}}public/js/jquery.min.js"></script>
<script src="{{$smarty.const.WEBSITE_URL}}theme/js/index.js"></script>
<script src="{{$smarty.const.WEBSITE_URL}}theme/js/jquery.dd.min.js"></script>
<!--<link type="text/css" rel="stylesheet" href="http://cdn.staticfile.org/twitter-bootstrap/3.1.1/css/bootstrap.min.css"/>-->
<script src="{{$smarty.const.WEBSITE_URL}}theme/js/jqPaginator.js"></script>
<script language="javascript"> 
		{{$weburl}}
		{{$curPageType}}
		var name='1s';
		$(document).ready(function(e) {  
		    Index.topnav();
		    //鼠标hover时间，出现游戏名称提示
			Index.listitemHover();
			Index.arraction();
			$("select").msDropdown({roundedBorder:false});
			Index.changeLan();
			Index.init();
			Index.pageLoad(pagetype);
      	});
			
</script>
</head>
<body>
<header>
<div id="header">
<h1 class="logo f_l"><a href="{{$smarty.const.WEBSITE_URL}}index/show"><img
	src="{{$smarty.const.WEBSITE_URL}}theme/img/headerlogo.png" alt="logo" title="Gamesitebuilder" /></a></h1>
<!--begin menu-->
<div id="menu">

<div id="menuinner">

<ul class="site-nav">
	{{foreach from=$mainNavs key=k item=itemmain}}
	<li 	{{if $itemmain['state'] eq 'selected'}} class="topnav_li" {{/if}} >
		<a href="javascript:return false;" class="site-nav_a" data="#{{$itemmain['title']}}"> <img
			src="{{$smarty.const.WEBSITE_URL}}theme/img/{{$itemmain['title']}}.png" /> <span>{{$itemmain['name']}}</span>
			<span  class="site-nav_{{$itemmain['state']}}" ></span> 
		</a>
	</li>
	{{/foreach}}
</ul>
</div>
</div>

<div id="dropdownouter" class="f_r"><select name="countries"
	id="countries" style="width: 105px;">
	{{foreach from=$languages key=k item=itemlanguage}} 
		{{if $itemlanguage['select'] eq 'selected'}}
			<option value="{{$itemlanguage['lan']}}"
				data-image="{{$smarty.const.WEBSITE_URL}}theme/img/blank.gif"
				data-imagecss="flag {{$itemlanguage['lan']}}" selected="selected"
				data-title="{{$itemlanguage->title}}">{{$itemlanguage['name']}}</option>
		{{else}}
		<option value="{{$itemlanguage['lan']}}"
			data-image="{{$smarty.const.WEBSITE_URL}}theme/img/blank.gif"
			data-imagecss="flag {{$itemlanguage['lan']}}"
			data-title="{{$itemlanguage->title}}">{{$itemlanguage['name']}}</option>
		{{/if}} 
	{{/foreach}}

</select></div>
<!--  end dropdown --> <!-- begin sub-website-menu -->

{{foreach from=$subNavs key=k item=subnav}}
	<div id="{{$subnav['id']}}" class="sub-nav_mark {{$subnav['state']}}">
	<div class="sub-site-nav">
		{{foreach from=$subnav['values'] key=k item=subnavitem}}

			<a href="{{$subnavitem['href']}}"
                    {{if $subnavitem['state'] == 'active'}} class="subnavactive"	{{/if}}

                    >{{$subnavitem['genre_name']}}</a>
		{{/foreach}}
		</div>
	</div>
{{/foreach}}	
 
<!--end menu--> <!-- dropdown language  -->



