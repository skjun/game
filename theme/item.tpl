{{include file = "theme/header.tpl"}}
<script src="{{$smarty.const.WEBSITE_URL}}theme/js/unslider.min.js"></script>
 			<div id="crumbread">
                {{foreach from=$crumbreadlist key=k item=item}}
                <a href="{{$item['href']}}"
                        {{if $item['append'] == null}}
                   onclick="return false;"  class="subnav_on_click"
                        {{/if}}
                        >
                    {{$item['name']}}
                </a>
                {{$item['append']}}
                {{/foreach}}
             </div>
			{{include file = "theme/hotgames.tpl"}}
    <!--begin container -->
        <div id="container">       
            <div id="main">


                <!-- begin sidebar-->
                  {{include file = "theme/navs.tpl"}}
                <!-- end sidebar -->
                <!-- begin right lists -->
                <div class="lists f_l">
                    <div class="outerlist">
                    <div>                    
                     	<h1 class="ml10">{{$game->game_name}}</h1>
                        <div  class="f_r ddthis_sharing_toolbox ">
                        	<div class="addthis_sharing_toolbox   pull-left">
    				    	</div>
         					<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5343ad843f8ba58f" async="async"></script>
                        </div>
                    </div> 
                    
                    <div class="smtags">
                    	<h5>{{$str[2]}}:</h5>
                    	{{foreach from=$gamtags key=k item=item}}
	                        <div class="taglistitem">
	                        	<a href="{{$item->href}}">
		                        	<span>{{$item->tag_name}}</span>

	                          </a>                         
	                        </div>
                        {{/foreach}}
                    </div>
                    
                    <div id="textandad">
                            <div class="adbox_336x280">
                                <span class="h4">-Advertisement-</span>
                                <div  class="ad ml10 mr10 f_l Item_center_336x280">
                                    <img src="{{$smarty.const.WEBSITE_URL}}theme/photo/imgad.jpg" />
                                </div>
                            </div>
                          <p>
                              <span>Description</span>: {{$game->longdesc}}    
                          </p>
                          <p></p>
                          
                      </div>                       
                      <div class="embed f_l">
<!--                          <embed src="{{$game->video}}" style="overflow-y:hidden"></embed>-->
<!--                          <div class="arr">-->
<!--                        	<a href="#"><i  class="leftarr1"></i></a>-->
<!--                        	<a href="#"><i  class="rightarr1"></i></a>-->
<!--                          </div>-->
                          <div class="midthk">
                              <div class="banner">
                                  <ul>
                                      {{foreach from=$game->gameimages key=k item=item}}
                                         <li> <img src="{{$item[0]}}"   width="336" /> </li>
                                      {{/foreach}}
                                  </ul>

                              </div>
                          </div>
                          {{if $game->downloadurl neq null}}
                            <a href="{{$game->direct_href}}" class="but_style but_dow">
                              Play Now
                                {{if $game->gametype eq 'og'}}
                                {{else}}
                                  <span>Size:{{$game->gamesize}} MB</span>
                                {{/if}}

                            </a>
                          {{else}}
                            <a href="{{$game->direct_href}}" class="but_style but_dow">
                                Buy Now
                                {{if $game->gametype eq 'og'}}
                                {{else}}
                                    <span>Size:{{$game->gamesize}} MB</span>
                                {{/if}}
                            </a>
                          {{/if}}

                      </div>

                      <div class="rating  f_l">
                      
                      	{{if $game->gametype eq 'og'}}
		 				{{else}}
		 					<table>
                                <thead>
                                    <tr>
                                        <td>Rating:</td>
                                        <td><img src="{{$smarty.const.WEBSITE_URL}}theme/img/stars.png" /></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>OS:</td>
                                        <td class="fontgray">{{$game->OS}}</td>
                                    </tr>

                                    <tr>
                                        <td>RAM:</td>
                                        <td class="fontgray">{{$game->RAM}} MB</td>
                                    </tr>

                                    <tr>
                                        <td>Size:</td>
                                        <td class="fontgray">{{$game->gamesize}} MB</td>
                                    </tr>
                                    <tr>
                                        <td>CPU:</td>
                                        <td class="fontgray">{{$game->CPU}} GHZ</td>
                                    </tr>
                                    <tr>
                                        <td>DirectX:</td>
                                        <td class="fontgray">{{$game->DirectX}}</td>
                                    </tr>
                                    <tr>
                                        <td>Release Date:</td>
                                        <td class="fontgray">{{$game->releasedate}}</td>
                                    </tr>

                                </tbody>
                            </table>
						{{/if}}
                        </div>
                    
                       <div id="rightad2" class="f_r">
                            <h4 class="h4">-Advertisement-</h4>
                            <div id="adright" class="Item_center_right_300x600">

                            </div>
                       </div>

                    </div>
                     <!-- begin hot tags -->
                   <div  id="hottags" class="lists f_l  ">
                    <h1>HOT TAGS</h1>
                        <div id="allgamelists" >
                        
	                        {{foreach from=$navs key=k item=item}}
	                        	{{if $k lt 14}}
									<div class="listitemsmall">
										<a href="{{$item->href}}"> <img src="{{$item->tag_img}}" /> <span>{{$item->tag_name}}</span>
										</a>
									</div>
								{{/if}}
							{{/foreach}}
                           
                        </div>                        
                    </div>
                    <!-- end hot tags -->
                    
                    <!-- begin hot games -->
                   <div class="lists f_l">
                    <h1>Hot games</h1>
                        <div id="allgamelists">
                        {{foreach from=$hotgames key=k item=item}}
                        	{{if $k lt 12}}
                         	<div class="listitem"><a href="{{$item['href']}}"> <img src="{{$item['img']}}" /> </a>
							<a href="{{$item['href']}}" class="item-hover">
							<div class="item-hover-bg"> <div class="title">{{$item['game_name']}}</div> </div>
							</a></div>
							{{/if}}
                        {{/foreach}}
                           
                        </div>                        
                    </div>
                    <!-- end hot games -->
                 </div>  
              
             </div>     	
        </div>
<script>
    if(window.chrome) {
        $('.banner li').css('background-size', '100% 100%');
    }
    $('.banner').unslider({
        speed: 500,
        fluid: true,
        dots: true
    });

    //  Find any element starting with a # in the URL
    //  And listen to any click events it fires
    $('a[href^="#"]').click(function() {
        //  Find the target element
        var target = $($(this).attr('href'));

        //  And get its position
        var pos = target.offset(); // fallback to scrolling to top || {left: 0, top: 0};

        //  jQuery will return false if there's no element
        //  and your code will throw errors if it tries to do .offset().left;
        if(pos) {
            //  Scroll the page
            $('html, body').animate({
                scrollTop: pos.top,
                scrollLeft: pos.left
            }, 1000);
        }

        //  Don't let them visit the url, we'll scroll you there
        return false;
    });
</script>
{{include file = "theme/root.tpl"}}