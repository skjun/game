var Index = function() {
	function topnav() {
		$(".site-nav li")
				.click(
						function(e) {
							// 更改箭头位置
							$(".site-nav_selected").remove();
							$(this).children("a").append(
									"<span class='site-nav_selected'></span>");
							// 切换导航内容
							var currentidname = $(this).children("a").attr(
									"data");
							$(".sub-nav_mark").removeClass("active").addClass(
									"hidden");
							$(currentidname).addClass("active").removeClass(
									"hidden");
						});
	}

	function listitemHover() {
		$(".listitem").hover(function(evt) {
			$(this).children('.item-hover').css('display', "block");
		}, function() {
			$(this).children('.item-hover').css('display', "none");
		});
	}
	function updateLeftandRightIcon(count) {
		if (count == 0) {
			$(".leftarr").css("background",
					"url(" + weburl + "theme/img/arrowsleft.png)  no-repeat");
		} else {
			$(".leftarr")
					.css(
							"background",
							"url(" + weburl
									+ "theme/img/arrowsleftred.png)  no-repeat");
		}
		if (count == 2) {
			$(".rightarr").css("background",
					"url(" + weburl + "theme/img/arrowsright.png) no-repeat ");
		} else {
			$(".rightarr").css(
					"background",
					"url(" + weburl
							+ "theme/img/arrowsrightred.png) no-repeat ");
		}
	}

	function arraction() {

		var count = 0;
		updateLeftandRightIcon(count);
		$(".hotgame .arr a .leftarr").click(function() {
			var current = $(".hotgame .active");
			if (count > 0) {
				var prev = $(current).prev(".hotgamelist");

				$(current).removeClass("active").addClass("hidden");
				$(prev).addClass("active").removeClass("hidden");
				--count;
			} else {
				count = 0;

			}
			updateLeftandRightIcon(count);
		});
		$(".hotgame .arr a .rightarr").click(function() {

			var current = $(".hotgame .active");
			if (count < $(".hotgamelist").length - 1) {
				var next = $(current).next();
				$(current).removeClass("active").addClass("hidden");
				$(next).addClass("active").removeClass("hidden");
				++count;
			} else {
				count = $(".hotgamelist").length - 1;
			}
			updateLeftandRightIcon(count);
		});
	}

	// 修改语言版本
	function changeLan() {
		$("#countries").change(changeLanResult);
	}
	function changeLanResult() {
		var value = $("#countries option:selected").attr("value");
		$.post(weburl + "index/changeLan", {
			ln : value
		}, function(data) {
			window.location.href = data;
		});
	}
	function pageLoad(type) {
		if (type == 0) {
			//home
			getAd("Home_top_300x250");
			getAd("Home_center_970x90");
			getAd("Home_center_300x250");
			getAd("Home_center_left_160x600");
		}
		if (type == 1) {
			//item	
			getAd("Item_top_300x250");
			getAd("Item_center_right_300x600");
			getAd("Item_center_336x280");
			getAd("Item_center_left_160x600");
		}
		if(type ==2){
			//down
			getAd("Down_top_300x250");
			getAd("Down_center_970x90");
			getAd("Down_center_336x280");
			getAd("Down_center_left_160x600");
		}
	}
	function getAd(key){
		$("."+key).html("<iframe width='100%' height='100%' marginwidth='0' marginheight='0' hspace='0' vspace='0' frameborder='0' scrolling='no' src='/ad/show/"+key+"'></iframe>");
	}
	/**
	 * 页面加载完毕,通过ajax获取数据
	 */
	function init() {
		$("a").click(
				function() {
					if ($(this).attr("href") == "#"
							&& $(this).attr("class") != "site-nav_a") {
						return false;
					}
				});

	}
	function loadSubNavData(target) {
		alert(jQuery(target).html());
	}
	return {
		topnav : topnav,
		listitemHover : listitemHover,
		arraction : arraction,
		changeLan : changeLan,
		init : init,
		pageLoad : pageLoad
	};

}();// JavaScript Document
