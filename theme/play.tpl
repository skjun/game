
<div class="ad ml10 f_l Down_center_336x280">

    <!--                        	<h4 class="h4">-Advertisement-</h4>-->
    <!--                            <a href="#">-->
    <!--                                <img src="{{$smarty.const.WEBSITE_URL}}theme/photo/imgad.jpg" />-->
    <!--                            </a>-->
</div>
<div class="midthk ml10 f_l">
    <h1 >Thank you for download <span>{{$game->game_name}}</span> game!</h1>
    Or  <a class="return1" href="{{$game->returnhref}}">Return to the game page</a>
    <br />
    <br />
    <p>{{$game->shortdesc}}</p>

    <div class="rating ml10 mt10 f_l" id="rating_down">

        {{if $game->downloadurl neq null}}
        <a href="{{$game->downhref}}" class="but_style but_dow">
            Download Now
            <span>Download</span>
        </a>
        {{else}}
        <a href="{{$game->downhref}}" class="but_style but_buy">
            Buy Now
            <span>Price:<font>{{$game->price}}</font></span>
        </a>
        {{/if}}

        {{if $game->OS neq null}}
        <table>
            <thead>
            <tr>
                <td>Rating:</td>
                <td><img src="{{$smarty.const.WEBSITE_URL}}theme/img/stars.png" /></td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>OS:</td>
                <td class="fontgray">{{$game->OS}}</td>
            </tr>

            <tr>
                <td>RAM:</td>
                <td class="fontgray">{{$game->RAM}} MB</td>
            </tr>

            <tr>
                <td>Size:</td>
                <td class="fontgray">{{$game->gamesize}} MB</td>
            </tr>

            </tbody>
        </table>
        {{/if}}
    </div>
</div>
<div id="innertpic">
    <div class="listitemsmaller">
            <img src="{{$game->img_thumb1}}" />
    </div>
    <div class="listitemsmaller">
            <img src="{{$game->img_thumb2}}" />
    </div>
    <div class="listitemsmaller">
            <img src="{{$game->img_thumb3}}" />
    </div>
</div>
</div>
