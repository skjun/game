 <!-- end container -->
        <footer>
        	<div id="footer">
            	<div id="friendlink_outer">
                  <div id="friendlink">
                      <img src="theme/{{$config['theme']}}/img/logo2.gif" />
                      <div class="f_r followus">
                          <span>follow us</span>
                          <img src="theme/{{$config['theme']}}/img/icon05.jpg" />
                      </div>
                      
                      <div class="links">
                          <ul class="f_l leftlink">
                              <li class="f_l">
                                  <ul>
	                                  {{foreach from=$mainNavs key=k item=itemmain}}
	                                  
	                                  	  <li>
	                                          <a href="{{$smarty.const.WEBSITE_URL}}index/show/m{{lcfirst($itemmain['gname'])}}_s.html"> {{$itemmain['name']}} </a>
	                                      </li>
	                                  {{/foreach}}
                                  </ul>
                              </li>
                              
		                      {{foreach from=$subNavs key=k item=subnav}}
		                          {{if $subnav['state'] eq 'active'}}
		                         	<li class="f_l">
		                             <ul>
										{{foreach from=$subnav['values'] key=k item=subnavitem}}
											{{if $k ge 0 and $k le 4}}
												<li> 
		                                          	 <a href="{{$subnavitem['href']}}">{{$subnavitem['genre_name']}}</a>  
		                                         </li>
		                                         {{/if}}
										{{/foreach}}
									   </ul>
		                              </li>
		                           {{/if}}
								{{/foreach}}
                                    
                                {{foreach from=$subNavs key=k item=subnav}}
		                          {{if $subnav['state'] eq 'active'}}
		                         	<li class="f_l">
		                             <ul>
										{{foreach from=$subnav['values'] key=k item=subnavitem}}
											{{if $k ge 5 and $k le 9}}
												<li> 
		                                          	 <a href="{{$subnavitem['href']}}">{{$subnavitem['genre_name']}}</a>  
		                                         </li>
		                                         {{/if}}
										{{/foreach}}
									   </ul>
		                              </li>
		                           {{/if}}
								{{/foreach}}
<!--                              <li class="f_l">-->
<!--                                  <ul>-->
<!--                                      <li>-->
<!--                                          <a href="#"><h3>match 3</h3></a>-->
<!--                                      </li>-->
<!--                                  </ul>-->
<!--                              </li>                          -->
                          </ul>
                          <ul class="f_r rightnews">
                              <li><h3 style="color:#fff;">Email Newsletters</h3></li>
                              <li><i>Subscribe to receive inspiration, ideas, and news in your inbox</i></li>
                              <li><input type="text" placeholder="Email Address"/></li>
                              <li><input type="button" class="bginput" /></li>
                          </ul>
                      </div>
                    </div>
                </div>
                <div id="cpright_outer">
                	<div id="cpright">
                        <span>Free Online Games</span>
                        <ul>
                             {{foreach from=$friendlinks key=k item=fitem}}
                                <li><a  href="{{$fitem->siteurl}}">{{$fitem->sitename}}</a><span >|</span></li>
                       		{{/foreach}}
                          
<!--                            <li><a href="#">Two Player Games &nbsp;&nbsp;&nbsp;|</a></li>-->
<!--                            <li><a href="#">2p games &nbsp;&nbsp;&nbsp;|</a></li>-->
<!--                            <li><a href="#">popular flash games &nbsp;&nbsp;&nbsp;|</a></li>-->
<!--                            <li><a href="#">P5games &nbsp;&nbsp;&nbsp;|</a></li>-->
<!--                            <li><a href="#">More links</a></li>-->
                        </ul>
                        <p>Copyright 2011 games-msn.com. All rights reserved</p>
                    </div>
                </div>
            </div>
        </footer>
      <!--      <script src="http://libs.baidu.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>   -->
    </body>
</html>